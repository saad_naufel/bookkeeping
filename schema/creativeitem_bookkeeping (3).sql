-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2016 at 06:04 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `creativeitem_bookkeeping`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_account`
--

DROP TABLE IF EXISTS `bk_account`;
CREATE TABLE IF NOT EXISTS `bk_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `balance` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_account`
--

INSERT INTO `bk_account` (`account_id`, `type`, `title`, `account_number`, `balance`) VALUES
(2, 'bank', 'Aspernatur cumque omnissss', '868888', '10009'),
(3, 'cash', 'Molestiae aut error', '333', '7356');

-- --------------------------------------------------------

--
-- Table structure for table `bk_contact`
--

DROP TABLE IF EXISTS `bk_contact`;
CREATE TABLE IF NOT EXISTS `bk_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `first_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `last_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `company_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext COLLATE utf8_unicode_ci NOT NULL,
  `mobile` longtext COLLATE utf8_unicode_ci NOT NULL,
  `website` longtext COLLATE utf8_unicode_ci NOT NULL,
  `skype_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `country` longtext COLLATE utf8_unicode_ci NOT NULL,
  `city` longtext COLLATE utf8_unicode_ci NOT NULL,
  `state` longtext COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `bank_account` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bk_contact`
--

INSERT INTO `bk_contact` (`contact_id`, `type`, `first_name`, `last_name`, `company_name`, `email`, `phone`, `mobile`, `website`, `skype_id`, `address`, `country`, `city`, `state`, `zip_code`, `bank_account`) VALUES
(2, 'customer', 'Elliott', 'Bentley', 'Noble Estrada', 'pegojesu@gmail.com', '+695-32-3885613', 'Eligendi asperiores enim eius provident libero fuga Repellendus Rerum quis esse architecto proident aut', 'http://www.buvatad.net', 'Dolore accusamus vero est adipisci quis', 'Esse laboris quia ', 'Maiores vero inventore exercitation minus lorem doloremque temporibus illum amet rem labore soluta', 'Earum eiusmod provident eum qui inventore esse accusantium', 'Aliquam necessitatibus et itaque et quasi', '80473', 'Qui consectetur aut fugit ullam labore nemo voluptate'),
(3, 'customer', 'Rahim', 'Carney', 'Calista Shepherd', 'jegyhi@gmail.com', '+378-92-9421268', 'Tempora id excepturi nesciunt excepturi maiores pariatur Pariatur Corrupti aut', 'http://www.duzivefilutebe.net', 'Velit adipisicing est consequat Nostrud quibusdam ex excepteur optio qui ad ut adipisicing earum non quo', 'Ut laboriosam, provide', 'Consectetur ipsam beatae culpa eaque', 'Qui dolor odit ea dolor iste maxime officia modi perspiciatis praesentium', 'In est et iusto quae commodo proident animi', '14524', 'Sunt ea perferendis eligendi beatae et sit minim ducimus rerum minus'),
(4, 'customer', 'Nash', 'Branch', 'Connor Peterson', 'wycesutew@yahoo.com', '+237-61-7788917', 'Quis accusantium eos perspiciatis ipsam odio', 'http://www.letiwof.co', 'Quos voluptas itaque blanditiis autem in necessitatibus lorem cum ut aliquid ad odio nisi nulla enim quibusdam enim', 'Adipisicing dolorem aliquid', 'Qui deserunt dolores eum ullam aut voluptatum earum', 'Atque ipsa dolore eius ipsa nemo unde aut qui aut error modi quibusdam deserunt lorem reiciendis quidem', 'Et consequuntur voluptate optio quidem duis dolores anim error facilis modi explicabo Cum et omnis minima autem', '45728', 'Enim dolore eius iste voluptatibus temporibus eum enim consequatur Eligendi non ex excepturi explicabo'),
(5, 'supplier', 'Rogan', 'Roach', 'Alexis Willis', 'qyhufibuq@gmail.com', '+315-53-7361127', 'Optio et non pariatur Qui dolor', 'http://www.qeqokykixazehag.net', 'Officia ratione est minim vero aliquid dolorem facere ut vel atque veniam omnis at tenetur', 'Maiores eu quaera', 'Voluptatem Earum maiores aut debitis architecto et', 'Consequatur voluptatem Quidem quod dicta est quaerat commodi labore provident ipsa atque ut ipsam blanditiis minima autem dolor', 'Adipisci quia laboris dolorem odio cillum consequatur Reiciendis quae nobis eligendi consequuntur', '32302', 'Et et ut dolore voluptatem vero dignissimos ipsum sunt voluptates exercitation ducimus voluptas at do harum repudiandae quod occaecat enim'),
(6, 'supplier', 'Anika', 'Holder', 'Judah Lara', 'kazukyf@yahoo.com', '+936-25-8450645', '425254', 'http://www.qufyrejurew.ca', 'xfsf@cgds.c', 'Enim aliquam nobis', 'Amet repellendus', 'Inventore', 'Sed necess', '15860', '6183');

-- --------------------------------------------------------

--
-- Table structure for table `bk_currency`
--

DROP TABLE IF EXISTS `bk_currency`;
CREATE TABLE IF NOT EXISTS `bk_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `bk_currency`
--

INSERT INTO `bk_currency` (`currency_id`, `currency_code`, `currency_symbol`, `currency_name`) VALUES
(1, 'USD', '$', 'US dollar'),
(2, 'GBP', '£', 'Pound'),
(3, 'EUR', '€', 'Euro'),
(4, 'AUD', '$', 'Australian Dollar'),
(5, 'CAD', '$', 'Canadian Dollar'),
(6, 'JPY', '¥', 'Japanese Yen'),
(7, 'NZD', '$', 'N.Z. Dollar'),
(8, 'CHF', 'Fr', 'Swiss Franc'),
(9, 'HKD', '$', 'Hong Kong Dollar'),
(10, 'SGD', '$', 'Singapore Dollar'),
(11, 'SEK', 'kr', 'Swedish Krona'),
(12, 'DKK', 'kr', 'Danish Krone'),
(13, 'PLN', 'zł', 'Polish Zloty'),
(14, 'HUF', 'Ft', 'Hungarian Forint'),
(15, 'CZK', 'Kč', 'Czech Koruna'),
(16, 'MXN', '$', 'Mexican Peso'),
(17, 'CZK', 'Kč', 'Czech Koruna'),
(18, 'MYR', 'RM', 'Malaysian Ringgit');

-- --------------------------------------------------------

--
-- Table structure for table `bk_email_template`
--

DROP TABLE IF EXISTS `bk_email_template`;
CREATE TABLE IF NOT EXISTS `bk_email_template` (
  `email_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `task` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subject` longtext COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`email_template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bk_email_template`
--

INSERT INTO `bk_email_template` (`email_template_id`, `task`, `subject`, `body`) VALUES
(1, 'new_admin_account_opening', 'Admin account creation', '<span>\r\n<div>Hi [ADMIN_NAME],</div>\r\n</span>Your admin account is created !\r\nPlease login to your admin&nbsp;<span>account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>Login credential :<br>email : [ADMIN_EMAIL]<br>password : [ADMIN_PASSWORD]');

-- --------------------------------------------------------

--
-- Table structure for table `bk_expense`
--

DROP TABLE IF EXISTS `bk_expense`;
CREATE TABLE IF NOT EXISTS `bk_expense` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `income_expense_category_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bk_expense`
--

INSERT INTO `bk_expense` (`expense_id`, `title`, `description`, `income_expense_category_id`, `amount`, `account_id`, `date`) VALUES
(2, 'Officia est cupidatat in qu', 'Et consequuntur eu sint laboriosam, sunt non sed enim quia ab aspernatur reiciendis dolor ad deleniti hic incidunt.', 2, '54', 2, '1474394400'),
(3, 'Aperiam incidunt enim', 'At provident, cumque exercitation voluptatum enim even', 3, '73', 3, '1458669600'),
(4, 'Libero id consectetur cu', 'Consequat. Lorem et earum accusantium omnis labore ea voluptates ut.', 2, '32', 2, '1497981600'),
(5, 'Non nesciunt minima volu', 'Et explicabo. Voluptatum consequatur, nisi', 3, '89', 3, '1463508000'),
(6, 'Non ad exercitationem mag', 'Quo error obcaecati maxime placeat, velit, eu rerum sit ea minima.', 2, '57', 2, '1447178400');

-- --------------------------------------------------------

--
-- Table structure for table `bk_income`
--

DROP TABLE IF EXISTS `bk_income`;
CREATE TABLE IF NOT EXISTS `bk_income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `income_expense_category_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`income_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_income`
--

INSERT INTO `bk_income` (`income_id`, `title`, `description`, `income_expense_category_id`, `amount`, `account_id`, `date`) VALUES
(2, 'Ut ut adipisci sed exercita', 'Nulla id ut labore facere doloribus iste et ad architecto dolorem quia consequatur? Non doloribus tempore, error nobis animi, culpa.', 3, '542', 2, '1452016800'),
(3, 'Mollitia aliqua Dolores', 'A molestias est fuga. Libero aliquam incididunt', 2, '200', 2, '1439920800');

-- --------------------------------------------------------

--
-- Table structure for table `bk_income_expense_category`
--

DROP TABLE IF EXISTS `bk_income_expense_category`;
CREATE TABLE IF NOT EXISTS `bk_income_expense_category` (
  `income_expense_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`income_expense_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_income_expense_category`
--

INSERT INTO `bk_income_expense_category` (`income_expense_category_id`, `name`) VALUES
(2, 'Sales'),
(3, 'Advertising');

-- --------------------------------------------------------

--
-- Table structure for table `bk_language`
--

DROP TABLE IF EXISTS `bk_language`;
CREATE TABLE IF NOT EXISTS `bk_language` (
  `phrase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `en` longtext COLLATE utf8_unicode_ci NOT NULL,
  `bn` longtext COLLATE utf8_unicode_ci NOT NULL,
  `es` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ar` longtext COLLATE utf8_unicode_ci NOT NULL,
  `de` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fr` longtext COLLATE utf8_unicode_ci NOT NULL,
  `it` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ru` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tr` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=319 ;

--
-- Dumping data for table `bk_language`
--

INSERT INTO `bk_language` (`phrase_id`, `phrase`, `en`, `bn`, `es`, `ar`, `de`, `fr`, `it`, `ru`, `tr`) VALUES
(1, 'admin_dashboard', 'Admin Dashboard', 'অ্যাডমিন ড্যাশবোর্ড', 'Dashboard administración', 'المشرف لوحة', 'Admin-Dashboard', 'Administrateur Dashboard', 'Admin Dashboard', 'Админ Панель', 'Yönetici Paneli'),
(313, 'Admin Dashboard', '', '', '', '', '', '', '', '', ''),
(314, 'Ajustes del sistema', '', '', '', '', '', '', '', '', ''),
(315, 'write_language_file', '', '', '', '', '', '', '', '', ''),
(316, 'file_creation_success', '', '', '', '', '', '', '', '', ''),
(3, 'manage_users', 'Manage Users', 'ব্যবহারকারী গালাগাল প্রতিবেদন', 'Administrar usuarios', 'إدارة المستخدمين', 'Benutzer verwalten', 'Gérer les utilisateurs', 'Gestisci utenti', 'Управление пользователями', 'Kullanıcıları Yönetme'),
(4, 'support_staffs', 'Support Staffs', 'সাপোর্ট কর্মীরা', 'El personal de apoyo', 'موظفو الدعم', 'Support Staffs', 'Personnel de soutien', 'Il personale di supporto', 'Поддержка Штабы', 'Destek Değnek'),
(5, 'store_owners', 'Store Owners', 'দোকান মালিকদের', 'Los propietarios de las tiendas', 'أصحاب المحلات التجارية', 'Shop-Inhaber', 'Propriétaires de magasins', 'Negozio Proprietari', 'Владельцы магазина', 'Mağaza sahipleri'),
(6, 'employees', 'Employees', 'এমপ্লয়িজ', 'Empleados', 'الموظفين', 'Mitarbeiter', 'Des employés', 'Dipendenti', 'Сотрудники', 'Çalışanlar'),
(7, 'customers', 'Customers', 'গ্রাহকরা', 'Clientes', 'الزبائن', 'Kunden,', 'Les clients', 'Clienti', 'Клиенты', 'Müşteriler'),
(8, 'manage_service', 'Manage Service', 'সেবা পরিচালনা', 'Administrar servicio', 'إدارة خدمة', 'Dienst verwalten', 'Gérer le service', 'Gestione servizio', 'Управление службы', 'Servis yönetin'),
(9, 'service', 'Service', 'সেবা', 'Servicio', 'خدمة', 'Bedienung', 'Service', 'Servizio', 'обслуживание', 'Servis'),
(10, 'manage_store', 'Manage Store', 'দোকান পরিচালনা', 'Administrar tienda', 'إدارة المتجر', 'Shop verwalten', 'Gérer magasin', 'Gestirà Negozio', 'Управление магазин', 'Mağaza Yönet'),
(11, 'stores', 'Stores', 'দোকান', 'Víveres', 'مخازن', 'Shops', 'Magasins', 'I negozi', 'магазины', 'Mağazalar'),
(12, 'branches', 'Branches', 'শাখা', 'Ramas', 'الفروع', 'Geäst', 'Branches', 'Filiali', 'ветви', 'Şubeler'),
(13, 'manage_stores', 'Manage Stores', 'দোকানে পরিচালনা', 'Administrar Tiendas', 'إدارة المخازن', 'Stores verwalten', 'Gérer Magasins', 'Gestire Negozi', 'Управление Магазины', 'Mağazalar yönetin'),
(14, 'manage_appointments', 'Manage Appointments', 'কলকব্জা পরিচালনা', 'Administrar citas', 'إدارة التعيينات', 'Termine verwalten', 'Gérer nominations', 'Gestire Appuntamenti', 'Управление Назначения', 'Randevu yönetin'),
(15, 'manage_tables', 'Manage Tables', 'টেবিল পরিচালনা', 'Administrar Tablas', 'إدارة الجداول', 'Tabellen verwalten', 'Gérer les tableaux', 'Gestire le tabelle', 'Управление Столы', 'Tablolar yönetin'),
(16, 'manage_cities', 'Manage Cities', 'শহর ও পরিচালনা', 'Administrar Ciudades', 'إدارة المدن', 'Städte verwalten', 'Gérer Villes', 'Gestire Città', 'Управление Города', 'Şehirler yönetin'),
(17, 'manage_support_tickets', 'Manage Support Tickets', 'সাপোর্ট টিকেট পরিচালনা', 'Administrar Tickets de Soporte', 'إدارة تذاكر الدعم الفني', 'Verwalten Support Tickets', 'Gérer les tickets de support', 'Gestire Biglietti Supporto', 'Управление поддержки билеты', 'Destek Biletleri yönetin'),
(18, 'manage_invoices', 'Manage Invoices', 'চালানে পরিচালনা', 'Manejo de facturas', 'إدارة الفواتير', 'Rechnungen verwalten', 'Gérer factures', 'Gestire fatture', 'Управление Счета', 'Faturalar yönetin'),
(19, 'settings', 'Settings', 'সেটিংস', 'Ajustes', 'إعدادات', 'Einstellungen', 'Paramètres', 'Impostazioni', 'Настройки', 'Ayarlar'),
(20, 'system_settings', 'System Settings', 'পদ্ধতি নির্ধারণ', 'Ajustes del sistema', 'اعدادات النظام', 'Systemeinstellungen', 'Les paramètres du système', 'Impostazioni di sistema', 'Настройки системы', 'Sistem ayarları'),
(21, 'email_settings', 'Email Settings', 'ইমেইল সেটিংস', 'Ajustes del correo electrónico', 'إعدادات البريد الإلكتروني', 'E-Mail-Einstellungen', 'Paramètres de messagerie', 'Impostazioni e-mail', 'Настройки электронной почты', 'E-posta Ayarları'),
(22, 'language_settings', 'Language Settings', 'ভাষা ব্যাবস্থা', 'Ajustes de idioma', 'اعدادات اللغة', 'Spracheinstellungen', 'Paramètres de langue', 'Impostazioni della lingua', 'Языковые настройки', 'Dil ayarları'),
(23, 'account', 'Account', 'হিসাব', 'Cuenta', 'حساب', 'Konto', 'Compte', 'Conto', 'Счет', 'Hesap'),
(24, 'active_:_', 'Active : ', 'সক্রিয়: ', 'Activo : ', 'نشط : ', 'Aktiv : ', 'Actif : ', 'Attivo : ', 'Активность: ', 'Aktif : '),
(25, 'on_trial_:_', 'On Trial : ', 'তাকে বিচারের সম্মুখীন: ', 'En juicio : ', 'قيد التجربة : ', 'On Trial: ', 'En procès : ', 'In prova : ', 'На суде : ', 'Deneme : '),
(26, 'pending_:_', 'Pending : ', 'অপেক্ষারত: ', 'Pendiente : ', 'بانتظار : ', 'Bis: ', 'En attendant : ', 'In attesa di : ', 'В ожидании: ', 'Beklemede: '),
(27, 'appointments', 'Appointments', 'কলকব্জা', 'Equipo', 'تعيينات', 'Ernennungen', 'Nominations', 'Appuntamenti', 'Назначения', 'Randevular'),
(28, 'banned_:_', 'Banned : ', 'নিষিদ্ধ: ', 'Expulsado: ', 'المحظورة: ', 'Verboten : ', 'Interdit: ', 'Vietato : ', 'Banned: ', 'Yasak: '),
(29, 'cancelled_:_', 'Cancelled : ', 'বাতিল হয়েছে: ', 'Cancelado : ', 'تم الالغاء : ', 'Abgebrochen : ', 'Annulé : ', 'Annullato : ', 'Отменено: ', 'İptal: '),
(30, 'tickets', 'Tickets', 'টিকিট', 'Entradas', 'تذاكر', 'Tickets', 'Billets', 'Biglietti', 'Билеты', 'Biletler'),
(31, 'opened:_', 'Opened: ', 'খোলা: ', 'Abierto: ', 'افتتح: ', 'Eröffnet am: ', 'Ouvert: ', 'Aperto: ', 'Открыт: ', 'Açıldı: '),
(32, 'closed:_', 'Closed: ', 'বন্ধ হয়েছে: ', 'Cerrado: ', 'مغلق: ', 'Abgeschlossen: ', 'Fermé: ', 'Chiusa: ', 'Закрыт: ', 'Kapalı: '),
(33, 'invoices', 'Invoices', 'চালান', 'Facturas', 'الفواتير', 'Rechnungen', 'Factures', 'Fatture', 'Счета', 'Faturalar'),
(34, 'paid:_', 'Paid: ', 'প্রদত্ত: ', 'Pagado: ', 'دفع: ', 'Bezahlt: ', 'Payé: ', 'Pagato: ', 'Выплачено: ', 'Ücretli: '),
(35, 'unpaid:_', 'Unpaid: ', 'অবৈতনিক: ', 'No pagado: ', 'غير المدفوعة: ', 'Unbezahlte: ', 'Non payé: ', 'Non pagato: ', 'Неоплаченный: ', 'Ödenmemiş: '),
(36, 'salon_appointment_schedule', 'Salon Appointment Schedule', 'সেলুন এপয়েন্টমেন্ট Schedule', 'Horario cita del salón', 'صالون جدولة موعد', 'Salon Termin zu vereinbaren', 'Planning de rendez-Salon', 'Salon FISSARE UN APPUNTAMENTO', 'Салон Назначение Расписание', 'Salon Randevu Takvimi'),
(37, 'restaurant_appointment_schedule', 'Restaurant Appointment Schedule', 'রেষ্টুরেন্ট এপয়েন্টমেন্ট Schedule', 'Restaurante Horario Nombramiento', 'مطعم جدولة موعد', 'Restaurant Termin zu vereinbaren', 'Nomination restaurant annexe', 'Ristorante FISSARE UN APPUNTAMENTO', 'Ресторан Назначение Расписание', 'Restoran Randevu Takvimi'),
(38, 'manage_support_staffs', 'Manage Support Staffs', 'সাপোর্ট কর্মীরা পরিচালনা', 'Manejo de personal de apoyo', 'إدارة الأركان الدعم', 'Verwalten Support-Teams', 'Gérer personnel de soutien', 'Gestire personale di supporto', 'Управление поддержки штабам', 'Destek Staffs yönetin'),
(39, 'Manage Support Staffs', 'Manage Support Staffs', 'সাপোর্ট কর্মীরা পরিচালনা', 'Manejo de personal de apoyo', 'إدارة الأركان الدعم', 'Verwalten Support-Teams', 'Gérer personnel de soutien', 'Gestire personale di supporto', 'Управление поддержки штабам', 'Destek Staffs yönetin'),
(40, 'add_new_support_staff', 'Add New Support Staff', 'নিউ সাপোর্ট স্টাফ করো', 'Agregar nuevo personal auxiliar', 'إضافة دعم جديد للموظفين', 'In New Support Staff', 'Ajouter du personnel de New Soutien', 'Aggiungi nuovo supporto del personale', 'Добавить вспомогательного персонала', 'Yeni Destek Ekibi ekle'),
(41, 'first_name', 'First Name', 'প্রথম নাম', 'Nombre', 'الاسم الأول', 'Vorname', 'Prénom', 'Nome', 'Имя', 'İsim'),
(42, 'last_name', 'Last Name', 'শেষ নাম', 'Apellido', 'الاسم الاخير', 'Familienname, Nachname', 'Nom de famille', 'Cognome', 'Фамилия', 'Soyadı'),
(43, 'phone', 'Phone', 'ফোন', 'Teléfono', 'الهاتف', 'Telefon', 'Téléphone', 'Telefono', 'Телефон', 'Telefon'),
(44, 'email', 'Email', 'ইমেইল', 'Email', 'البريد الإلكتروني', 'Email', 'Email', 'E-mail', 'Эл. адрес', 'E-posta'),
(45, 'options', 'Options', 'বিকল্প', 'Opciones', 'خيارات', 'Optionen', 'Options', 'Opzioni', 'Опции', 'Seçenekler'),
(46, 'actions', 'Actions', 'পদক্ষেপ', 'Comportamiento', 'تطبيقات', 'Aktionen', 'actes', 'Azioni', 'Меры', 'Eylemler'),
(47, 'edit', 'Edit', 'সম্পাদন করা', 'Editar', 'تحرير', 'Bearbeiten', 'modifier', 'Modifica', 'редактировать', 'Düzenleme'),
(48, 'delete', 'Delete', 'মুছে', 'Borrar', 'حذف', 'Löschen', 'Effacer', 'Cancellare', 'Удалить', 'Silmek'),
(49, 'add_support_staff', 'Add Support Staff', 'সাপোর্ট স্টাফ করো', 'Añadir Personal de Apoyo', 'إضافة دعم للموظفين', 'In Support Staff', 'Ajouter personnel de soutien', 'Aggiungere Support Staff', 'Добавить вспомогательного персонала', 'Destek Ekibi ekle'),
(50, 'password', 'Password', 'পাসওয়ার্ড', 'Clave', 'الرمز السري', 'Passwort', 'Mot de passe', 'Parola d''ordine', 'пароль', 'Parola'),
(51, 'submit', 'Submit', 'জমা দিন', 'Enviar', 'عرض', 'einreichen', 'Proposer', 'Presentare', 'Отправить', 'Gönder'),
(52, 'edit_support_staff', 'Edit Support Staff', 'সম্পাদনা সাপোর্ট স্টাফ', 'Editar Personal de Apoyo', 'تحرير موظفي الدعم', 'Bearbeiten Support Staff', 'Modifier personnel de soutien', 'Modifica personale di supporto', 'Редактировать Персонал Поддержка', 'Düzenleme Destek Elemanı'),
(53, 'update', 'Update', 'আপডেট', 'Actualizar', 'تحديث', 'Aktualisieren', 'Mettre à jour', 'Aggiornare', 'Обновить', 'Güncelleştirme'),
(54, 'manage_store_owners', 'Manage Store Owners', 'দোকান মালিকদের পরিচালনা', 'Administrar tienda Propietarios', 'إدارة المتجر مالكي', 'Verwalten Store Eigentümer', 'Gérer les propriétaires de magasins', 'Gestirà Negozio Proprietari', 'Управление магазин Владельцы', 'Mağaza Sahipleri yönetin'),
(55, 'Manage Store Owners', 'Manage Store Owners', 'দোকান মালিকদের পরিচালনা', 'Administrar tienda Propietarios', 'إدارة المتجر مالكي', 'Verwalten Store Eigentümer', 'Gérer les propriétaires de magasins', 'Gestirà Negozio Proprietari', 'Управление магазин Владельцы', 'Mağaza Sahipleri yönetin'),
(56, 'pending_store_owners', 'Pending Store Owners', 'দোকান মালিকদের অপেক্ষারত', 'A la espera de los propietarios de tiendas', 'في انتظار أصحاب المحلات التجارية', 'Schwebende Store Eigentümer', 'Dans l''attente de propriétaires de magasins', 'In attesa Negozio Proprietari', 'В ожидании владельцев магазинов', 'Mağaza Sahipleri Bekleyen'),
(57, 'country', 'Country', 'দেশ', 'País', 'بلد', 'Land', 'Pays', 'Nazione', 'Страна', 'Ülke'),
(58, 'city', 'City', 'শহর', 'Ciudad', 'المدينة', 'Stadt', 'Ville', 'Città', 'город', 'Şehir'),
(59, 'status', 'Status', 'অবস্থা', 'Estado', 'الحالة', 'Status', 'statut', 'Stato', 'Положение дел', 'Statü'),
(60, 'owned_stores', 'Owned Stores', 'মালিকানাধীন দোকানে', 'Tiendas de propiedad', 'متاجر يملكها', 'Owned Stores', 'Magasins d''occasion', 'Negozi di proprietà', 'Собственные магазины', 'Hisseli Mağazaları'),
(61, 'active', 'Active', 'সক্রিয়', 'Activo', 'نشط', 'Aktiv', 'actif', 'Attivo', 'Активный', 'Aktif'),
(62, 'owned_stores_:_', 'Owned Stores : ', 'মালিকানাধীন দোকানে: ', 'Tiendas de propiedad: ', 'متاجر مملوكة: ', 'Owned Shops: ', 'Magasins d''occasion: ', 'Negozi di proprietà: ', 'Собственные магазины: ', 'Hisseli Mağazalar: '),
(63, 'decline', 'Decline', 'পতন', 'Disminución', 'رفض', 'Ablehnen', 'décliner', 'Declino', 'Снижение', 'Gerileme'),
(64, 'pending', 'Pending', 'বিচারাধীন', 'Pendiente', 'بانتظار', 'In Erwartung', 'en attendant', 'in attesa di', 'До', 'Bekleyen'),
(65, 'no_stores_owned_yet', 'No Stores Owned Yet', 'কোন দোকান তবুও মালিক', 'No hay tiendas de propiedad embargo,', 'لا المخازن المملوكة لكن', 'Noch keine Shops Owned', 'Pas de magasins appartenant Pourtant,', 'Nessun negozi di proprietà Ancora', 'Нет Магазины имеемое же', 'Hiçbir Mağazaları Henüz Hisseli'),
(66, 'approve', 'Approve', 'অনুমোদন করা', 'Aprobar', 'الموافقة', 'Genehmigen', 'Approuver', 'Approvare', 'Одобрить', 'Onayla'),
(67, 'manage_employees', 'Manage Employees', 'কর্মচারীদের পরিচালনা', 'Manejo de Empleados', 'إدارة الموظفين', 'Mitarbeiter verwalten', 'Gérer employés', 'Gestire i dipendenti', 'Управление сотрудников', 'Çalışan yönetin'),
(68, 'Manage Employees', 'Manage Employees', 'কর্মচারীদের পরিচালনা', 'Manejo de Empleados', 'إدارة الموظفين', 'Mitarbeiter verwalten', 'Gérer employés', 'Gestire i dipendenti', 'Управление сотрудников', 'Çalışan yönetin'),
(69, 'all_stores', 'All Stores', 'সমস্ত দোকান', 'Todas las tiendas', 'جميع المتاجر', 'Alle Händler', 'Tous les magasins', 'Tutti i negozi', 'Все магазины', 'Tüm Mağazaları'),
(70, 'filter', 'Filter', 'ফিল্টার', 'Filtrar', 'فلتر', 'Filter', 'Filtre', 'Filtro', 'Фильтр', 'Filtre'),
(71, 'add_new_employee', 'Add New Employee', 'নতুন কর্মী যোগ', 'Añadir nuevo empleado', 'إضافة جديد الموظف', 'Hinzufügen Neuer Mitarbeiter', 'Ajouter des nouveaux employés', 'Aggiungi nuovo dipendente', 'Добавить нового сотрудника', 'Yeni Çalışanı ekle'),
(72, 'employee_name', 'Employee Name', 'কর্মকর্তার নাম', 'nombre de empleado', 'اسم الموظف', 'Mitarbeitername', 'Nom de l''employé', 'Nome dipendente', 'Имя сотрудника', 'Çalışan Adı'),
(73, 'store', 'Store', 'দোকান', 'Almacenar', 'تخزين', 'Geschäft', 'boutique', 'Negozio', 'хранить', 'Mağaza'),
(74, 'add_employee', 'Add Employee', 'কর্মচারী যোগ', 'Añadir Empleado', 'إضافة موظف', 'In Mitarbeiter', 'Ajouter Employé', 'Aggiungi dipendenti', 'Добавить Employee', 'Çalışanların ekle'),
(75, 'select', 'Select', 'নির্বাচন করা', 'Seleccionar', 'اختار', 'Auswählen', 'Sélectionner', 'Selezionare', 'Выбрать', 'Seçin'),
(76, 'edit_employee', 'Edit Employee', 'সম্পাদনা কর্মচারী', 'Editar Empleado', 'تحرير موظف', 'Bearbeiten Mitarbeiter', 'Modifier des employés', 'Modifica dei dipendenti', 'Редактировать Сотрудник', 'Düzenleme Çalışan'),
(77, 'value_required', 'Value Required', 'মূল্য প্রয়োজন', 'Valor Obligatorio', 'القيمة المطلوبة', 'Wert Erforderlich', 'Valeur Obligatoire', 'Valore Obbligatorio', 'Значение Обязательно', 'Değer Gerekli'),
(78, 'select_store', 'Select Store', 'নির্বাচন স্টোর', 'Seleccionar tienda', 'حدد المتجر', 'Store auswählen', 'Choisir un magasin', 'Seleziona il negozio', 'Выберите магазин', 'Seçin Mağaza'),
(79, 'update_employee', 'Update Employee', 'আপডেট কর্মচারী', 'Actualización del Empleado', 'تحديث موظف', 'Update Mitarbeiter', 'Mise à jour des employés', 'Aggiornamento dei dipendenti', 'Обновление Сотрудник', 'Güncelleme Çalışan'),
(80, 'manage_customers', 'Manage Customers', 'গ্রাহকদের পরিচালনা', 'Administrar Clientes', 'إدارة العملاء', 'Kunden verwalten', 'Gérer clients', 'Gestire i clienti', 'Управление клиентами', 'Müşteriler yönetin'),
(81, 'Manage Customers', 'Manage Customers', 'গ্রাহকদের পরিচালনা', 'Administrar Clientes', 'إدارة العملاء', 'Kunden verwalten', 'Gérer clients', 'Gestire i clienti', 'Управление клиентами', 'Müşteriler yönetin'),
(82, 'appointments_history', 'Appointments History', 'অ্যাপয়েন্টমেন্টগুলো ইতিহাস', 'Historia de Nombramientos', 'التعيينات التاريخ', 'Termine Geschichte', 'Nominations Histoire', 'Appuntamenti Storia', 'Назначения История', 'Randevular Tarih'),
(83, 'banned', 'Banned', 'নিষিদ্ধ', 'Banned', 'حظرت', 'Verboten', 'Banned', 'Vietato', 'Запрещенный', 'Yasaklı'),
(84, 'appointments_of_:_', 'Appointments Of : ', 'কলকব্জা: ', 'Los nombramientos de: ', 'تعيين كل من: ', 'Ernennungen von: ', 'Nominations de: ', 'Appuntamenti di: ', 'Назначения: ', 'Randevular Of: '),
(85, 'Appointments Of : Megan Parks', 'Appointments Of : Megan Parks', 'মেগান পার্ক: নিয়োগ', 'Los nombramientos de: Megan Parques', 'تعيين كل من: ميغان حدائق', 'Ernennungen von: Megan Parks', 'Nominations de: Megan Parcs', 'Appuntamenti di: Megan Parchi', 'Назначения: Меган парки', 'Megan Parks: Of Randevular'),
(86, 'appointment_number', 'Appointment Number', 'নিয়োগ সংখ্যা', 'Número Nombramiento', 'عدد التعيين', 'Terminnummer', 'Nombre de rendez-vous', 'Numero appuntamento', 'Назначение Количество', 'Randevu Numarası'),
(87, 'appointment_date', 'Appointment Date', 'সাক্ষাৎকারের তারিখ', 'Día de la cita', 'تعيين التسجيل', 'Termin', 'Date de rendez-vous', 'Data dell''appuntamento', 'Назначенная дата', 'Randevu Tarihi'),
(88, 'appointment_time', 'Appointment Time', 'অ্যাপয়েন্টমেন্টের সময়', 'Hora de la cita', 'تعيين الوقت', 'Termin', 'Nomination Temps', 'Appuntamento Tempo', 'Время встречи', 'Randevu Zamanı'),
(89, 'employee', 'Employee', 'কর্মচারী', 'Empleado', 'عامل', 'Mitarbeiter', 'Employé', 'Dipendente', 'Наемный рабочий', 'Çalışan'),
(90, 'customer', 'Customer', 'ক্রেতা', 'Cliente', 'زبون', 'Kunde', 'Client', 'Cliente', 'Клиент', 'Müşteri'),
(91, 'booking_date', 'Booking Date', 'বুকিং তারিখ', 'Fecha para registrarse', 'تاريخ الحجز', 'Buchungsdatum', 'Date de réservation', 'Data di prenotazione', 'Дата бронирования', 'Rezervasyon tarihi'),
(92, 'booking_time', 'Booking Time', 'বুকিং সময়', 'Hora de Reserva', 'حجز الوقت', 'Reservierungszeit', 'Réservation Temps', 'Ora di Prenotazione', 'Бронирование времени', 'Kitap Zamanı'),
(93, 'Manage Store', 'Manage Store', 'দোকান পরিচালনা', 'Administrar tienda', 'إدارة المتجر', 'Shop verwalten', 'Gérer magasin', 'Gestirà Negozio', 'Управление магазин', 'Mağaza Yönet'),
(94, 'add_new_store', 'Add New Store', 'নতুন দোকান যুক্ত', 'Añadir Nueva tienda', 'إضافة مخزن جديد', 'In New Store', 'Ajouter un nouveau magasin', 'Aggiungi New Store', 'Добавить новый магазин', 'Yeni Mağaza Ekle'),
(95, 'store_name', 'Store Name', 'দোকানের নাম', 'Nombre tienda', 'اسم المتجر', 'Shop-Name', 'Nom de magasin', 'Nome del negozio', 'Название магазина', 'Dükkan adı'),
(96, 'location', 'Location', 'অবস্থান', 'Localización', 'موقع', 'Ort', 'Lieu', 'Posizione', 'Место нахождения', 'Konum'),
(97, 'on_trial', 'On Trial', 'তাকে বিচারের সম্মুখীন', 'En juicio', 'قيد التجربة', 'On Trial', 'En procès', 'In prova', 'На суде', 'Deneme'),
(98, 'view_dashboard', 'View Dashboard', 'দেখুন ড্যাশবোর্ডে', 'Ver Dashboard', 'عرض لوحة', 'Dashboard-Ansicht', 'Voir le tableau de bord', 'Visualizza Dashboard', 'Просмотр панели', 'Görünüm Paneli'),
(99, 'manage_services', 'Manage Services', 'সার্ভিস পরিচালনা', 'Administrar servicios', 'إدارة الخدمات', 'Dienstleistungen verwalten', 'Gérer les services', 'Gestione servizi', 'Управление службами', 'Hizmetler yönet'),
(100, 'edit_store', 'Edit Store', 'সম্পাদনা স্টোর', 'Editar tienda', 'تحرير مخزن', 'Bearbeiten Shop', 'Modifier magasin', 'Modifica Negozio', 'Редактировать магазин', 'Düzenleme Mağazası'),
(101, 'update_store', 'Update Store', 'আপডেট দোকান', 'Actualización tienda', 'تحديث المتجر', 'Update-Store', 'Mise à jour de magasin', 'Aggiornamento Negozio', 'Обновление магазин', 'Güncelleme Mağazası'),
(102, 'store_dashboard', 'Store Dashboard', 'স্টোর ড্যাশবোর্ড', 'Dashboard tienda', 'متجر لوحة', 'Shop-Dashboard', 'Tableau de bord de magasin', 'Conservare Dashboard', 'Магазин Панель', 'Mağaza Dashboard'),
(103, 'Store Dashboard', 'Store Dashboard', 'স্টোর ড্যাশবোর্ড', 'Dashboard tienda', 'متجر لوحة', 'Shop-Dashboard', 'Tableau de bord de magasin', 'Conservare Dashboard', 'Магазин Панель', 'Mağaza Dashboard'),
(104, 'appointment_schedule', 'Appointment Schedule', 'এপয়েন্টমেন্ট Schedule', 'Horario Nombramiento', 'تحديد موعد', 'Terminvereinbarung', 'Planning de rendez-', 'Programma appuntamento', 'Назначение Расписание', 'Randevu Takvimi'),
(105, 'appointment_details', 'Appointment Details', 'নিয়োগ বিবরণ', 'Detalles de la cita', 'تفاصيل التعيين', 'Termindetails', 'Nomination Détails', 'Appuntamento Dettagli', 'Назначение детали', 'Randevu Detayları'),
(106, 'Manage Appointments', 'Manage Appointments', 'কলকব্জা পরিচালনা', 'Administrar citas', 'إدارة التعيينات', 'Termine verwalten', 'Gérer nominations', 'Gestire Appuntamenti', 'Управление Назначения', 'Randevu yönetin'),
(107, 'add_new_appointment', 'Add New Appointment', 'নতুন এপয়েন্টমেন্ট যুক্ত', 'Añadir Nueva cita', 'إضافة موعد جديد', 'Fügen Sie Neuer Termin', 'Ajouter un nouveau rendez-vous', 'Aggiungi Nuovo appuntamento', 'Добавить Назначение', 'Yeni Randevu Ekle'),
(108, 'store_appointments', 'Store Appointments', 'দোকান কলকব্জা', 'Las citas de las tiendas', 'متجر تعيينات', 'Shop Appointments', 'Nominations de magasins', 'Negozio Appuntamenti', 'Храните Назначения', 'Mağaza Randevular'),
(109, 'pending_appointments', 'Pending Appointments', 'অপেক্ষারত অ্যাপয়েন্টমেন্টগুলো', 'Citas pendientes', 'تعيينات المعلقة', 'Schwebende Termin', 'En attente de nomination', 'Appuntamenti in sospeso', 'В ожидании назначения', 'Bekleyen Randevular'),
(110, 'add_appointment', 'Add Appointment', 'এপয়েন্টমেন্ট যুক্ত', 'Añadir cita', 'إضافة موعد', 'Termin hinzufügen', 'Ajouter Nomination', 'Aggiungere Appuntamento', 'Добавить Назначение', 'Randevu Ekle'),
(111, 'select_a_store', 'Select A Store', 'একটি দোকান নির্বাচন', 'Seleccione una tienda', 'اختر مخزن', 'Wählen Sie ein Geschäft', 'Choisir un magasin', 'Seleziona un negozio', 'Выберите магазин', 'Bir Mağaza Seçiniz'),
(112, 'select_a_store_first', 'Select A Store First', 'একটি স্টোর প্রথমটি বেছে নিন', 'Seleccione una tienda Primera', 'اختيار أول متجر', 'Wählen Sie ein Geschäft Erste', 'Sélectionnez un magasin First', 'Selezionare un negozio First', 'Выберите магазин в первую очередь', 'Bir Mağaza İlk Seçiniz'),
(113, 'select_customer', 'Select Customer', 'গ্রাহকের নির্বাচন করুন', 'Seleccione Cliente', 'حدد العملاء', 'Wählen Kunden', 'Sélectionnez la clientèle', 'Seleziona cliente', 'Выберите клиента', 'Seçin Müşteri'),
(114, 'edit_appointment', 'Edit Appointment', 'সম্পাদনা নিয়োগ', 'Modificar cita', 'تحرير تعيين', 'Termin bearbeiten', 'Modifier Nomination', 'Modifica appuntamento', 'Изменить назначение', 'Düzenle Randevu'),
(115, 'salon', 'Salon', 'বৈঠকখানা', 'Salón', 'صالون', 'Salon', 'Salon', 'Salone', 'Салон', 'Salon'),
(116, 'select_an_employee', 'Select An Employee', 'একজন কর্মী নির্বাচন', 'Seleccione Un Empleado', 'حدد موظف', 'Wählen Sie einen Mitarbeiter', 'Sélectionnez un employé', 'Selezionare un dipendente', 'Выберите сотрудника', 'Bir Çalışanı Seçiniz'),
(117, 'working_hours_of_:_', 'Working Hours Of : ', 'কাজের সময়ের: ', 'Horas de trabajo de: ', 'ساعات العمل: ', 'Arbeitszeiten: ', 'Heures de travail de: ', 'Orario di lavoro: Di ', 'Время работы: ', 'Çalışma Saatleri Of: '),
(118, 'day', 'Day', 'দিন', 'Día', 'يوم', 'Tag', 'jour', 'Giorno', 'День', 'Gün'),
(119, 'start_time', 'Start Time', 'সময় শুরু', 'Hora de inicio', 'وقت البدء', 'Startzeit', 'Heure de début', 'Ora di inizio', 'Время начала', 'Başlama zamanı'),
(120, 'end_time', 'End Time', 'শেষ সময়', 'Hora de finalización', 'نهاية الوقت', 'Endzeit', 'Heure de fin', 'Fine del tempo', 'Время окончания', 'Bitiş zamanı'),
(121, 'off_day', 'Off Day', 'বন্ধ দিন', 'Día libre', 'من يوم', 'Off Day', 'Off Jour', 'Giornataccia', 'Выходной', 'İzin günü'),
(122, 'Manage Cities', 'Manage Cities', 'শহর ও পরিচালনা', 'Administrar Ciudades', 'إدارة المدن', 'Städte verwalten', 'Gérer Villes', 'Gestire Città', 'Управление Города', 'Şehirler yönetin'),
(123, 'all_countries', 'All Countries', 'সকল দেশ', 'Todos los países', 'كل البلدان', 'Alle Länder', 'Tous les pays', 'Tutti i paesi', 'Все страны', 'Tüm ülkeler'),
(124, 'add_new_city', 'Add New City', 'নতুন শহরের করো', 'Añadir Nueva Ciudad', 'إضافة مدينة نيويورك', 'In New City', 'Ajouter une nouvelle ville', 'Aggiungi New City', 'Добавить новый город', 'Yeni Şehir ekle'),
(125, 'add_city', 'Add City', 'সিটি করো', 'Añadir Ciudad', 'إضافة سيتي', 'In der Stadt', 'Ajouter Ville', 'Aggiungere Città', 'Добавить город', 'Şehir ekle'),
(126, 'edit_city', 'Edit City', 'সম্পাদনা সিটি', 'Editar City', 'تحرير مدينة', 'Bearbeiten-Stadt', 'Modifier la ville', 'Modifica Città', 'Редактировать Город', 'Düzenleme Şehri'),
(127, 'update_city', 'Update City', 'আপডেট সিটি', 'Actualización de la ciudad', 'تحديث سيتي', 'Update-Stadt', 'Mise à jour Ville', 'Aggiornamento Città', 'Город Обновление', 'Güncelleme Şehri'),
(128, 'support_ticket', 'Support Ticket', 'সাপোর্ট টিকিট', 'Boleto de apoyo', 'تذكرة دعم', 'Support Ticket', 'Soutien Ticket', 'Support Ticket', 'Поддержка билетов', 'Destek Bilet'),
(129, 'Support Ticket', 'Support Ticket', 'সাপোর্ট টিকিট', 'Boleto de apoyo', 'تذكرة دعم', 'Support Ticket', 'Soutien Ticket', 'Support Ticket', 'Поддержка билетов', 'Destek Bilet'),
(130, 'opened', 'Opened', 'খোলা', 'Abierto', 'افتتح', 'Geöffnet', 'Ouvert', 'Aperto', 'Открыт', 'Açılmış'),
(131, 'closed', 'Closed', 'বন্ধ', 'Cerrado', 'مغلق', 'Abgeschlossen', 'Fermé', 'Chiusa', 'Закрыто', 'Kapalı'),
(132, 'title', 'Title', 'খেতাব', 'Título', 'العنوان', 'Titel', 'Titre', 'Titolo', 'заглавие', 'Başlık'),
(133, 'ticket_code', 'Ticket Code', 'টিকেট কোড', 'Código de entradas', 'كود تذكرة', 'Ticket-Code', 'Code de billet', 'Codice Biglietto', 'Код билетов', 'Bilet Kodu'),
(134, 'created_by', 'Created By', 'দ্বারা নির্মিত', 'Creado por', 'تم إنشاؤها بواسطة', 'Erstellt von', 'Créé par', 'Creato da', 'Сделано', 'Tarafından yaratıldı'),
(135, 'priority', 'Priority', 'অগ্রাধিকার', 'Prioridad', 'أفضلية', 'Priorität', 'Priorité', 'Priorità', 'Приоритет', 'Öncelik'),
(136, 'view_ticket', 'View Ticket', 'দেখুন টিকিট', 'Ver Ticket', 'عرض التذاكر', 'Ansicht Ticket', 'Voir billets', 'Visualizza Ticket', 'Посмотреть билетов', 'Görünüm Bilet'),
(137, 'dashboard', 'Dashboard', 'ড্যাশবোর্ড', 'Tablero de instrumentos', 'لوحة أجهزة القياس', 'Armaturenbrett', 'Tableau de bord', 'Cruscotto', 'Панель приборов', 'Dashboard'),
(138, 'ticket_list', 'Ticket List', 'টিকেট তালিকা', 'Lista de entradas', 'قائمة تذكرة', 'Ticket-Liste', 'Liste de billets', 'Lista Ticket', 'Список по продаже билетов', 'Bilet Listesi'),
(139, 'reply_ticket', 'Reply Ticket', 'টিকিট উত্তর', 'Responder Ticket', 'الرد التذاكر', 'Antworten Ticket', 'Répondre Ticket', 'Rispondi Ticket', 'Ответить билет', 'Bilet Cevap'),
(140, 'select_file', 'Select File', 'নথি নির্বাচন', 'Seleccione Archivo', 'اختر ملفا', 'Datei aussuchen', 'Choisir le dossier', 'Seleziona il file', 'Выберите файл', 'Dosya Seç'),
(141, 'change', 'Change', 'পরিবর্তন', 'Cambiar', 'تغيير', 'Veränderung', 'monnaie', 'Cambia', '+ Изменить', 'Değişim'),
(142, 'post_reply', 'Post Reply', 'জবাব দিন', 'Enviar respuesta', 'إضافة رد', 'Briefantwort', 'Poster une réponse', 'Invia una risposta', 'Ответить', 'Cevap Gönder'),
(143, 'ticket_summary', 'Ticket Summary', 'টিকিট সংক্ষিপ্ত', 'Resumen de entradas', 'ملخص تذكرة', 'Ticket Zusammenfassung', 'Résumé de billets', 'Riepilogo del Ticket', 'Резюме по продаже билетов', 'Bilet Özeti'),
(144, 'assigned_staff', 'Assigned Staff', 'নির্ধারিত স্টাফ', 'Asignado Personal', 'الموظفون المنتدبون', 'Zugeordnete Mitarbeiter', 'Le personnel affecté', 'Il personale assegnato', 'Назначенный персонал', 'Atanan Çalışanlar'),
(145, 'ticket_status', 'Ticket Status', 'টিকেট অবস্থা', 'Estado del Ticket', 'الحالة تذكرة', 'Ticket-Status', 'Statut du ticket', 'Stato del Ticket', 'Статус заявки', 'Bilet Durumu'),
(146, 'ticket_priority', 'Ticket Priority', 'টিকিট অগ্রাধিকার', 'Prioridad de entradas', 'تذكرة الأولوية', 'Ticket-Priorität', 'Ticket priorité', 'Priorità Ticket', 'Приоритет по продаже билетов', 'Bilet Önceliği'),
(147, 'assign_staff', 'Assign Staff', 'স্টাফ ধার্য', 'Asignar personal', 'تعيين الموظفين', 'Weisen Sie Mitarbeiter', 'Affecter du personnel', 'Assegnare personale', 'Связать персонал', 'Çalışanlar atama'),
(148, 'update_ticket_status', 'Update Ticket Status', 'আপডেট টিকেট অবস্থা', 'Venta de entradas Actualización de estado', 'حالة التحديث التذاكر', 'Update Ticketstatus', 'Mise à jour Statut du ticket', 'Status Update Ticket', 'Обновление статуса билетов', 'Güncelleme Bilet Durumu'),
(149, 'assign_staff_for_ticket', 'Assign Staff For Ticket', 'টিকেটের দাম স্টাফ ধার্য', 'Asignar personal para entradas', 'تعيين موظفين للحصول على التذاكر', 'Bedienstete mit Ticket', 'Affecter du personnel pour un billet', 'Assegnare personale per Ticket', 'Связать персонал за билет', 'Bilet için Çalışanlar atama'),
(150, 'support_staff', 'Support Staff', 'সাপোর্ট স্টাফ', 'Personal de apoyo', 'فريق الدعم', 'Support-Mitarbeiter', 'Le personnel de soutien', 'Personale di supporto', 'Вспомогательный персонал', 'Destek personeli'),
(151, 'select_a_staff', 'Select A Staff', 'স্টাফ নির্বাচন', 'Seleccione un Estado Mayor', 'اختيار الموظفين', 'Wählen Sie A Staff', 'Sélectionnez Un personnel', 'Scegli una Personale', 'Выберите штат', 'A Personel Seçiniz'),
(152, 'notify_staff', 'Notify Staff', 'কর্মীদের অবহিত', 'Notificar al personal', 'إبلاغ الموظفين', 'Benachrichtigen Sie Mitarbeiter', 'Informer le personnel', 'Notifica personale', 'Сообщите персонал', 'Çalışanlar bildir'),
(153, 'update_status', 'Update Status', 'আপডেট স্থিতি', 'Actualizar estado', 'تحديث الحالة', 'Aktualisierungsstatus', 'Mettre à jour le statut', 'Aggiorna stato', 'Обновить состояние', 'Güncelleme durumu'),
(154, 'store_owner', 'Store Owner', 'দোকান মালিক', 'Dueño de la tienda', 'صاحب متجر', 'Ladenbesitzer', 'Propriétaire du magasin', 'Padrone del negozio', 'Владелец магазина', 'Dükkan sahibi'),
(155, 'manage_invoice', 'Manage Invoice', 'চালান পরিচালনা', 'Administrar Factura', 'إدارة الفاتورة', 'Rechnung verwalten', 'Gérer facture', 'Gestire Fattura', 'Управление Счет', 'Fatura yönetin'),
(156, 'Manage Invoice', 'Manage Invoice', 'চালান পরিচালনা', 'Administrar Factura', 'إدارة الفاتورة', 'Rechnung verwalten', 'Gérer facture', 'Gestire Fattura', 'Управление Счет', 'Fatura yönetin'),
(157, 'invoice_number', 'Invoice Number', 'চালান নম্বর', 'Número de factura', 'رقم الفاتورة', 'Rechnungsnummer', 'Numéro de facture', 'Numero di fattura', 'Номер счета', 'Fatura numarası'),
(158, 'owner', 'Owner', 'মালিক', 'Propietario', 'مالك', 'Inhaber', 'Propriétaire', 'Proprietario', 'Владелец', 'Mal sahibi'),
(159, 'store_status', 'Store Status', 'স্থিতি', 'Status tienda', 'مخزن الحالة', 'Shop-Status', 'Statut magasin', 'Negozio di stato', 'Магазин Статус', 'Mağaza Durumu'),
(160, 'amount', 'Amount', 'পরিমাণ', 'Cantidad', 'كمية', 'Menge', 'Montant', 'Importo', 'Количество', 'Miktar'),
(161, 'payment_date', 'Payment Date', 'টাকা প্রদানের তারিখ', 'Día de pago', 'تاريخ الدفع', 'Zahlungsdatum', 'Date de paiement', 'Data di pagamento', 'Дата оплаты', 'Ödeme tarihi'),
(162, 'unpaid', 'Unpaid', 'অবৈতনিক', 'No pagado', 'غير مدفوع', 'Unbezahlt', 'Non payé', 'Non pagato', 'Неоплаченный', 'Ödenmemiş'),
(163, 'view_/_print', 'View / Print', 'দেখুন / প্রিন্ট', 'Ver / Imprimir', 'عرض / طباعة', 'View / Print', 'Voir / Imprimer', 'Visualizza / Stampa', 'Посмотреть / Распечатать', 'Görünüm / Baskı'),
(164, 'paid', 'Paid', 'প্রদত্ত', 'Pagado', 'دفع', 'Bezahlt', 'Payé', 'Pagato', 'Платный', 'Ücretli'),
(165, 'payment_to', 'Payment To', 'পরিশোদ করা', 'Pago Para', 'دفع ل', 'Zahlung Um', 'Paiement à', 'pagamento a', 'Оплата Для', 'Için ödeme'),
(166, 'bill_to', 'Bill To', 'বিল', 'Cobrar a', 'فاتورة الى', 'Gesetzesentwurf für', 'Facturer', 'Fatturare a', 'Плательщик', 'Ya fatura edilecek'),
(167, 'phone_:_', 'Phone : ', 'ফোন: ', 'Teléfono : ', 'الهاتف : ', 'Telefon : ', 'Téléphone : ', 'Telefono : ', 'Телефон : ', 'Telefon : '),
(168, 'invoice_entries', 'Invoice Entries', 'চালান দাখিলা', 'Entradas Factura', 'فاتورة مقالات', 'Rechnungs Einträge', 'Entrées facture', 'Voci della fattura', 'Счет-фактура Записи', 'Fatura Girişler'),
(169, 'price', 'Price', 'মূল্য', 'Precio', 'السعر', 'Preis', 'Prix', 'Prezzo', 'Цена', 'Fiyat'),
(170, 'grand_total', 'Grand Total', 'সর্বমোট', 'Gran total', 'المجموع الإجمالي', 'Endsumme', 'somme finale', 'Somma totale', 'Общая сумма', 'Genel Toplam'),
(171, 'System Settings', 'System Settings', 'পদ্ধতি নির্ধারণ', 'Ajustes del sistema', 'اعدادات النظام', 'Systemeinstellungen', 'Les paramètres du système', 'Impostazioni di sistema', 'Настройки системы', 'Sistem ayarları'),
(172, 'system_title', 'System Title', 'সিস্টেম শিরোনাম', 'Sistema Título', 'نظام العنوان', 'System Titel', 'Système titre', 'Titolo di sistema', 'Система Название', 'Sistem Başlığı'),
(173, 'text_align', 'Text Align', 'টেক্সট সারিবদ্ধ', 'Texto Alinear', 'محاذاة النص', 'Text Align', 'Text Align', 'Allinea il testo', 'Text Align', 'Metin Hizala'),
(174, 'language', 'Language', 'ভাষা', 'idioma', 'لغة', 'Sprache', 'Langue', 'Lingua', 'Язык', 'Dil'),
(175, 'store_price', 'Store Price', 'দোকান মূল্য', 'Tienda Precio', 'مخزن الأسعار', 'Shop Preis', 'Magasin Prix', 'Store Prezzo', 'Магазин Цена', 'Mağaza Fiyatı'),
(176, 'store_trial_period_(days)', 'Store Trial Period (days)', 'স্টোর ট্রায়াল কাল (দিন)', 'Tienda periodo de prueba (días)', 'مخزن الفترة التجريبية (أيام)', 'Shop Probezeit (Tage)', 'Magasin période d''essai (jours)', 'Conservare periodo di prova (giorni)', 'Магазин Испытательный срок (дни)', 'Mağaza Deneme Süresi (gün)'),
(177, 'nearby_search_radius', 'Nearby Search Radius', 'কাছাকাছি অনুসন্ধান ব্যাসার্ধ', 'Cerca de la Radio de Búsqueda', 'ابحث عن أنصاف المدى القريب', 'In der Nähe suchen Radius', 'Recherche à proximité Rayon', 'Nelle vicinanze Raggio di ricerca', 'Соседние Радиус поиска', 'Yakınlardaki Arama Radius'),
(178, 'system_currency', 'System Currency', 'সিস্টেম মুদ্রা', 'Moneda del sistema', 'نظام العملات', 'Systemwährung', 'Système devise', 'Sistema di valuta', 'Система валют', 'Sistem Para'),
(179, 'paypal_email', 'Paypal Email', 'PayPal এর ইমেইল', 'e-mail de Paypal', 'باي بال البريد الإلكتروني', 'PayPal E-Mail', 'Email Paypal', 'Paypal Email', 'Paypal E-mail', 'Paypal E-posta'),
(180, 'stripe_secret_key', 'Stripe Secret Key', 'ডোরা সিক্রেট কী', 'Raya clave secreta', 'شريط سر مفتاح', 'Stripe Secret Key', 'Stripe Secret Key', 'Stripe chiave segreta', 'Полоса Секретный ключ', 'Çizgili Gizli Anahtarı'),
(181, 'stripe_publishable_key', 'Stripe Publishable Key', 'ডোরা প্রকাশযোগ্য কী', 'Raya Publicable clave', 'شريط للنشر مفتاح', 'Stripe Veröffentlichbar Key', 'Stripe Publiable clé', 'Stripe Pubblicabile Chiave', 'Полоса опубликованию ключ', 'Çizgili Yayınlanabilir Key'),
(182, 'nexmo_api_key', 'Nexmo Api Key', 'Nexmo API কী', 'Nexmo Api Key', 'Nexmo API مفتاح', 'Nexmo API-Schlüssel', 'Nexmo Api Key', 'Nexmo Api Chiave', 'Nexmo ключ API', 'Nexmo Api Key'),
(183, 'nexmo_secret_key', 'Nexmo Secret Key', 'Nexmo সিক্রেট কী', 'Nexmo clave secreta', 'Nexmo سر مفتاح', 'Nexmo Secret Key', 'Nexmo Secret Key', 'Nexmo chiave segreta', 'Nexmo Секретный ключ', 'Nexmo Gizli Anahtarı'),
(184, 'save', 'Save', 'সংরক্ষণ', 'Ahorrar', 'حفظ', 'sparen', 'Conserver', 'Salvare', 'Сохранить', 'Kaydet'),
(185, 'upload_logo', 'Upload Logo', 'আপলোড লোগো', 'Subir Logo', 'تحميل الشعار', 'Logo hochladen', 'Upload Logo', 'Upload Logo', 'Загрузить логотип', 'Yükleme Logosu'),
(186, 'upload', 'Upload', 'আপলোড', 'Subir', 'الرفع', 'Hochladen', 'Télécharger', 'Caricare', 'Загрузить', 'Yükleme'),
(187, 'email_template_settings', 'Email Template Settings', 'ইমেইল টেমপ্লেট সেটিংস', 'Configuración de plantilla de correo electrónico', 'إعدادات قالب البريد الإلكتروني', 'E-Mail-Template-Einstellungen', 'Paramètres Email Template', 'Impostazioni e-mail dei modelli', 'Настройки электронной почты шаблона', 'E-posta Şablon Ayarları'),
(188, 'Email Template Settings', 'Email Template Settings', 'ইমেইল টেমপ্লেট সেটিংস', 'Configuración de plantilla de correo electrónico', 'إعدادات قالب البريد الإلكتروني', 'E-Mail-Template-Einstellungen', 'Paramètres Email Template', 'Impostazioni e-mail dei modelli', 'Настройки электронной почты шаблона', 'E-posta Şablon Ayarları'),
(189, 'new_support_staff_account_opening', 'New Support Staff Account Opening', 'নিউ সাপোর্ট স্টাফ অ্যাকাউন্ট খোলা', 'Cuenta Personal Nueva Apoyo Apertura', 'فتح حساب الموظفين دعم جديد', 'Neue Support Staff Kontoeröffnung', 'Nouvelle prise en charge compte personnel ouverture', 'Nuovo supporto del personale di apertura conto', 'Новый Персонал Открытие счета', 'Yeni Destek Elemanı Hesap Açma'),
(190, 'new_employee_account_opening', 'New Employee Account Opening', 'নতুন কর্মী অ্যাকাউন্ট খোলা', 'Cuenta Nueva Empleado Apertura', 'فتح حساب الموظف الجديد', 'Neuer Mitarbeiter Kontoeröffnung', 'Nouveau compte Ouverture employés', 'Nuovo account dei dipendenti di apertura', 'Новый сотрудник Открытие счета', 'Yeni Personel Hesap Açma'),
(191, 'store_owner_account_approval', 'Store Owner Account Approval', 'মালিকের অ্যাকাউন্ট অনুমোদন সঞ্চয়', 'Guarde titular de la cuenta de Aprobación', 'صاحب المحل الموافقة حساب', 'Ladenbesitzer Konto Approval', 'Stockez compte l''approbation du propriétaire', 'Titolare del Negozio account Approvazione', 'Храните владелец счета утверждении', 'Sahibi Hesap Onayı Mağaza'),
(192, 'store_owner_account_denial', 'Store Owner Account Denial', 'মালিক অ্যাকউন্টে অস্বীকার সঞ্চয়', 'Guarde titular de la cuenta Negación', 'صاحب المحل حساب الحرمان', 'Ladenbesitzer Konto Denial', 'Stocker titulaire du compte Déni', 'Titolare del Negozio account Denial', 'Храните владелец счета отказ', 'Sahibi Hesap Reddi Mağaza'),
(193, 'customer_account_approval', 'Customer Account Approval', 'গ্রাহক অ্যাকাউন্ট অনুমোদন', 'Cuenta Aprobación Cliente', 'العميل الموافقة حساب', 'Kundenkonto Approval', 'Compte client Approbation', 'Clienti Conto Approvazione', 'Утверждение учетной записи клиента', 'Müşteri Hesap Onayı'),
(194, 'customer_account_denial', 'Customer Account Denial', 'গ্রাহকের অ্যাকাউন্ট অস্বীকার', 'Cliente Cuenta Negación', 'حساب العميل الحرمان', 'Kundenkonto Denial', 'Compte client Déni', 'Clienti Conto Denial', 'Заказчик аккаунт Отказ', 'Müşteri Hesap Reddi'),
(195, 'customer_appointment_approval', 'Customer Appointment Approval', 'গ্রাহক নিয়োগ অনুমোদন', 'Aprobación Nombramiento Cliente', 'تعيين العميل الموافقة', 'Kundentermin Approval', 'Nomination à la clientèle Approbation', 'Customer Appointment Approvazione', 'Назначение клиентов Утверждение', 'Müşteri Randevu Onayı'),
(196, 'customer_appointment_denial', 'Customer Appointment Denial', 'গ্রাহক অ্যাপয়েন্টমেন্ট অস্বীকার', 'Nombramiento Cliente Negación', 'تعيين العملاء الحرمان', 'Kundentermin Denial', 'Nomination à la clientèle Déni', 'Customer Appointment Denial', 'Назначение клиентов Отказ', 'Müşteri Randevu Reddi'),
(197, 'new_support_ticket_notify_admin', 'New Support Ticket Notify Admin', 'নতুন সমর্থন টিকেট অ্যাডমিন অবহিত', 'Nuevo Ticket Avisar de administración', 'تذكرة دعم جديد يخطر الادارية', 'Neue Support Ticket Benachrichtigen Admin', 'Ticket support Prévenez Administrateur', 'Nuovo supporto Ticket Notifica Admin', 'Поддержка новых билетов Сообщите Администратор', 'Yeni Destek Bilet Yönetici bildir'),
(198, 'support_ticket_assign_staff', 'Support Ticket Assign Staff', 'সাপোর্ট টিকিট বরাদ্দ স্টাফ', 'Ticket Asignar personal', 'تذكرة دعم تعيين الموظفين', 'Support Ticket zuweisen Mitarbeiter', 'Soutien Ticket affecter du personnel', 'Support Ticket Assegna Personale', 'Поддержка билетов Назначить персонал', 'Destek Bilet Ata Personel'),
(199, 'password_reset_confirmation', 'Password Reset Confirmation', 'পাসওয়ার্ড রিসেট নিশ্চিতকরণ', 'Restablecer contraseña Confirmación', 'إعادة تعيين كلمة المرور تأكيد', 'Password Reset Confirmation', 'Password Reset Confirmation', 'Password Reset Conferma', 'Сброс пароля Подтверждение', 'Parola Sıfırlama Onayı'),
(200, 'new_customer_account_opening', 'New Customer Account Opening', 'নতুন গ্রাহক অ্যাকাউন্ট খোলা', 'Nueva apertura de cuenta de cliente', 'جديد فتح حساب العملاء', 'Neuer Kunde Kontoeröffnung', 'Nouvelle ouverture de compte client', 'Nuova apertura Account Cliente', 'Новый клиент Открытие счета', 'Yeni Müşteri Hesap Açma'),
(201, 'new_store_owner_account_registration', 'New Store Owner Account Registration', 'নতুন দোকান মালিক অ্যাকাউন্ট নিবন্ধন', 'Nueva tienda titular de la cuenta de registro', 'التسجيل المتجر الجديد صاحب الحساب', 'New Store Owner Konto Registration', 'Nouveau magasin titulaire du compte Inscription', 'New Store account Registrazione proprietario', 'Новый владелец магазина аккаунт Регистрация', 'Yeni Mağaza Sahibi Hesap Kaydı');
INSERT INTO `bk_language` (`phrase_id`, `phrase`, `en`, `bn`, `es`, `ar`, `de`, `fr`, `it`, `ru`, `tr`) VALUES
(202, 'email_subject', 'Email Subject', 'ইমেইল বিষয়', 'Asunto del correo', 'موضوع البريد الإلكتروني', 'E-Mail Betreff', 'Sujet du courriel', 'Oggetto dell''email', 'Тема письма', 'E-posta konu'),
(203, 'email_body', 'Email Body', 'ইমেলের', 'Cuerpo del correo electronico', 'البريد الإلكتروني الجسم', 'Email Körper', 'Email du corps', 'Email Corpo', 'E-mail тела', 'E-posta Vücut'),
(204, 'save_template', 'Save Template', 'Save Template এ', 'Guardar plantilla', 'حفظ القالب', 'Vorlage speichern', 'Enregistrer le modèle', 'Salva modello', 'Сохранить шаблон', 'Şablonu Kaydet'),
(205, 'manage_language', 'Manage Language', 'ভাষা পরিচালনা', 'Administrar Idioma', 'إدارة اللغة', 'Sprache verwalten', 'Gérer Langue', 'Gestire Lingua', 'Управление Язык', 'Dil yönetin'),
(206, 'Manage Language', 'Manage Language', 'ভাষা পরিচালনা', 'Administrar Idioma', 'إدارة اللغة', 'Sprache verwalten', 'Gérer Langue', 'Gestire Lingua', 'Управление Язык', 'Dil yönetin'),
(207, 'language_list', 'Language List', 'নতুন ভাষাটি তালিকায় আগে', 'Lista Idioma', 'قائمة لغة', 'Sprachenliste', 'Liste de Langue', 'Elenco lingue', 'Список языков', 'Dil Listesi'),
(208, 'add_phrase', 'Add Phrase', 'শব্দবন্ধ যোগ', 'Añadir Frase', 'إضافة العبارة', 'Phrase hinzufügen', 'Ajouter Phrase', 'Aggiungere Frase', 'Добавить фразу', 'Cümle ekle'),
(209, 'add_language', 'Add Language', 'নতুন ভাষা যোগ করা', 'Agregar idioma', 'إضافة اللغة', 'Sprache hinzufügen', 'Ajouter une langue', 'Aggiungi lingua', 'Добавить язык', 'Dil ekle'),
(210, 'option', 'Option', 'পছন্দ', 'Opción', 'خيار', 'Option', 'Option', 'Opzione', 'Вариант', 'Seçenek'),
(211, 'edit_phrase', 'Edit Phrase', 'সম্পাদনা শব্দবন্ধ', 'Editar Frase', 'تحرير العبارة', 'Phrase bearbeiten', 'Modifier Phrase', 'Modifica Frase', 'Редактировать Фраза', 'Düzenleme Cümle'),
(212, 'delete_language', 'Delete Language', 'ভাষা মুছে', 'Eliminar Idioma', 'حذف اللغة', 'Sprache löschen', 'Supprimer Langue', 'Eliminare Lingua', 'Удалить Язык', 'Dil Sil'),
(213, 'phrase', 'Phrase', 'শব্দবন্ধ', 'Frase', 'العبارة', 'Phrase', 'Phrase', 'Frase', 'Фраза', 'İfade'),
(214, 'manage_profile', 'Manage Profile', 'অমিমাংসীত সংস্করণ লগ', 'Administrar perfil', 'إدارة الملف', 'Profil verwalten', 'Gérer le profil', 'Gestisci profilo', 'Управление профиля', 'Profilinizi Yönetin'),
(215, 'Manage Profile', 'Manage Profile', 'অমিমাংসীত সংস্করণ লগ', 'Administrar perfil', 'إدارة الملف', 'Profil verwalten', 'Gérer le profil', 'Gestisci profilo', 'Управление профиля', 'Profilinizi Yönetin'),
(216, 'edit_profile', 'Edit Profile', 'জীবন বৃত্তান্ত সম্পাদনা', 'Editar perfil', 'تعديل الملف الشخصي', 'Profil bearbeiten', 'Modifier le profil', 'Modifica Profilo', 'Редактировать профиль', 'Profili Düzenle'),
(217, 'update_profile', 'Update Profile', 'প্রফাইল হালনাগাদ', 'Actualización del perfil', 'تحديث الملف', 'Profil aktualisieren', 'Mettre à jour le profil', 'Aggiorna il profilo', 'Обновить профиль', 'Profili güncelle'),
(218, 'change_password', 'Change Password', 'পাসওয়ার্ড পরিবর্তন করুন', 'Cambiar la contraseña', 'تغيير كلمة السر', 'Kennwort ändern', 'Changer le mot de passe', 'Cambiare la password', 'Изменить пароль', 'Şifre değiştir'),
(219, 'current_password', 'Current Password', 'বর্তমান পাসওয়ার্ড', 'contraseña actual', 'كلمة السر الحالية', 'Aktuelles Passwort', 'Mot de passe actuel', 'Password attuale', 'текущий пароль', 'Şimdiki Şifre'),
(220, 'new_password', 'New Password', 'নতুন পাসওয়ার্ড', 'nueva contraseña', 'كلمة سر جديدة', 'Neues Kennwort', 'nouveau mot de passe', 'nuova password', 'новый пароль', 'Yeni Şifre'),
(221, 'confirm_new_password', 'Confirm New Password', 'নিশ্চিত কর নতুন গোপননম্বর', 'Confirmar nueva contraseña', 'تأكيد كلمة السر الجديدة', 'Bestätige neues Passwort', 'Confirmer le nouveau mot de passe', 'Conferma la nuova password', 'Подтвердите новый пароль', 'Yeni şifreyi onayla'),
(222, 'update_password', 'Update Password', 'আপডেট পাসওয়ার্ড', 'Actualiza contraseña', 'تحديث كلمة المرور', 'Passwort aktualisieren', 'Mise à jour Mot de passe', 'Aggiorna password', 'Обновление Пароль', 'Güncelleme Şifre'),
(223, 'support_dashboard', 'Support Dashboard', 'সাপোর্ট ড্যাশবোর্ড', 'Soporte del salpicadero', 'دعم لوحة', 'Unterstützungsarmaturenbrett', 'Soutien Dashboard', 'Dashboard di supporto', 'Поддержка Dashboard', 'Destek Paneli'),
(224, 'Support Dashboard', 'Support Dashboard', 'সাপোর্ট ড্যাশবোর্ড', 'Soporte del salpicadero', 'دعم لوحة', 'Unterstützungsarmaturenbrett', 'Soutien Dashboard', 'Dashboard di supporto', 'Поддержка Dashboard', 'Destek Paneli'),
(225, 'assigned_support_tickets', 'Assigned Support Tickets', 'নির্ধারিত সাপোর্ট টিকিট', 'Entradas de apoyo asignado', 'تذاكر الدعم الفني المخصصة', 'Assigned Support Tickets', 'Billets de soutien affectés', 'Biglietti di sostegno assegnati', 'Назначенные Поддержка Билеты', 'Atanan Destek Bildirimleri'),
(226, 'opened_support_tickets', 'Opened Support Tickets', 'খোলা সাপোর্ট টিকিট', 'Tickets de Soporte Abierto', 'تذاكر الدعم الفني فتح', 'Eröffnet Support Tickets', 'Billets de soutien ouverts', 'Biglietti di supporto Aperto', 'Открытые Поддержка Билеты', 'Açılmış Destek Bildirimleri'),
(227, 'closed_support_tickets', 'Closed Support Tickets', 'বন্ধ সাপোর্ট টিকিট', 'Tickets de Soporte cerrados', 'تذاكر الدعم الفني مغلقة', 'Geschlossen Support Tickets', 'Billets de support fermé', 'Biglietti di supporto Chiuso', 'Закрытые Поддержка Билеты', 'Kapalı Destek Bildirimleri'),
(228, 'create_store', 'Create Store', 'স্টোর তৈরি করুন', 'Crear tienda', 'إنشاء مخزن', 'Shop erstellen', 'Créer magasin', 'Creare Negozio', 'Создать магазин', 'Mağaza oluştur'),
(229, 'support_tickets', 'Support Tickets', 'সাপোর্ট টিকিট', 'Tickets de Soporte', 'تذاكر الدعم الفني', 'Support Tickets', 'Billets de soutien', 'Biglietti di supporto', 'Поддержка Билеты', 'Destek İstekleri'),
(230, 'create_ticket', 'Create Ticket', 'টিকেট নির্মাণ', 'Crear Ticket', 'إنشاء تذكرة', 'Ticket erstellen', 'Créer un Ticket', 'Crea Ticket', 'Создать билетов', 'Bilet Oluştur'),
(231, 'store_settings', 'Store Settings', 'দোকান সেটিংস', 'Ajustes de almacenamiento', 'مخزن إعدادات', 'Einstellungen speichern', 'Paramètres de magasins', 'Impostazioni Negozio', 'Настройки магазин', 'Mağaza Ayarları'),
(232, 'Manage Stores', 'Manage Stores', 'দোকানে পরিচালনা', 'Administrar Tiendas', 'إدارة المخازن', 'Stores verwalten', 'Gérer Magasins', 'Gestire Negozi', 'Управление Магазины', 'Mağazalar yönetin'),
(233, 'new_store', 'New Store', 'নতুন স্টোর', 'Nueva tienda', 'المتجر الجديد', 'New Store', 'Nouveau magasin', 'New Store', 'Новый магазин', 'Yeni Mağaza'),
(234, 'New Store', 'New Store', 'নতুন স্টোর', 'Nueva tienda', 'المتجر الجديد', 'New Store', 'Nouveau magasin', 'New Store', 'Новый магазин', 'Yeni Mağaza'),
(235, 'add_store', 'Add Store', 'দোকানযুক্ত', 'Añadir tienda', 'إضافة إلى مخزن ل', 'In-Store', 'Ajouter magasin', 'Aggiungere Negozio', 'Добавить магазин', 'Mağaza ekle'),
(236, 'select_a_country_first', 'Select A Country First', 'একটি দেশ প্রথমটি বেছে নিন', 'Selecciona un país', 'اختر البلد أولا', 'Wählen Sie ein Land Erste', 'Sélectionnez d''abord un pays', 'Selezionare un paese prima', 'Выберите страну в первую очередь', 'Ülke Önce Seçin'),
(237, 'pay_with', 'Pay With', 'সঙ্গে দিতে', 'Pagar con', 'ادفع عن طريق', 'Bezahlen mit', 'Payer avec', 'Paga con', 'Оплатить с', 'İle ödemek'),
(238, 'bank_transfer', 'Bank Transfer', 'ব্যাংক লেনদেন', 'Transferencia bancaria', 'التحويل المصرفي', 'Banküberweisung', 'Virement', 'Bonifico', 'Банковский перевод', 'Banka transferi'),
(239, 'free_trial', 'Free Trial', 'ফ্রি ট্রায়াল', 'Prueba gratis', 'تجربة مجانية', 'Kostenlose Testversion', 'Essai gratuit', 'Prova gratuita', 'Бесплатная пробная версия', 'Ücretsiz deneme'),
(240, 'manage_servicees_of_:_', 'Manage Servicees Of : ', 'Servicees পরিচালনা: ', 'Administrar Servicees de: ', 'إدارة Servicees من: ', 'Servicees verwalten Of: ', 'Gérer Servicees de: ', 'Gestire servicees Di: ', 'Управление Servicees Из: ', 'Servicees Of Yönet: '),
(241, 'Manage Servicees Of : My Store Salon', 'Manage Servicees Of : My Store Salon', 'আমার দোকান সেলুন: এর Servicees পরিচালনা', 'Administrar Servicees De: Mi Salon Tienda', 'إدارة Servicees من: مخزن صالون بلدي', 'My Store Salon: Servicees Of verwalten', 'Gérer Servicees de: Mon Salon de magasin', 'Gestire servicees Di: mio deposito Salon', 'Управление Servicees Of: Мой магазин салон', 'Benim Mağaza Salon: Of Servicees yönetin'),
(242, 'add_new_service', 'Add New Service', 'নতুন পরিষেবা যোগ', 'Agregar nuevo servicio', 'إضافة خدمة جديدة', 'Neuen Service hinzufügen', 'Ajouter un nouveau service', 'Aggiungi nuovo servizio', 'Добавить новый сервис', 'Yeni Hizmet Ekle'),
(243, 'service_name', 'Service Name', 'কাজের নাম', 'Nombre del Servicio', 'اسم الخدمة', 'Dienstname', 'Nom du service', 'Nome del servizio', 'наименование услуги', 'hizmet adı'),
(244, 'description', 'Description', 'বিবরণ', 'Descripción', 'الوصف', 'Beschreibung', 'Description', 'Descrizione', 'Описание', 'Açıklama'),
(245, 'duration', 'Duration', 'স্থিতিকাল', 'Duración', 'المدة الزمنية', 'Dauer', 'Durée', 'Durata', 'Продолжительность', 'Süre'),
(246, 'cost', 'Cost', 'মূল্য', 'Costo', 'كلفة', 'Kosten', 'Coût', 'Costo', 'Стоимость', 'Maliyet'),
(247, 'add_service', 'Add Service', 'পরিষেবা করো', 'Añadir Servicio', 'إضافة خدمة', 'Dienst hinzufügen', 'Ajouter un service', 'Aggiungi servizio', 'Добавить службу', 'Hizmet Ekle'),
(248, 'edit_service', 'Edit Service', 'সম্পাদনা পরিষেবা', 'Editar Servicio', 'تحرير الخدمات', 'Dienst bearbeiten', 'Modifier un service', 'Modifica servizio', 'Редактировать служба', 'Düzenleme Hizmeti'),
(249, 'update_service', 'Update Service', 'আপডেট পরিষেবা', 'Servicio de actualización', 'تحديث الخدمة', 'Update Service', 'Update Service', 'Update Service', 'Служба обновления', 'Güncelleme Servisi'),
(250, 'new_employee', 'New Employee', 'নতুন কর্মকর্তা', 'Nuevo empleado', 'موظف جديد', 'Neuer Angestellter', 'Nouvel employé', 'Nuovo impiegato', 'Новый сотрудник', 'Yeni çalışan'),
(251, 'pending_store_appointments', 'Pending Store Appointments', 'দোকান কলকব্জা অপেক্ষারত', 'A la espera de citas de las tiendas', 'في انتظار مخزن تعيينات', 'Schwebende Shop Appointments', 'Dans l''attente de rendez-vous de magasins', 'In attesa Negozio Appuntamenti', 'До магазин Назначения', 'Mağaza randevular Bekleyen'),
(252, 'salon_appointment_approved_successfully', 'Salon Appointment Approved Successfully', 'সেলুন নিয়োগ সফলভাবে অনুমোদিত', 'Cita del salón Aprobado con éxito', 'صالون تعيين المعتمدة بنجاح', 'Salon-Verabredung Erfolgreich Genehmigt', 'Salon Nomination Approuvé succès', 'Salon Appuntamento Approvato con successo', 'Салон Назначение Утверждено успешно', 'Salon Randevu Başarıyla Onaylı'),
(253, 'Manage Invoices', 'Manage Invoices', 'চালানে পরিচালনা', 'Manejo de facturas', 'إدارة الفواتير', 'Rechnungen verwalten', 'Gérer factures', 'Gestire fatture', 'Управление Счета', 'Faturalar yönetin'),
(254, 'all_invoices', 'All Invoices', 'সব চালানে', 'Todas las facturas', 'جميع الفواتير', 'Alle Rechnungen', 'Toutes les factures', 'Tutte le fatture', 'Все счета-фактуры', 'Tüm Faturalar'),
(255, 'paid_invoices', 'Paid Invoices', 'প্রদত্ত চালান', 'Facturas pagadas', 'دفع الفواتير', 'Bezahlte Rechnungen', 'Factures payées', 'Fatture a pagamento', 'Платные Счета', 'Ücretli Faturalar'),
(256, 'unpaid_invoices', 'Unpaid Invoices', 'অবৈতনিক চালানে', 'Facturas pendientes de pago', 'فواتير غير مدفوعة', 'Unbezahlte Rechnungen', 'Factures impayées', 'Fatture non pagate', 'Неоплаченные счета', 'Ödenmemiş Faturalar'),
(257, 'issue_date', 'Issue Date', 'প্রদানের তারিখ', 'Fecha de asunto', 'تاريخ الاصدار', 'Ausgabedatum', 'Date d''émission', 'Data di rilascio', 'Дата выпуска', 'Veriliş tarihi'),
(258, 'total_income_:_', 'Total Income : ', 'মোট আয় : ', 'Ingresos totales : ', 'إجمالي الدخل : ', 'Gesamteinkommen : ', 'Revenu total : ', 'Reddito totale : ', 'Общая прибыль : ', 'Toplam gelir : '),
(259, 'total_due_:_', 'Total Due : ', 'মোট বাকি : ', 'Debido Total: ', 'الاجمالي المستحق : ', 'Insgesamt Due: ', 'Total dû : ', 'Totale dovuto : ', 'Итого В: ', 'Toplam nedeniyle: '),
(260, 'issue_date:_', 'Issue Date: ', 'প্রদানের তারিখ: ', 'Fecha de asunto: ', 'تاريخ الاصدار: ', 'Ausgabedatum: ', 'Date d''émission: ', 'Data di rilascio: ', 'Дата выпуска: ', 'Veriliş tarihi: '),
(261, 'payment_date_:_', 'Payment Date : ', 'টাকা প্রদানের তারিখ : ', 'Día de pago : ', 'تاريخ الدفع : ', 'Zahlungsdatum : ', 'Date de paiement : ', 'Data di pagamento : ', 'Дата оплаты: ', 'Ödeme tarihi : '),
(262, 'email_:_', 'Email : ', 'ইমেইল: ', 'Email : ', 'البريد الإلكتروني : ', 'Email : ', 'Email : ', 'E-mail : ', 'Эл. адрес : ', 'E-posta: '),
(263, 'create_new_ticket', 'Create New Ticket', 'নতুন টিকেট তৈরি', 'Crear nuevo Ticket', 'إنشاء تذكرة جديدة', 'Neues Ticket', 'Créer un nouveau ticket', 'Crea nuovo Ticket', 'Создать новый ярлык', 'Yeni Bilet Oluştur'),
(264, 'Create New Ticket', 'Create New Ticket', 'নতুন টিকেট তৈরি', 'Crear nuevo Ticket', 'إنشاء تذكرة جديدة', 'Neues Ticket', 'Créer un nouveau ticket', 'Crea nuovo Ticket', 'Создать новый ярлык', 'Yeni Bilet Oluştur'),
(265, 'ticket_form', 'Ticket Form', 'টিকেট ফর্ম', 'Formulario de entradas', 'نموذج تذكرة', 'Ticket-Formular', 'Formulaire de billets', 'Modulo Ticket', 'Форма билетов', 'Bilet Formu'),
(266, 'ticket_title', 'Ticket Title', 'টিকিট শিরোনাম', 'Venta de entradas Título', 'عنوان تذكرة', 'Ticket-Titel', 'Ticket Titre', 'Titolo Ticket', 'Название билетов', 'Bilet Başlığı'),
(267, 'low', 'Low', 'কম', 'Bajo', 'منخفض', 'Niedrig', 'Bas', 'Basso', 'Низкий', 'Düşük'),
(268, 'medium', 'Medium', 'মাঝারি', 'Medio', 'متوسط', 'Medium', 'Moyen', 'Media', 'средний', 'Orta'),
(269, 'high', 'High', 'উচ্চ', 'Alto', 'عالي', 'Hoch', 'Haut', 'Alto', 'Высокая', 'Yüksek'),
(270, 'choose', 'Choose', 'নির্বাচন করা', 'Elegir', 'أختر', 'Wählen', 'Choisir', 'Scegliere', 'выберите', 'Seçin'),
(271, 'submit_support_ticket', 'Submit Support Ticket', 'সাপোর্ট টিকেট জমা দিন', 'Enviar Ticket', 'إرسال تذكرة دعم', 'Submit Support Ticket', 'Un ticket support', 'Invia Support Ticket', 'Разместить в службу поддержки', 'Destek Bildirim Gönder'),
(272, 'Store Settings', 'Store Settings', 'দোকান সেটিংস', 'Ajustes de almacenamiento', 'مخزن إعدادات', 'Einstellungen speichern', 'Paramètres de magasins', 'Impostazioni Negozio', 'Настройки магазин', 'Mağaza Ayarları'),
(273, 'theme_settings', 'Theme Settings', 'থিম সেটিং', 'Ajustes de tema', 'إعدادات موضوع', 'Themen Einstellungen', 'Réglage des thèmes', 'Impostazioni tema', 'Настройки темы', 'Tema ayarları'),
(274, 'default', 'Default', 'ডিফল্ট', 'Defecto', 'افتراضي', 'Standard', 'Défaut', 'Predefinito', 'По умолчанию', 'Standart'),
(275, 'select_theme', 'Select Theme', 'থিম নির্বাচন কর', 'Seleccione el tema', 'اختر الموضوع', 'Thema wählen', 'Sélectionnez Thème', 'Seleziona il tema', 'Выберите тему', 'Seç Tema'),
(276, 'select_a_theme_to_make_changes', 'Select A Theme To Make Changes', 'পরিবর্তন করার জন্য একটি থিম নির্বাচন করুন', 'Seleccione un tema para hacer cambios', 'اختيار موضوع لإجراء تغييرات', 'Wählen Sie ein Thema um Änderungen vorzunehmen', 'Sélectionnez un thème pour effectuer des modifications', 'Selezionare un tema di apportare modifiche', 'Выберите тему, чтобы внести изменения', 'Değişiklikleri yapmak bir Tema Seç'),
(277, 'logo_image', 'Logo Image', 'প্রতীক ছবি', 'Logotipo', 'صورة الشعار', 'Logo Bild', 'Image logo', 'Logo Immagine', 'Изображение логотипа', 'Logo resmi'),
(278, 'select_a_country', 'Select A Country', 'একটি দেশ নির্বাচন কর', 'Seleccione un país', 'اختر البلد', 'Wähle ein Land', 'Choisissez un pays', 'Seleziona un Paese', 'Выберите страну', 'Bir ülke seçin'),
(279, 'employee_dashboard', 'Employee Dashboard', 'কর্মচারী ড্যাশবোর্ড', 'Dashboard Empleado', 'لوحة موظف', 'Mitarbeiter-Dashboard', 'Tableau de bord des employés', 'Dashboard dei dipendenti', 'Сотрудник Панель', 'Çalışan Dashboard'),
(280, 'Employee Dashboard', 'Employee Dashboard', 'কর্মচারী ড্যাশবোর্ড', 'Dashboard Empleado', 'لوحة موظف', 'Mitarbeiter-Dashboard', 'Tableau de bord des employés', 'Dashboard dei dipendenti', 'Сотрудник Панель', 'Çalışan Dashboard'),
(281, 'salon_appointments', 'Salon Appointments', 'পার্লার কলকব্জা', 'Citas Salon', 'تعيينات صالون', 'Salon Appointments', 'Nominations de Salon', 'Appuntamenti Salon', 'Салон Назначения', 'Salon Randevular'),
(282, 'pending_salon_appointments', 'Pending Salon Appointments', 'পার্লার কলকব্জা অপেক্ষারত', 'A la espera de Nombramientos Salon', 'في انتظار تعيينات صالون', 'Schwebende Salon Appointments', 'Dans l''attente de rendez-vous de salon', 'In attesa Appuntamenti Salon', 'В ожидании красоты Назначения', 'Salon randevular Bekleyen'),
(283, 'manage_working_hours', 'Manage Working Hours', 'ওয়ার্কিং ঘন্টা পরিচালনা', 'Administrar Horas de Trabajo', 'إدارة ساعات العمل', 'Verwalten Arbeitsstunden', 'Gérer la durée du travail', 'Gestire orario di lavoro', 'Управление Рабочие часы', 'Çalışma Saatleri yönetin'),
(284, 'Manage Services', 'Manage Services', 'সার্ভিস পরিচালনা', 'Administrar servicios', 'إدارة الخدمات', 'Dienstleistungen verwalten', 'Gérer les services', 'Gestione servizi', 'Управление службами', 'Hizmetler yönet'),
(285, 'no_pending_appointments', 'No Pending Appointments', 'কোনও মুলতুবি অ্যাপয়েন্টমেন্টগুলো', 'No hay citas pendientes', 'لا تعيينات المعلقة', 'Keine Pending Appointments', 'Pas de rendez-vous en attente', 'No appuntamenti in sospeso', 'Нет ожидании назначения', 'Hiçbir Bekleyen Randevular'),
(286, 'working_hours', 'Working Hours', 'কর্মঘন্টা', 'Horas laborales', 'ساعات العمل', 'Arbeitsstunden', 'Heures de travail', 'Ore lavorative', 'Рабочее время', 'Çalışma saatleri'),
(287, 'Working Hours', 'Working Hours', 'কর্মঘন্টা', 'Horas laborales', 'ساعات العمل', 'Arbeitsstunden', 'Heures de travail', 'Ore lavorative', 'Рабочее время', 'Çalışma saatleri'),
(288, 'edit_working_hours', 'Edit Working Hours', 'সম্পাদনা ওয়ার্কিং ঘন্টা', 'Horas Editar Trabajo', 'ساعات تحرير العمل', 'Bearbeiten Arbeitsstunden', 'Modifier les heures de travail', 'Ore di lavoro Modifica', 'Редактировать Часы работы', 'Düzenleme Çalışma Saatleri'),
(289, 'monday', 'Monday', 'সোমবার', 'lunes', 'الإثنين', 'Montag', 'Lundi', 'Lunedi', 'понедельник', 'Pazartesi'),
(290, 'tuesday', 'Tuesday', 'মঙ্গলবার', 'martes', 'الثلاثاء', 'Dienstag', 'Mardi', 'martedì', 'вторник', 'Salı'),
(291, 'wednesday', 'Wednesday', 'বুধবার', 'miércoles', 'الأربعاء', 'Mittwoch', 'Mercredi', 'mercoledì', 'Среда', 'Çarşamba'),
(292, 'thursday', 'Thursday', 'বৃহস্পতিবার', 'jueves', 'الخميس', 'Donnerstag', 'Jeudi', 'giovedì', 'Четверг', 'Perşembe'),
(293, 'friday', 'Friday', 'শুক্রবার', 'viernes', 'الجمعة', 'Freitag', 'Vendredi', 'Venerdì', 'Пятница', 'Cuma'),
(294, 'saturday', 'Saturday', 'শনিবার', 'sábado', 'السبت', 'Samstag', 'samedi', 'Sabato', 'суббота', 'Cumartesi'),
(295, 'sunday', 'Sunday', 'রবিবার', 'domingo', 'الأحد', 'Sonntag', 'dimanche', 'Domenica', 'Воскресенье', 'Pazar'),
(296, 'customer_dashboard', 'Customer Dashboard', 'গ্রাহক ড্যাশবোর্ড', 'Dashboard Cliente', 'لوحة العملاء', 'Kunden-Dashboard', 'Tableau de bord de la clientèle', 'Dashboard cliente', 'Приборная панель клиентов', 'Müşteri Paneli'),
(297, 'Customer Dashboard', 'Customer Dashboard', 'গ্রাহক ড্যাশবোর্ড', 'Dashboard Cliente', 'لوحة العملاء', 'Kunden-Dashboard', 'Tableau de bord de la clientèle', 'Dashboard cliente', 'Приборная панель клиентов', 'Müşteri Paneli'),
(298, 'make_appointment', 'Make Appointment', 'এ্যাপয়েন্টমেন্ট', 'Concertar cita', 'حدد موعدا', 'Terminvereinbarung', 'Prendre un rendez-vous', 'Prendere appuntamento', 'Назначить встречу', 'Randevu ayarlamak'),
(299, 'my_appointments', 'My Appointments', 'আমার অ্যাপয়েন্টমেন্টগুলো', 'Mis citas', 'بلدي تعيينات', 'Meine Termine', 'Mes rendez-vous', 'I miei appuntamenti', 'Мои Назначения', 'Benim Randevular'),
(300, 'Make Appointment', 'Make Appointment', 'এ্যাপয়েন্টমেন্ট', 'Concertar cita', 'حدد موعدا', 'Terminvereinbarung', 'Prendre un rendez-vous', 'Prendere appuntamento', 'Назначить встречу', 'Randevu ayarlamak'),
(301, 'make_reservations', 'Make Reservations', 'আসন সংরক্ষণ কর', 'Hacer resevaciones', 'جعل الحجز', 'Reservieren', 'Faire des réservations', 'Accetta prenotazioni', 'Сделать предварительный заказ', 'Rezervasyon yap'),
(302, 'nearby', 'Nearby', 'নিকটবর্তী', 'Cerca', 'قريب', 'Hier in der Nähe', 'Juste à côté', 'Vicino', 'Рядом, поблизости', 'Yakınlardaki'),
(303, 'search', 'Search', 'অনুসন্ধান', 'Buscar', 'البحث عن', 'Suche', 'Recherche', 'Ricerca', 'Поиск', 'Arama'),
(304, 'make_salon_appointment', 'Make Salon Appointment', 'সেলুন এ্যাপয়েন্টমেন্ট', 'Haga la cita del salón', 'جعل صالون تعيين', 'Machen Sie Salon-Verabredung', 'Assurez Salon Rendez-vous', 'Fai Salon appuntamento', 'Сделайте салон Назначение', 'Salon Randevu olun'),
(305, 'Make Salon Appointment', 'Make Salon Appointment', 'সেলুন এ্যাপয়েন্টমেন্ট', 'Haga la cita del salón', 'جعل صالون تعيين', 'Machen Sie Salon-Verabredung', 'Assurez Salon Rendez-vous', 'Fai Salon appuntamento', 'Сделайте салон Назначение', 'Salon Randevu olun'),
(306, 'go_back', 'Go Back', 'ফিরে যাও', 'Regresa', 'عُد', 'Geh zurück', 'Retournez', 'Torna indietro', 'Возвращаться', 'Geri dön'),
(307, 'address', 'Address', 'ঠিকানা', 'Dirección', 'العنوان', 'Adresse', 'Adresse', 'Indirizzo', 'Адрес', 'Adres'),
(308, 'cancel', 'Cancel', 'বাতিল', 'Cancelar', 'إلغاء', 'Absagen', 'Annuler', 'Cancellare', 'Отмена', 'İptal'),
(309, 'phrase_list', 'Phrase List', 'শব্দসমষ্টির তালিকা', 'Lista Frase', 'قائمة العبارة', 'Phrasenliste', 'Liste phrase', 'Lista frase', 'Фраза Список', 'Öbek Listesi'),
(310, 'update_phrase', 'Update Phrase', 'আপডেট শব্দবন্ধ', 'Actualización Frase', 'تحديث العبارة', 'Update-Satz', 'Mise à jour Phrase', 'Aggiornamento Frase', 'Обновление Фраза', 'Güncelleme Cümle'),
(311, 'settings_updated', '', '', '', '', '', '', '', '', ''),
(312, 'পদ্ধতি নির্ধারণ', '', '', '', '', '', '', '', '', ''),
(317, 'expense_percentage', '', '', '', '', '', '', '', '', ''),
(318, 'income_percentage', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bk_note`
--

DROP TABLE IF EXISTS `bk_note`;
CREATE TABLE IF NOT EXISTS `bk_note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `bk_product`
--

DROP TABLE IF EXISTS `bk_product`;
CREATE TABLE IF NOT EXISTS `bk_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `product_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `price` longtext COLLATE utf8_unicode_ci NOT NULL,
  `notes` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bk_product`
--

INSERT INTO `bk_product` (`product_id`, `type`, `product_code`, `name`, `product_category_id`, `price`, `notes`) VALUES
(1, 'product', 'a9130c0db5', 'Fasttrack Wrist Watch', 2, '1499', 'Minus explicabo. Esse voluptatibus reprehenderit optio, Nam accusamus reprehenderit quis modi itaque enimmmmm'),
(2, 'service', '09f5249e4d', 'Home Delivery Service', 4, '20', 'Fugit, obcaecati in elit, ad aut soluta animi, velit, nihil do amet, in iste officiaaaaaa'),
(3, 'product', '6d5b00442d', 'iPhone 6s', 1, '700', 'Placeat, consequuntur quos magnam atque est, nostrud consequatur facilis qui cupidatat.'),
(4, 'service', '35a4427624', 'Catering Service', 4, '50', 'Molestiae consequuntur in ipsam quisquam est nobis repellendus. Ea qui fuga. Veritatis voluptas.');

-- --------------------------------------------------------

--
-- Table structure for table `bk_product_category`
--

DROP TABLE IF EXISTS `bk_product_category`;
CREATE TABLE IF NOT EXISTS `bk_product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bk_product_category`
--

INSERT INTO `bk_product_category` (`product_category_id`, `name`) VALUES
(1, 'Electronics'),
(2, 'Fashion'),
(4, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `bk_purchase`
--

DROP TABLE IF EXISTS `bk_purchase`;
CREATE TABLE IF NOT EXISTS `bk_purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `vat_id` int(11) NOT NULL,
  `discount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  `due_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bk_purchase`
--

INSERT INTO `bk_purchase` (`purchase_id`, `purchase_code`, `supplier_id`, `account_id`, `amount`, `vat_id`, `discount`, `creation_date`, `due_date`) VALUES
(1, '0dceace19d', 6, 3, '3518.95', 3, '50', '1452621600', '1454176800'),
(2, '17f2bbf883', 5, 3, '774', 3, '50', '1453312800', '1461780000');

-- --------------------------------------------------------

--
-- Table structure for table `bk_purchase_item`
--

DROP TABLE IF EXISTS `bk_purchase_item`;
CREATE TABLE IF NOT EXISTS `bk_purchase_item` (
  `purchase_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` longtext COLLATE utf8_unicode_ci NOT NULL,
  `purchase_price` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`purchase_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_purchase_item`
--

INSERT INTO `bk_purchase_item` (`purchase_item_id`, `purchase_id`, `product_id`, `quantity`, `purchase_price`) VALUES
(1, 1, 1, '2', '1600'),
(2, 1, 4, '5', '53'),
(3, 2, 3, '1', '800');

-- --------------------------------------------------------

--
-- Table structure for table `bk_sale`
--

DROP TABLE IF EXISTS `bk_sale`;
CREATE TABLE IF NOT EXISTS `bk_sale` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `vat_id` int(11) NOT NULL,
  `discount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  `due_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sale_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bk_sale`
--

INSERT INTO `bk_sale` (`sale_id`, `sale_code`, `customer_id`, `account_id`, `amount`, `vat_id`, `discount`, `creation_date`, `due_date`) VALUES
(1, 'f516551fc3', 4, 3, '4538.55', 2, '100', '1452708000', '1462816800');

-- --------------------------------------------------------

--
-- Table structure for table `bk_sale_item`
--

DROP TABLE IF EXISTS `bk_sale_item`;
CREATE TABLE IF NOT EXISTS `bk_sale_item` (
  `sale_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sale_price` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sale_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bk_sale_item`
--

INSERT INTO `bk_sale_item` (`sale_item_id`, `sale_id`, `product_id`, `quantity`, `sale_price`) VALUES
(4, 1, 1, '3', '1500'),
(5, 1, 2, '1', '20'),
(6, 1, 4, '1', '50');

-- --------------------------------------------------------

--
-- Table structure for table `bk_sessions`
--

DROP TABLE IF EXISTS `bk_sessions`;
CREATE TABLE IF NOT EXISTS `bk_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bk_sessions`
--

INSERT INTO `bk_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('005e600a4e522e1207e3d1cbc0375e1648f25f8a', '::1', 1453294628, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239343339353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('010fac54cb875e22c085a76c3c449c9a8543ddb5', '::1', 1453627719, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632373633383b7265715f75726c7c733a34343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('030babac8826b72305141e195a7c99a3e390288a', '::1', 1453372699, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337323336323b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('04930003e7a603e925727182d0846a8a8deed777', '::1', 1453118086, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131373835333b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('04dc200dd27ed0f299075bbd2af2e69b79c4a093', '::1', 1452682220, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638313835393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('04e755dfa34c22b98022e88db4b995ce53f18c78', '::1', 1453286647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238363634373b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0a2142d70054fcace206edfcf0c115d1b0971f1a', '::1', 1453356659, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335363439373b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0a3d412819aaabf5f5c46f8af15551ff4014a89d', '::1', 1452676483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637363236393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('0c6f247756f16907078358e924edb4df44bc5c9d', '::1', 1452757113, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323735373039313b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('1516a8ec7dbdde2ab36896443636dbe69f7777b4', '::1', 1453295034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239343735353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('17554df9e7e12a1dccc13f3efaf6be97607dda74', '::1', 1452675747, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637353437353b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('1943c7d5cd4c291d372788e0d970c38813bd0fe0', '::1', 1453363787, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336333532393b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1ca383b7f8ddf3460f3c837ea75f9f144db45253', '::1', 1453296135, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239363133353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1fc98f954d0f4ba0f1f25ef28e416c7868c46aa3', '::1', 1453291430, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239313135343b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2096e034dbbd3e8d2e09ff2ae2c264bc1b8c92de', '::1', 1453364857, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336343835363b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('232ede4a1b3ac9e702314c24a8f4754c1f758806', '::1', 1452681768, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638313336313b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('25e6fce1fecaa59d00d5a498ff78c92e9f648654', '::1', 1453295629, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239353338353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('25fbd2ef862fa44e1eaf63efaf49886c369120e5', '::1', 1453276289, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237363039383b),
('26199a5bdad9ff2e6c2cc1ba20df45010eb520ab', '::1', 1453631497, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633313239373b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('268984e4248a8a258350a9f026c5907c77347e17', '::1', 1453287860, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238373835393b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('28e19813f9d8df6077bfc3e6774160e406b69c43', '::1', 1453295970, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239353733333b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2e0c2a5934370a3c15758214272a7d0e2979a9d1', '::1', 1453357986, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335373733373b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2f7d76cdcd478e256737571f634980e86132e280', '::1', 1453289652, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238393438383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2f8e7aeaa829941996546bab7672e0bf9af8fc79', '::1', 1453624087, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632333832383b666c6173685f6d6573736167657c733a32343a22446174612044656c65746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('305685e7d899e20fa4d434570d108277e6865df0', '::1', 1453623547, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632333235343b666c6173685f6d6573736167657c733a32343a22446174612043726561746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('32d235cde282210ac299f95c9ba2b371c55eedb2', '::1', 1453113948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131333635363b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('33f70782bc22f82b6740e51cc187260bf6000ed1', '::1', 1453285933, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238353637353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3570c8fb9c13226b1518d213569ee3095bb2546d', '::1', 1453627297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632373239373b),
('3890a3cccd098bf11fd8c3ce1da5720107e51069', '::1', 1453266570, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333236363536383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b),
('38abbf1f3d038b7a2ddeb34d79f26ff0e9a1c6e0', '::1', 1453296693, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239363538303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3b227e1a5b9fcca86f1c30b90bf33ce3b3a17d81', '::1', 1453274228, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237343230373b),
('3b55155d77037bc9dbe009c8ea6a1f92ee588435', '::1', 1453290422, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239303135363b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3dd51a451e75d651a69e04a7e351d6c05a1c91fc', '::1', 1453299409, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239393132353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3df6ba2308854c59027641ad7616476f8c422981', '::1', 1453363512, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336333232313b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3f437cbb85b076880d9b2056e054f8f92775b1b1', '::1', 1452681225, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638313031373b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('45eab1ca772aa567a469432b825a88c20f1daa8d', '::1', 1453367229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336373031343b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4a0d3fecc6d520635c222170312224fb958ff7e0', '::1', 1453118347, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131383232313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4bf6fff7b180e4f544b6d0d9711f2e734ffa744d', '::1', 1452683022, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638323932313b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('4d2cca2b71ce143c3ab95fe0c958d1aed7b0e9bf', '::1', 1453367945, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336373830303b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4f5b03aab6d6f7e626d35edf258ed1c9e06a3fcf', '::1', 1453623620, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632333632303b),
('5187ab7346575cc4df49268a22c8f18dddfb3ee7', '::1', 1453629236, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632393138303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('53fc3f5a24e03f3506950fef6c62c2eb09092b44', '::1', 1453287023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238373032323b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('58edf2a9697af17f029f33bf8ba692b5bebd54f0', '::1', 1453285034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238343736313b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5923e6f9be6f2516f678c97be766d3e17c13f39c', '::1', 1452664441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636343130303b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('5940e59d131c9536b4d362c8b9b0ceef665febc2', '::1', 1453374617, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337343434363b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b),
('5a786e8f85886050441815e8f41216514809246a', '::1', 1452665027, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636343831393b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('6607036f4a09feb6fc377b71d7c0aba3f615292f', '::1', 1452760572, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323736303238303b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('6701ee34674ce4fecd52285d9ffc143502400bdc', '::1', 1453286483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238363334303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('684b82354da10e74212da67273547831eec56a7c', '::1', 1453636607, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633363537363b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32343a22446174612044656c65746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('68b7758ea51a4096e50eb40b8668b8d05ae1996f', '::1', 1453275478, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237353230363b),
('6961a89146a0e3dc819434eed456efeac6105dde', '::1', 1453116870, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131363538313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6a9bfc1ef5d1c2ade06836bc19cf4a86e602102e', '::1', 1453116132, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131353834373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6bb17ee17747215646fca7b300f49f938dba965c', '::1', 1453631646, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633313634363b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6bdbc109da4f675cc9ee2e334d0f35b3c308cde4', '::1', 1453623245, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632323935313b666c6173685f6d6573736167657c733a32343a22446174612043726561746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('6d4c1824e9f6ff19693449a91a34e9daad00a0af', '::1', 1453372908, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337323930383b),
('714e03ea58bfaadbc9adb6f7e1b49da66eb969b5', '::1', 1453117840, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131373534303b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('71eeb826963a672e0e7e8ac7ef61e1fa261292ac', '::1', 1453294254, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239343034383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('73c40e0423fbc6975c5ddbf245b856f128699c5d', '::1', 1452664098, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636323836383b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('7836aaa1be33a50d40d8665235cfe26b4228f02d', '::1', 1453291049, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239303739303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('78eb44bfa18302fd5a4c21adf2b8339b618c5028', '::1', 1453299100, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239383830353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('7e93c4d590f3defa37023b522dc57e392505e527', '::1', 1453631293, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633303830393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('811e64b69a8c3569a2abf6caa0d2c1147d20955a', '::1', 1453115504, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131353238303b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('82af6745e4c873b114eccba1061edba438e851f3', '::1', 1453367793, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336373439343b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('8366fe38b1e02c7ccf18a646e83f1f9b5d6d3b89', '::1', 1453364204, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336333936323b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('946340504594e855b2aa1dd7f2de356d09d84af8', '::1', 1453360909, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336303636323b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('96159cf3a25473fae47ec1641d03af6c7fc960a1', '::1', 1453628254, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632373938303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('978cf7642f46bf42bb71495bbc7b522c803b8d9d', '::1', 1453351631, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335313630383b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('98818b4b8f3e049d72292ae91cca6f99fe14891e', '::1', 1453285329, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238353036363b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('999ca45a5087e095e08ce2781b032060bfa1ad77', '::1', 1453274575, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237343537343b),
('9b332d537b71be6bbf48654a48136bde4b0a3810', '::1', 1453372269, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337313837383b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9caba1e7527d331858816041ff39da773b86a2c2', '::1', 1453117514, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131373232323b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9cd8817d1b516a2394e38be6968bbd3c8df17f2d', '::1', 1453292194, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239323133323b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a3fa5002d9499edcb88bdac40e6c403ba9d4a3e4', '::1', 1452664722, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636343439333b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('a7722d00f34d69dbb19522d3c234150dac61ae94', '::1', 1452675387, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637353131323b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('a78ef89613b22cddad95c0393748239c4025fa53', '::1', 1453361219, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336313035393b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a8549c52c0c1eac39eff0882b3e9f36f70ecdc0f', '::1', 1453287674, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238373534323b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('a878677a80f5131b2420faeb5f16769f6872b796', '::1', 1452680115, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638303036303b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('a8c1bba7026ecc756ad4b22ab0d327d0c8ca2585', '::1', 1453289999, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238393739383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ac159e050a47ad93e32c680cf5bbc1d999d3f764', '::1', 1452682552, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638323233323b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('b29e354a537b3694709bf6331990e704cd382108', '::1', 1453621972, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333631393437303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b404137bc2ac4a443152e579c76ee2749507510f', '::1', 1452670265, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637303235393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('b836e38f4680c4cdfabfde1e041bb6c614563d96', '::1', 1453355330, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335353333303b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b8e3aedc05bceff9db5f61d0ed5c63c8968fd242', '::1', 1452668077, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636373836363b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b666c6173685f6d6573736167657c733a32343a22446174612043726561746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('bdf9d5664dc89586d9270507efaad290355e7415', '::1', 1453109517, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333130393531373b),
('be1bdf94bdad215281df6b98fb4f85efe090e256', '::1', 1453622104, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632323130343b),
('be2ea8dd7b6ab521ba20c923658c290b8870e86d', '::1', 1452676205, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637353932363b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('bf34b45d9974583a1aae91ce315382b988c9a4cd', '::1', 1453371713, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337313533343b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c02a3bd1839fdfe9ee6573d5df81d03ba6f6a16b', '::1', 1453370078, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336393836313b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32323a22456d61696c2054656d706c6174652055706461746564223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('c5560779aec14d356efddebccf4b17c3d6b3f8f2', '::1', 1453114340, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131343237373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c851c132d228cb1301ed32ba9a6d0178fc746fca', '::1', 1453291939, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239313832363b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b);
INSERT INTO `bk_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('cb0d194acea3eb732e4ff6bf9b568983937371fe', '::1', 1453627149, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632363837343b),
('cc14e2d5611247cf565cb653934ba90d26b5cc11', '::1', 1453275746, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237353533313b),
('ccefc146689b535eb4f93f2117453cb9f53aeffa', '::1', 1453284606, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238343332373b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ce57d2c6d1dc5b2a8941207446c5b269ec37803f', '::1', 1452669228, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636393038393b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('cfffe926642097b32c902b9918e84d9c5a7149a8', '::1', 1452680862, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638303537323b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('d0f5e0dd8237f2283152f90f5178777cb5d94f57', '::1', 1453625725, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632353732353b),
('d2cef1ecd505203897766d3d4aee78a9a9355da3', '::1', 1453293963, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239333731303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d3e971b32281b085a20dca12a2b46868b6a1741f', '::1', 1453284073, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238333835353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d57e53025c35758bd8b40cf5cf3ed1c6a6c3672a', '::1', 1453622272, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632323230323b),
('e0b49ed1398a0d97cc5b4e2e8b7166b590a56199', '::1', 1453290688, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239303437303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e4b8b53fcf1b10f1535e3c1d2e9ee0d3b061ee23', '::1', 1453107711, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333130363835333b7265715f75726c7c733a36373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f696e766f6963652f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e5ff3e44fa800b6561288f1920b60f7bb371a668', '::1', 1453361800, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336313530383b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e62c1d778cc6cf872d3826412b8d54179deca781', '::1', 1452682864, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638323537303b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('e8456907994b8c7aaefe4f585029020c5140fec1', '::1', 1453622915, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632323633313b),
('e8caaf9f4c1e13e98fb4985b51c4292cb33b7a6c', '::1', 1453362093, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336313831373b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e9e576cc1c4c23140ac1c9e6e9aaf5e679734a45', '::1', 1453116397, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131363137323b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('eafb0cff437902900e373969d7af6247fa36e94c', '::1', 1453365673, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336353637333b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ebe56557c2d97018eb6300861ab0beed66af3587', '::1', 1453295334, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239353037303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f1435fd698716f7a5cfc9c5fdaa97c99bd2e1f0a', '::1', 1453106851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333039393938363b7265715f75726c7c733a36373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f696e766f6963652f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('f2db01485ce554acecf191b86cb6ed29a588ca31', '::1', 1453113283, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131333137313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f4e6eb055e4c2c028a95ed8ebd79fe8449e018e5', '::1', 1453114229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131333935383b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f88f698023500c4958a497011a813f09844739c3', '::1', 1453281420, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238313430383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f94ec773a80cf09054be79ba5de98afe9de23ce2', '::1', 1452760730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323736303630353b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('fa459bb308bcb8c4ec1cced65ff51850678c1daa', '::1', 1453117163, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131363931313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('fc41c78eb4d47293c65986e373fea09ffa818e73', '::1', 1453357257, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335373134363b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('fcd370a48ec65dca1ffa0336dad9a18c34408223', '::1', 1452665485, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636353135313b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('fdcefb33c5af69106f30dfb32ef2176e91f5d7ff', '::1', 1452666939, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636353534353b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b);

-- --------------------------------------------------------

--
-- Table structure for table `bk_settings`
--

DROP TABLE IF EXISTS `bk_settings`;
CREATE TABLE IF NOT EXISTS `bk_settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `bk_settings`
--

INSERT INTO `bk_settings` (`settings_id`, `type`, `description`) VALUES
(11, 'skin_colour', 'blue'),
(2, 'system_title', 'Bookkeeping'),
(4, 'system_currency_id', 'BDT'),
(5, 'paypal_email', 'sagarbiswas52-facilitator@yahoo.com'),
(6, 'stripe_publishable_key', 'pk_test_hOlcxbZoqlNGmvi0jjrlbc5e'),
(7, 'stripe_api_key', 'sk_test_5gBa7Cdhffk6t4SRfcEFs2oI'),
(10, 'store_price', '500'),
(12, 'text_align', 'left-to-right'),
(13, 'nearby_search_radius', '100'),
(14, 'trial_period', '30'),
(15, 'nexmo_api_key', '2430330d'),
(16, 'nexmo_secret_key', 'dbbcaa93'),
(17, 'language', 'en'),
(18, 'sms_notification', 'on'),
(19, 'currency_placing', 'after_with_gap'),
(20, 'financial_year_start', 'january');

-- --------------------------------------------------------

--
-- Table structure for table `bk_user`
--

DROP TABLE IF EXISTS `bk_user`;
CREATE TABLE IF NOT EXISTS `bk_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` char(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bk_user`
--

INSERT INTO `bk_user` (`user_id`, `first_name`, `last_name`, `email`, `password`, `status`) VALUES
(1, 'John', 'Doe', 'admin@example.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1),
(2, 'Rhoda', 'Bell', 'aqua.primera@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bk_vat`
--

DROP TABLE IF EXISTS `bk_vat`;
CREATE TABLE IF NOT EXISTS `bk_vat` (
  `vat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `percentage` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_vat`
--

INSERT INTO `bk_vat` (`vat_id`, `name`, `percentage`) VALUES
(2, 'Tax', '1.5'),
(3, 'Vat A', '3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
