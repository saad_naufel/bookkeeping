-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2016 at 04:41 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `creativeitem_bookkeeping`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_account`
--

DROP TABLE IF EXISTS `bk_account`;
CREATE TABLE IF NOT EXISTS `bk_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `balance` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bk_account`
--

INSERT INTO `bk_account` (`account_id`, `type`, `title`, `account_number`, `balance`) VALUES
(2, 'bank', 'Aspernatur cumque omnissss', '868888', '10009'),
(3, 'cash', 'Molestiae aut error', '333', '7356'),
(4, 'cash', 'Nostrud omnis in null', '433', '2006');

-- --------------------------------------------------------

--
-- Table structure for table `bk_contact`
--

DROP TABLE IF EXISTS `bk_contact`;
CREATE TABLE IF NOT EXISTS `bk_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `first_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `last_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `company_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext COLLATE utf8_unicode_ci NOT NULL,
  `mobile` longtext COLLATE utf8_unicode_ci NOT NULL,
  `website` longtext COLLATE utf8_unicode_ci NOT NULL,
  `skype_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `country` longtext COLLATE utf8_unicode_ci NOT NULL,
  `city` longtext COLLATE utf8_unicode_ci NOT NULL,
  `state` longtext COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `bank_account` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bk_contact`
--

INSERT INTO `bk_contact` (`contact_id`, `type`, `first_name`, `last_name`, `company_name`, `email`, `phone`, `mobile`, `website`, `skype_id`, `address`, `country`, `city`, `state`, `zip_code`, `bank_account`) VALUES
(2, 'customer', 'Elliott', 'Bentley', 'Noble Estrada', 'pegojesu@gmail.com', '+695-32-3885613', 'Eligendi asperiores enim eius provident libero fuga Repellendus Rerum quis esse architecto proident aut', 'http://www.buvatad.net', 'Dolore accusamus vero est adipisci quis', 'Esse laboris quia ', 'Maiores vero inventore exercitation minus lorem doloremque temporibus illum amet rem labore soluta', 'Earum eiusmod provident eum qui inventore esse accusantium', 'Aliquam necessitatibus et itaque et quasi', '80473', 'Qui consectetur aut fugit ullam labore nemo voluptate'),
(3, 'customer', 'Rahim', 'Carney', 'Calista Shepherd', 'jegyhi@gmail.com', '+378-92-9421268', 'Tempora id excepturi nesciunt excepturi maiores pariatur Pariatur Corrupti aut', 'http://www.duzivefilutebe.net', 'Velit adipisicing est consequat Nostrud quibusdam ex excepteur optio qui ad ut adipisicing earum non quo', 'Ut laboriosam, provide', 'Consectetur ipsam beatae culpa eaque', 'Qui dolor odit ea dolor iste maxime officia modi perspiciatis praesentium', 'In est et iusto quae commodo proident animi', '14524', 'Sunt ea perferendis eligendi beatae et sit minim ducimus rerum minus'),
(4, 'customer', 'Nash', 'Branch', 'Connor Peterson', 'wycesutew@yahoo.com', '+237-61-7788917', 'Quis accusantium eos perspiciatis ipsam odio', 'http://www.letiwof.co', 'Quos voluptas itaque blanditiis autem in necessitatibus lorem cum ut aliquid ad odio nisi nulla enim quibusdam enim', 'Adipisicing dolorem aliquid', 'Qui deserunt dolores eum ullam aut voluptatum earum', 'Atque ipsa dolore eius ipsa nemo unde aut qui aut error modi quibusdam deserunt lorem reiciendis quidem', 'Et consequuntur voluptate optio quidem duis dolores anim error facilis modi explicabo Cum et omnis minima autem', '45728', 'Enim dolore eius iste voluptatibus temporibus eum enim consequatur Eligendi non ex excepturi explicabo'),
(5, 'supplier', 'Rogan', 'Roach', 'Alexis Willis', 'aqua.primera@gmail.com', '+315-53-7361127', 'Optio et non pariatur Qui dolor', 'http://www.qeqokykixazehag.net', 'Officia ratione est minim vero aliquid dolorem facere ut vel atque veniam omnis at tenetur', 'Maiores eu quaera', 'Voluptatem Earum maiores aut debitis architecto et', 'Consequatur voluptatem Quidem quod dicta est quaerat commodi labore provident ipsa atque ut ipsam blanditiis minima autem dolor', 'Adipisci quia laboris dolorem odio cillum consequatur Reiciendis quae nobis eligendi consequuntur', '32302', 'Et et ut dolore voluptatem vero dignissimos ipsum sunt voluptates exercitation ducimus voluptas at do harum repudiandae quod occaecat enim'),
(6, 'supplier', 'Anika', 'Holder', 'Judah Lara', 'kazukyf@yahoo.com', '+936-25-8450645', '425254', 'http://www.qufyrejurew.ca', 'xfsf@cgds.c', 'Enim aliquam nobis', 'Amet repellendus', 'Inventore', 'Sed necess', '15860', '6183');

-- --------------------------------------------------------

--
-- Table structure for table `bk_currency`
--

DROP TABLE IF EXISTS `bk_currency`;
CREATE TABLE IF NOT EXISTS `bk_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `bk_currency`
--

INSERT INTO `bk_currency` (`currency_id`, `currency_code`, `currency_symbol`, `currency_name`) VALUES
(1, 'USD', '$', 'US dollar'),
(2, 'GBP', '£', 'Pound'),
(3, 'EUR', '€', 'Euro'),
(4, 'AUD', '$', 'Australian Dollar'),
(5, 'CAD', '$', 'Canadian Dollar'),
(6, 'JPY', '¥', 'Japanese Yen'),
(7, 'NZD', '$', 'N.Z. Dollar'),
(8, 'CHF', 'Fr', 'Swiss Franc'),
(9, 'HKD', '$', 'Hong Kong Dollar'),
(10, 'SGD', '$', 'Singapore Dollar'),
(11, 'SEK', 'kr', 'Swedish Krona'),
(12, 'DKK', 'kr', 'Danish Krone'),
(13, 'PLN', 'zł', 'Polish Zloty'),
(14, 'HUF', 'Ft', 'Hungarian Forint'),
(15, 'CZK', 'Kč', 'Czech Koruna'),
(16, 'MXN', '$', 'Mexican Peso'),
(17, 'CZK', 'Kč', 'Czech Koruna'),
(18, 'MYR', 'RM', 'Malaysian Ringgit');

-- --------------------------------------------------------

--
-- Table structure for table `bk_email_template`
--

DROP TABLE IF EXISTS `bk_email_template`;
CREATE TABLE IF NOT EXISTS `bk_email_template` (
  `email_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `task` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subject` longtext COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`email_template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bk_email_template`
--

INSERT INTO `bk_email_template` (`email_template_id`, `task`, `subject`, `body`) VALUES
(1, 'new_admin_account_opening', 'Admin account creation', '<span>\r\n<div>Hi [ADMIN_NAME],</div>\r\n</span>Your admin account is created !\r\nPlease login to your admin&nbsp;<span>account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>Login credential :<br>email : [ADMIN_EMAIL]<br>password : [ADMIN_PASSWORD]'),
(2, 'password_reset_confirmation', 'Password reset notification', 'Hi [NAME],<br>Your password is reset. New password : [NEW_PASSWORD]<br>Login here with your new password :<br>[SYSTEM_URL]<br><br>You can change your password after logging in to your account.');

-- --------------------------------------------------------

--
-- Table structure for table `bk_expense`
--

DROP TABLE IF EXISTS `bk_expense`;
CREATE TABLE IF NOT EXISTS `bk_expense` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `income_expense_category_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bk_expense`
--

INSERT INTO `bk_expense` (`expense_id`, `title`, `description`, `income_expense_category_id`, `amount`, `account_id`, `date`) VALUES
(2, 'Officia est cupidatat in qu', 'Et consequuntur eu sint laboriosam, sunt non sed enim quia ab aspernatur reiciendis dolor ad deleniti hic incidunt.', 2, '54', 2, '1474394400'),
(3, 'Aperiam incidunt enim', 'At provident, cumque exercitation voluptatum enim even', 3, '73', 3, '1458669600'),
(4, 'Libero id consectetur cu', 'Consequat. Lorem et earum accusantium omnis labore ea voluptates ut.', 2, '32', 2, '1497981600'),
(5, 'Non nesciunt minima volu', 'Et explicabo. Voluptatum consequatur, nisi', 3, '89', 3, '1452621600'),
(6, 'Non ad exercitationem mag', 'Quo error obcaecati maxime placeat, velit, eu rerum sit ea minima.', 2, '57', 2, '1447178400');

-- --------------------------------------------------------

--
-- Table structure for table `bk_income`
--

DROP TABLE IF EXISTS `bk_income`;
CREATE TABLE IF NOT EXISTS `bk_income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `income_expense_category_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`income_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_income`
--

INSERT INTO `bk_income` (`income_id`, `title`, `description`, `income_expense_category_id`, `amount`, `account_id`, `date`) VALUES
(2, 'Ut ut adipisci sed exercita', 'Nulla id ut labore facere doloribus iste et ad architecto dolorem quia consequatur? Non doloribus tempore, error nobis animi, culpa.', 3, '542', 2, '1452016800'),
(3, 'Mollitia aliqua Dolores', 'A molestias est fuga. Libero aliquam incididunt', 2, '200', 2, '1439920800');

-- --------------------------------------------------------

--
-- Table structure for table `bk_income_expense_category`
--

DROP TABLE IF EXISTS `bk_income_expense_category`;
CREATE TABLE IF NOT EXISTS `bk_income_expense_category` (
  `income_expense_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`income_expense_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_income_expense_category`
--

INSERT INTO `bk_income_expense_category` (`income_expense_category_id`, `name`) VALUES
(2, 'Sales'),
(3, 'Advertising');

-- --------------------------------------------------------

--
-- Table structure for table `bk_language`
--

DROP TABLE IF EXISTS `bk_language`;
CREATE TABLE IF NOT EXISTS `bk_language` (
  `phrase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `en` longtext COLLATE utf8_unicode_ci NOT NULL,
  `bn` longtext COLLATE utf8_unicode_ci NOT NULL,
  `es` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ar` longtext COLLATE utf8_unicode_ci NOT NULL,
  `de` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fr` longtext COLLATE utf8_unicode_ci NOT NULL,
  `it` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ru` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tr` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=261 ;

--
-- Dumping data for table `bk_language`
--

INSERT INTO `bk_language` (`phrase_id`, `phrase`, `en`, `bn`, `es`, `ar`, `de`, `fr`, `it`, `ru`, `tr`) VALUES
(1, 'login', 'Login', 'লগইন', 'Iniciar sesión', 'تسجيل الدخول', 'Anmeldung', 'S''identifier', 'Accesso', 'Авторизоваться', 'Oturum aç'),
(2, 'email', 'Email', 'ইমেইল', 'Email', 'البريد الإلكتروني', 'Email', 'Email', 'E-mail', 'Эл. адрес', 'E-posta'),
(3, 'password', 'Password', 'পাসওয়ার্ড', 'Clave', 'الرمز السري', 'Passwort', 'Mot de passe', 'Parola d''ordine', 'пароль', 'Parola'),
(4, 'forgot_your_password', 'Forgot Your Password', 'আপনি কি পাসওয়ার্ড ভুলে গেছেন', 'Olvidaste tu contraseña', 'نسيت رقمك السري', 'Haben Sie Ihr Passwort vergessen', 'Mot de passe oublié', 'Hai dimenticato la password', 'Забыли пароль', 'Şifrenizi mi unuttunuz'),
(5, 'reset_password', 'Reset Password', 'পাসওয়ার্ড রিসেট করুন', 'Restablecer la contraseña', 'اعادة تعيين كلمة السر', 'Passwort zurücksetzen', 'Nouveau mot de passe', 'Resettare la password', 'Сброс пароля', 'Şifre sıfırlamak'),
(6, 'return_to_login_page', 'Return To Login Page', 'পাতা লগইন আসতে', 'Volver a inicio página', 'العودة إلى صفحة تسجيل الدخول', 'Zurück zur Anmeldeseite Seite', 'Return to Login page', 'Ritorna al login pagina', 'Вернуться на страницу входа', 'Sayfayı Girişi Dönüş'),
(7, 'admin_dashboard', 'Admin Dashboard', 'অ্যাডমিন ড্যাশবোর্ড', 'Dashboard administración', 'المشرف لوحة', 'Admin-Dashboard', 'Administrateur Dashboard', 'Admin Dashboard', 'Админ Панель', 'Yönetici Paneli'),
(8, 'dashboard', 'Dashboard', 'ড্যাশবোর্ড', 'Tablero de instrumentos', 'لوحة أجهزة القياس', 'Armaturenbrett', 'Tableau de bord', 'Cruscotto', 'Панель приборов', 'Dashboard'),
(9, 'contacts', 'Contacts', 'যোগাযোগ', 'Contactos', 'جهات الاتصال', 'Impressum', 'Contacts', 'Contatti', 'Контакты', 'İletişim'),
(10, 'customers', 'Customers', 'গ্রাহকরা', 'Clientes', 'الزبائن', 'Kunden,', 'Les clients', 'Clienti', 'Клиенты', 'Müşteriler'),
(11, 'suppliers', 'Suppliers', 'সরবরাহকারী', 'Proveedores', 'الموردين', 'Lieferanten', 'Fournisseurs', 'Fornitori', 'Поставщики', 'Tedarikçiler'),
(12, 'financial_accounts', 'Financial Accounts', 'আর্থিক হিসাব', 'Cuentas financieras', 'الحسابات المالية', 'Finanzierungsrechnung', 'Comptes financiers', 'Conti finanziari', 'Финансовая отчетность', 'Finansal Hesaplar'),
(13, 'create_new_account', 'Create New Account', 'নতুন অ্যাকাউন্ট তৈরি করুন', 'Crear una nueva cuenta', 'إنشاء حساب جديد', 'Neuen Account erstellen', 'Créer un nouveau compte', 'Crea un nuovo account', 'Создать новый аккаунт', 'Yeni hesap oluştur'),
(14, 'account_list', 'Account List', 'অ্যাকাউন্টের তালিকা', 'Lista de cuentas', 'قائمة الحساب', 'Kontoliste', 'Liste des comptes', 'Account List', 'Список аккаунт', 'Hesap Listesi'),
(15, 'inventory', 'Inventory', 'জায়', 'Inventario', 'المخزون', 'Inventar', 'Inventaire', 'Inventario', 'Инвентаризация', 'Envanter'),
(16, 'products', 'Products', 'পণ্য', 'Productos', 'المنتجات', 'Produkte', 'Produits', 'Prodotti', 'Продукты', 'Ürünler'),
(17, 'services', 'Services', 'সেবা', 'Servicios', 'الخدمات', 'Dienstleistungen', 'Services', 'Servizi', 'Сервисы', 'Hizmetler'),
(18, 'product_/_service_categories', 'Product / Service Categories', 'পণ্য / সেবা বিভাগ', 'Producto / Servicio Categorías', 'المنتج / الخدمة الفئات', 'Produkte / Service Kategorien', 'Produit / Service de Catégories', 'Prodotto / Servizio Categorie', 'Товар / услуга Категории', 'Ürün / Hizmet Kategorileri'),
(19, 'purchases', 'Purchases', 'কেনাকাটা', 'Las compras', 'مشتريات', 'Einkäufe', 'Achats', 'Acquisti', 'Покупки', 'Alımlar'),
(20, 'new_purchase', 'New Purchase', 'নতুন ক্রয়', 'Nueva compra', 'شراء الجديد', 'Neuanschaffung', 'Nouvel achat', 'Nuovo Acquisto', 'Новый Покупка', 'Yeni Alım'),
(21, 'purchase_history', 'Purchase History', 'ক্রয় ইতিহাস', 'Historial de compras', 'تاريخ شراء', 'Kaufhistorie', 'Historique d''achat', 'Cronologia degli acquisti', 'История покупки', 'Satın alma geçmişi'),
(22, 'sales', 'Sales', 'সেলস', 'Ventas', 'مبيعات', 'Der Umsatz', 'Ventes', 'I saldi', 'Продажи', 'Satış'),
(23, 'new_sale', 'New Sale', 'নতুন বিক্রয়', 'Nueva venta', 'بيع جديدة', 'Neuheiten Aktionen', 'Nouveautés', 'Nuova vendita', 'Новое Распродажа', 'Yeni Satış'),
(24, 'sales_history', 'Sales History', 'সেলস ইতিহাস', 'Historia Ventas', 'مبيعات التاريخ', 'Vertrieb Geschichte', 'Histoire des ventes', 'Storia Vendite', 'История продаж', 'Satış Tarihi'),
(25, 'incomes_/_expenses', 'Incomes / Expenses', 'আয়ের / খরচ', 'Ingresos / Gastos', 'الدخل / المصروفات', 'Einkommen / Ausgaben', 'Revenus / dépenses', 'Redditi / Spese', 'Доходы / расходы', 'Gelirleri / Giderleri'),
(26, 'incomes', 'Incomes', 'আয়', 'Ingresos', 'دخل', 'Einkommen', 'Revenus', 'Redditi', 'Доходов', 'Gelirler'),
(27, 'expenses', 'Expenses', 'খরচ', 'Gastos', 'مصاريف', 'Kosten', 'Dépenses', 'Spese', 'Затраты', 'Giderler'),
(28, 'income_/_expense_categories', 'Income / Expense Categories', 'আয় / ব্যয় বিভাগ', 'Ingresos / gastos Categorías', 'الدخل / المصاريف الفئات', 'Erträge / Aufwendungen Kategorien', 'Revenus / Dépenses Catégories', 'Proventi / oneri categorie', 'Доходы / расходы Категории', 'Gelir / Gider Kategorileri'),
(29, 'reporting', 'Reporting', 'রিপোর্টিং', 'Informes', 'التقارير', 'Berichterstattung', 'Compte-rendu', 'Segnalazione', 'Составление отчетов', 'Raporlama'),
(30, 'account_statements', 'Account Statements', 'অ্যাকাউন্ট বিবৃতি', 'Estados de Cuenta', 'كشوف الحساب', 'Kontoauszüge', 'Relevés de compte', 'Estratti conto', 'Выписки', 'Hesap Raporları'),
(31, 'income_report', 'Income Report', 'রিপোর্ট আয়', 'Informe de Ingresos', 'تقرير الدخل', 'Einkommensbericht', 'Rapport sur le revenu', 'Reddito Relazione', 'Доходы Сообщить', 'Gelir Raporu'),
(32, 'expense_report', 'Expense Report', 'ব্যয় রিপোর্ট', 'Reporte de Gastos', 'تقرير حساب', 'Spesenabrechnung', 'Rapport de dépenses', 'Expense Report', 'Отчет о затратах', 'Gider raporu'),
(33, 'income_expense_comparison', 'Income Expense Comparison', 'আয় ব্যয় তুলনা', 'Comparación de Gastos Ingresos', 'نفقات الدخل مقارنة', 'Income Kostenvergleich', 'Comparaison des Charges Produits', 'Proventi Oneri Confronto', 'Сравнение на прибыль', 'Gelir Gider Karşılaştırma'),
(34, 'notes', 'Notes', 'নোট', 'Notas', 'ملاحظات', 'Hinweise', 'Remarques', 'Gli appunti', 'Заметки', 'Notlar'),
(35, 'admins', 'Admins', 'প্রশাসকদের', 'Administradores', 'مدراء', 'Admins', 'Admins', 'Amministratori', 'Администраторы', 'Yöneticiler'),
(36, 'settings', 'Settings', 'সেটিংস', 'Ajustes', 'إعدادات', 'Einstellungen', 'Paramètres', 'Impostazioni', 'Настройки', 'Ayarlar'),
(37, 'system_settings', 'System Settings', 'পদ্ধতি নির্ধারণ', 'Ajustes del sistema', 'اعدادات النظام', 'Systemeinstellungen', 'Les paramètres du système', 'Impostazioni di sistema', 'Настройки системы', 'Sistem ayarları'),
(38, 'email_settings', 'Email Settings', 'ইমেইল সেটিংস', 'Ajustes del correo electrónico', 'إعدادات البريد الإلكتروني', 'E-Mail-Einstellungen', 'Paramètres de messagerie', 'Impostazioni e-mail', 'Настройки электронной почты', 'E-posta Ayarları'),
(39, 'language_settings', 'Language Settings', 'ভাষা ব্যাবস্থা', 'Ajustes de idioma', 'اعدادات اللغة', 'Spracheinstellungen', 'Paramètres de langue', 'Impostazioni della lingua', 'Языковые настройки', 'Dil ayarları'),
(40, 'vat_settings', 'Vat Settings', 'ভ্যাট সেটিংস', 'Ajustes IVA', 'إعدادات ضريبة القيمة المضافة', 'MwSt-Einstellungen', 'Paramètres de cuve', 'Impostazioni Iva', 'Настройки Vat', 'Vat Ayarları'),
(41, 'account', 'Account', 'হিসাব', 'Cuenta', 'حساب', 'Konto', 'Compte', 'Conto', 'Счет', 'Hesap'),
(42, 'Bookkeeping', 'Bookkeeping', 'হিসাবরক্ষণ', 'Teneduría de libros', 'مسك دفاتر', 'Buchhaltung', 'Comptabilité', 'Contabilità', 'Бухгалтерия', 'Defter tutma'),
(43, 'log_out', 'Log Out', 'ত্যাগ করুন', 'Cerrar sesión', 'تسجيل خروج', 'Abmelden', 'Se déconnecter', 'Disconnettersi', 'Выйти', 'Çıkış Yap'),
(44, 'sale_code', 'Sale Code', 'বিক্রয় কোড', 'Venta Código', 'بيع مدونة', 'Sale-Code', 'Vente code', 'Vendita Codice', 'Продажа Код', 'Satış Kodu'),
(45, 'purchase_code', 'Purchase Code', 'ক্রয় কোড', 'Código de Compra', 'كود شراء', 'Kauf-Code', 'Code d''Achat', 'Codice di acquisto', 'Покупка код', 'Satınalma Kodu'),
(46, 'income', 'Income', 'আয়', 'Ingresos', 'دخل', 'Einkommen', 'Revenu', 'Reddito', 'Доход', 'Gelir'),
(47, 'last_30_days', 'Last 30 Days', 'সর্বশেষ 30 দিন', 'Últimos 30 Días', 'اخر 30 يوم', 'Letzte 30 Tage', '30 derniers jours', 'Ultimi 30 giorni', 'Последние 30 дней', 'Son 30 Gün'),
(48, 'expense', 'Expense', 'ব্যয়', 'Gastos', 'مصروف', 'Kostenaufwand', 'Frais', 'Spese', 'Расходы', 'Gider'),
(49, 'income_expense_calendar', 'Income Expense Calendar', 'আয় ব্যয় ক্যালেন্ডার', 'Gastos Ingresos Calendario', 'نفقات دخل التقويم', 'Income Expense Calendar', 'Charges Produits Calendrier', 'Proventi Oneri Calendario', 'Прибыль Календарь', 'Gelir Gider Takvim'),
(50, 'sale', 'Sale', 'বিক্রয়', 'Venta', 'تخفيض السعر', 'Verkauf', 'vendre', 'Vendita', 'Продажа', 'Satış'),
(51, 'purchase', 'Purchase', 'ক্রয়', 'Compra', 'شراء', 'Kauf', 'Achat', 'Acquisto', 'Покупка', 'Satınalma'),
(52, 'manage_customers', 'Manage Customers', 'গ্রাহকদের পরিচালনা', 'Administrar Clientes', 'إدارة العملاء', 'Kunden verwalten', 'Gérer clients', 'Gestire i clienti', 'Управление клиентами', 'Müşteriler yönetin'),
(53, 'add_new_contact', 'Add New Contact', 'নতুন যোগ করুন', 'Añadir nuevo contacto', 'إضافة جهة اتصال جديدة', 'Neuen Kontakt hinzufügen', 'Ajouter un nouveau contact', 'Aggiungi nuovo contatto', 'Добавить новый контакт', 'Yeni Kişi Ekle'),
(54, 'name', 'Name', 'নাম', 'Nombre', 'اسم', 'Name', 'prénom', 'Nome', 'имя', 'Isim'),
(55, 'company_name', 'Company Name', 'কোমপানির নাম', 'nombre de empresa', 'اسم الشركة', 'Name der Firma', 'Nom de la compagnie', 'Nome della ditta', 'название компании', 'Şirket Adı'),
(56, 'phone', 'Phone', 'ফোন', 'Teléfono', 'الهاتف', 'Telefon', 'Téléphone', 'Telefono', 'Телефон', 'Telefon'),
(57, 'type', 'Type', 'শ্রেণী', 'Escribe', 'اكتب', 'Art', 'Type', 'Tipo', 'Тип', 'Tip'),
(58, 'options', 'Options', 'বিকল্প', 'Opciones', 'خيارات', 'Optionen', 'Options', 'Opzioni', 'Опции', 'Seçenekler'),
(59, 'customer', 'Customer', 'ক্রেতা', 'Cliente', 'زبون', 'Kunde', 'Client', 'Cliente', 'Клиент', 'Müşteri'),
(60, 'actions', 'Actions', 'পদক্ষেপ', 'Comportamiento', 'تطبيقات', 'Aktionen', 'actes', 'Azioni', 'Меры', 'Eylemler'),
(61, 'edit', 'Edit', 'সম্পাদন করা', 'Editar', 'تحرير', 'Bearbeiten', 'modifier', 'Modifica', 'редактировать', 'Düzenleme'),
(62, 'view_details', 'View Details', 'বিস্তারিত দেখা', 'Ver detalles', 'عرض تفاصيل', 'Details anzeigen', 'Voir les détails', 'Guarda i dettagli', 'Посмотреть детали', 'Ayrıntıları görüntüle'),
(63, 'delete', 'Delete', 'মুছে', 'Borrar', 'حذف', 'Löschen', 'Effacer', 'Cancellare', 'Удалить', 'Silmek'),
(64, 'manage_suppliers', 'Manage Suppliers', 'সরবরাহকারী পরিচালনা', 'Manejo de Proveedores', 'إدارة الموردين', 'Lieferanten verwalten', 'Gérer Fournisseurs', 'Gestire Fornitori', 'Управление Поставщики', 'Tedarikçi yönetin'),
(65, 'supplier', 'Supplier', 'সরবরাহকারী', 'Proveedor', 'المورد', 'Lieferant', 'Fournisseur', 'Fornitore', 'Поставщик', 'Tedarikçi'),
(66, 'add_contact', 'Add Contact', 'পরিচিতি যোগ করুন', 'Agregar contacto', 'إضافة جهة اتصال', 'Kontakt hinzufügen', 'Ajouter contact', 'Aggiungi contatto', 'Добавить контакт', 'Kişi ekle'),
(67, 'contact_type', 'Contact Type', 'যোগাযোগ ধরন', 'Tipo de Contacto', 'نوع الاتصال', 'Kontakttyp', 'type de contact', 'Tipo di contatto', 'Тип контакта', 'İletişim Tipi'),
(68, 'first_name', 'First Name', 'প্রথম নাম', 'Nombre', 'الاسم الأول', 'Vorname', 'Prénom', 'Nome', 'Имя', 'İsim'),
(69, 'last_name', 'Last Name', 'শেষ নাম', 'Apellido', 'الاسم الاخير', 'Familienname, Nachname', 'Nom de famille', 'Cognome', 'Фамилия', 'Soyadı'),
(70, 'mobile', 'Mobile', 'মোবাইল', 'Móvil', 'التليفون المحمول', 'Mobile', 'Mobile', 'Mobile', 'Мобильный', 'Seyyar'),
(71, 'website', 'Website', 'ওয়েবসাইট', 'Sitio web', 'موقع الكتروني', 'Webseite', 'Site Internet', 'Sito web', 'Веб-сайт', 'Web Sitesi'),
(72, 'skype_id', 'Skype Id', 'স্কাইপ আইডি', 'Identificación del skype', 'هوية السكايب', 'Skype Kennzeichnung', 'Skype', 'Identificativo di Skype', 'Skype ID', 'Skype kullanıcı adı'),
(73, 'address', 'Address', 'ঠিকানা', 'Dirección', 'العنوان', 'Adresse', 'Adresse', 'Indirizzo', 'Адрес', 'Adres'),
(74, 'country', 'Country', 'দেশ', 'País', 'بلد', 'Land', 'Pays', 'Nazione', 'Страна', 'Ülke'),
(75, 'city', 'City', 'শহর', 'Ciudad', 'المدينة', 'Stadt', 'Ville', 'Città', 'город', 'Şehir'),
(76, 'state', 'State', 'রাষ্ট্র', 'Estado', 'حالة', 'Bundesland', 'Etat', 'Stato', 'состояние', 'Eyalet'),
(77, 'zip_code', 'Zip Code', 'ZIP কোড', 'Código postal', 'الرمز البريدي', 'PLZ', 'Code postal', 'Cap', 'Почтовый Индекс', 'Posta kodu'),
(78, 'bank_account', 'Bank Account', 'ব্যাংক হিসাব', 'Cuenta bancaria', 'حساب البنك', 'Bank Konto', 'Compte bancaire', 'Conto bancario', 'Банковский счет', 'Banka hesabı'),
(79, 'submit', 'Submit', 'জমা দিন', 'Enviar', 'عرض', 'einreichen', 'Proposer', 'Presentare', 'Отправить', 'Gönder'),
(80, 'edit_contact', 'Edit Contact', 'সম্পাদনা করুন', 'Editar contacto', 'تحرير الاتصال', 'Kontakt bearbeiten', 'Modifier le contact', 'Modifica il contatto', 'Изменить контакт', 'Düzenleme İletişim'),
(81, 'update', 'Update', 'আপডেট', 'Actualizar', 'تحديث', 'Aktualisieren', 'Mettre à jour', 'Aggiornare', 'Обновить', 'Güncelleştirme'),
(82, 'customer_details', 'Customer Details', 'কাস্টমার বিস্তারিত', 'Detalles del cliente', 'تفاصيل العميل', 'Kundendetails', 'Détails du client', 'Dettagli cliente', 'Реквизиты клиента', 'Müşteri detayları'),
(83, 'home', 'Home', 'বাড়ি', 'Casa', 'الصفحة الرئيسية', 'Zuhause', 'Accueil', 'Casa', 'Главная', 'Ev'),
(84, 'details', 'Details', 'বিবরণ', 'Detalles', 'تفاصيل', 'Einzelheiten', 'Détails', 'Dettagli', 'Детали', 'Detaylar'),
(85, 'summary', 'Summary', 'সংক্ষিপ্ত', 'Resumen', 'ملخص', 'Zusammenfassung', 'Récapitulatif', 'Sommario', 'Резюме', 'Özet'),
(86, 'code', 'Code', 'কোড', 'Código', 'رمز', 'Code', 'Code', 'Codice', 'Код', 'Kod'),
(87, 'creation_date', 'Creation Date', 'তৈরির তারিখ', 'Fecha de creación', 'تاريخ الإنشاء', 'Erstellungsdatum', 'Date de création', 'Data di creazione', 'Дата создания', 'Oluşturma tarihi'),
(88, 'amount', 'Amount', 'পরিমাণ', 'Cantidad', 'كمية', 'Menge', 'Montant', 'Importo', 'Количество', 'Miktar'),
(89, 'view_sale_invoice', 'View Sale Invoice', 'দেখুন বিক্রয় চালান', 'Ver Venta Factura', 'مشاهدة فاتورة بيع', 'Ansicht Verkauf Rechnung', 'Voir Vente facture', 'Vendita fattura', 'Посмотреть продажа Счет', 'Görünüm Satış Faturası'),
(90, 'to', 'To', 'থেকে', 'A', 'إلى', 'Zu', 'À', 'A', 'Для', 'Karşı'),
(91, 'subject', 'Subject', 'বিষয়', 'Sujeto', 'الموضوع', 'Gegenstand', 'Assujettir', 'Soggetto', 'Предмет', 'Tabi'),
(92, 'message', 'Message', 'বার্তা', 'Mensaje', 'الرسالة', 'Nachricht', 'Message', 'Messaggio', 'Сообщение', 'Mesaj'),
(93, 'send_email', 'Send Email', 'বার্তা পাঠাও', 'Enviar correo electrónico', 'إرسال البريد الإلكتروني', 'E-Mail senden', 'Envoyer un email', 'Invia una email', 'Отправить на e-mail', 'Eposta yolla'),
(94, 'supplier_details', 'Supplier Details', 'সরবরাহকারী', 'Detalles del proveedor', 'تفاصيل المورد', 'Supplier Details', 'Fournisseur Détails', 'Fornitore Dettagli', 'Поставщик Подробности', 'Tedarikçi Detayları'),
(95, 'view_purchase_invoice', 'View Purchase Invoice', 'দেখুন ক্রয় চালান', 'Ver Compra Factura', 'عرض شراء فاتورة', 'Ankaufsrechnung', 'Voir la facture d''achat', 'Visualizza Acquisto Fattura', 'Посмотреть накладная', 'Görünüm Satınalma Faturası'),
(96, 'add_account', 'Add Account', 'হিসাব যোগ করা', 'Añadir cuenta', 'إضافة حساب', 'Konto hinzufügen', 'Ajouter un compte', 'Aggiungi account', 'Добавить аккаунт', 'Hesap eklemek'),
(97, 'account_type', 'Account Type', 'হিসাবের ধরণ', 'Tipo de cuenta', 'نوع الحساب', 'Account', 'Type de compte', 'Tipo di account', 'тип аккаунта', 'hesap tipi'),
(98, 'bank', 'Bank', 'ব্যাংক', 'Banco', 'بنك', 'Bank', 'Banque', 'Banca', 'Банка', 'Banka'),
(99, 'cash', 'Cash', 'নগদ', 'Efectivo', 'السيولة النقدية', 'Kasse', 'Argent comptant', 'Contanti', 'Денежные средства', 'Nakit'),
(100, 'other', 'Other', 'অন্যান্য', 'Otro', 'الآخر', 'Andere', 'Autre', 'Altro', 'Другие', 'Diğer'),
(101, 'account_title', 'Account Title', 'অ্যাকাউন্ট শিরোনাম', 'Titulo de cuenta', 'عنوان حساب', 'Konto Titel', 'Compte Titre', 'Conto Titolo', 'Счет Название', 'Hesap Adı'),
(102, 'account_number', 'Account Number', 'হিসাব নাম্বার', 'Número de cuenta', 'رقم الحساب', 'Accountnummer', 'Numéro de compte', 'Numero di conto', 'Номер аккаунта', 'Hesap numarası'),
(103, 'starting_balance', 'Starting Balance', 'শুরু ভারসাম্য', 'Balance inicial', 'الرصيد الافتتاحي', 'Startguthaben', 'Solde de départ', 'Saldo iniziale', 'Начиная Баланс', 'Başlangıç ​​Bakiyesi'),
(104, 'manage_accounts', 'Manage Accounts', 'অ্যাকাউন্ট পরিচালনা', 'Administrar cuentas', 'إدارة الحسابات', 'Konten verwalten', 'Gérer les comptes', 'Gestisci account', 'Управление учетными записями', 'Hesapları Yönet'),
(105, 'current_balance', 'Current Balance', 'বর্তমান জমাখরচ', 'Saldo actual', 'الرصيد الحالي', 'Aktueller Kontostand', 'Solde actuel', 'Bilancio corrente', 'Текущий баланс', 'Cari Denge'),
(106, 'view_ledger', 'View Ledger', 'দেখুন লেজার', 'Ver Ledger', 'عرض ليدجر', 'Ansicht Ledger', 'Voir Ledger', 'Vista Ledger', 'Посмотреть Леджер', 'Görünüm Ledger'),
(107, 'edit_account', 'Edit Account', 'সম্পাদনা অ্যাকাউন্ট', 'Editar cuenta', 'تحرير الحساب', 'Account bearbeiten', 'Modifier le compte', 'Modifica account', 'Редактировать аккаунт', 'Hesabı Düzenle'),
(108, 'ledger', 'Ledger', 'খতিয়ান', 'Libro mayor', 'ليدجر', 'Hauptbuch', 'Grand livre', 'Libro mastro', 'Бухгалтерская книга', 'Defteri kebir'),
(109, 'date', 'Date', 'তারিখ', 'Fecha', 'التاريخ', 'Datum', 'date', 'Data', 'Дата', 'Tarih'),
(110, 'title', 'Title', 'খেতাব', 'Título', 'العنوان', 'Titel', 'Titre', 'Titolo', 'заглавие', 'Başlık'),
(111, 'credit', 'Credit', 'জমা', 'Crédito', 'ائتمان', 'Kredit', 'Crédit', 'Credito', 'Кредит', 'Kredi'),
(112, 'debit', 'Debit', 'ডেবিট', 'Débito', 'مدين', 'Debit', 'Débit', 'Addebito', 'Дебет', 'Zimmet'),
(113, 'total_credit', 'Total Credit', 'মোট ক্রেডিট', 'Crédito total', 'إجمالي الائتمان', 'Gesamt-Credit', 'Crédit total', 'Crediti', 'Всего Кредитная', 'Toplam Kredi'),
(114, 'total_debit', 'Total Debit', 'খরচের অঙ্ক', 'Débito total', 'إجمالي الخصم', 'Gesamtbank', 'Débit total', 'Debito totale', 'Всего Дебет', 'Toplam Borç'),
(115, 'balance', 'Balance', 'ভারসাম্য', 'Equilibrar', 'الرصيد', 'Balance', 'Équilibre', 'Bilancio', 'Баланс', 'Denge'),
(116, 'print', 'Print', 'ছাপা', 'Impresión', 'طباعة', 'Drucken', 'Imprimer', 'Stampare', 'Распечатать', 'Yazdır'),
(117, 'manage_products', 'Manage Products', 'পণ্য পরিচালনা', 'Manejo de Productos', 'إدارة المنتجات', 'Produkte verwalten', 'Gérer Produits', 'Gestione Prodotti', 'Управление продуктами', 'Ürünler yönetin'),
(118, 'add_new_product', 'Add New Product', 'নতুন পণ্য যোগ করুন', 'Añadir Nuevo Producto', 'إضافة منتج جديد', 'Neues Produkt hinzufügen', 'Ajouter un nouveau produit', 'Aggiungi nuovo prodotto', 'Добавить новый продукт', 'Yeni Ürün Ekle'),
(119, 'category', 'Category', 'শ্রেণী', 'Categoría', 'فئة', 'Kategorie', 'Catégorie', 'Categoria', 'Категория', 'Kategori'),
(120, 'price', 'Price', 'মূল্য', 'Precio', 'السعر', 'Preis', 'Prix', 'Prezzo', 'Цена', 'Fiyat'),
(121, 'add_product', 'Add Product', 'পণ্য যোগ করুন', 'Añadir Producto', 'إضافة منتج', 'Produkt hinzufügen', 'Ajouter un produit', 'Aggiungi prodotto', 'Добавить продукт', 'Ürün Ekle'),
(122, 'product_name', 'Product Name', 'পণ্যের নাম', 'nombre de producto', 'اسم المنتج', 'Produktname', 'Nom du produit', 'nome del prodotto', 'наименование товара', 'Ürün adı'),
(123, 'product_category', 'Product Category', 'পণ্য তালিকা', 'Categoria del Producto', 'فئة المنتج', 'Produktkategorie', 'catégorie de produit', 'Categoria del prodotto', 'категория продукта', 'ürün kategorisi'),
(124, 'unit_price', 'Unit Price', 'একক মূল্য', 'Precio unitario', 'سعر الوحدة', 'Einzelpreis', 'Prix ​​unitaire', 'Prezzo unitario', 'Цена за единицу', 'Birim fiyat'),
(125, 'edit_product', 'Edit Product', 'সম্পাদনা', 'Editar Producto', 'تحرير المنتج', 'Produkt bearbeiten', 'Modifier produit', 'Modifica del prodotto', 'Редактировать товаров', 'Düzenleme Ürün'),
(126, 'manage_services', 'Manage Services', 'সার্ভিস পরিচালনা', 'Administrar servicios', 'إدارة الخدمات', 'Dienstleistungen verwalten', 'Gérer les services', 'Gestione servizi', 'Управление службами', 'Hizmetler yönet'),
(127, 'add_new_service', 'Add New Service', 'নতুন পরিষেবা যোগ', 'Agregar nuevo servicio', 'إضافة خدمة جديدة', 'Neuen Service hinzufügen', 'Ajouter un nouveau service', 'Aggiungi nuovo servizio', 'Добавить новый сервис', 'Yeni Hizmet Ekle'),
(128, 'add_service', 'Add Service', 'পরিষেবা করো', 'Añadir Servicio', 'إضافة خدمة', 'Dienst hinzufügen', 'Ajouter un service', 'Aggiungi servizio', 'Добавить службу', 'Hizmet Ekle'),
(129, 'service_name', 'Service Name', 'কাজের নাম', 'Nombre del Servicio', 'اسم الخدمة', 'Dienstname', 'Nom du service', 'Nome del servizio', 'наименование услуги', 'hizmet adı'),
(130, 'service_category', 'Service Category', 'সার্ভিস ক্যাটাগরি', 'Categoría de Servicio', 'خدمة الفئة', 'Service-Kategorie', 'Catégorie de service', 'Servizio Categoria', 'Категория сервиса', 'Servis Kategorisi'),
(131, 'edit_service', 'Edit Service', 'সম্পাদনা পরিষেবা', 'Editar Servicio', 'تحرير الخدمات', 'Dienst bearbeiten', 'Modifier un service', 'Modifica servizio', 'Редактировать служба', 'Düzenleme Hizmeti'),
(132, 'manage_product_/_service_categories', 'Manage Product / Service Categories', 'প্রোডাক্ট / সার্ভিস বিভাগ গালাগাল', 'Manejo de producto / servicio Categorías', 'إدارة المنتج / الخدمة الفئات', 'Verwalten Produkte / Service Kategorien', 'Gérer / Catégories de services produit', 'Gestire prodotto / servizio Categorie', 'Управление продукт / услуга Категории', 'Ürün / Hizmet Kategorileri yönet'),
(133, 'add_new_category', 'Add New Category', 'নতুন বিভাগ যোগ', 'Añadir nueva categoria', 'إضافة فئة جديدة', 'Neue Kategorie hinzufügen', 'Ajouter une nouvelle catégorie', 'Aggiungi Nuova Categoria', 'Добавить новую категорию', 'Yeni Kategori Ekle'),
(134, 'category_name', 'Category Name', 'নামের তালিকা', 'Nombre de la categoría', 'اسم التصنيف', 'Name der Kategorie', 'Nom de catégorie', 'Nome della categoria', 'Категория Имя', 'Kategori adı'),
(135, 'add_category', 'Add Category', 'বিষয়শ্রেণী যোগ', 'Guardar Categoría', 'إضافة فئة', 'Kategorie hinzufügen', 'Ajouter Catégorie', 'Aggiungi categoria', 'Добавить категорию', 'Kategori Ekle'),
(136, 'edit_category', 'Edit Category', 'সম্পাদনা বিভাগ', 'Editar categoría', 'تحرير الفئة', 'Kategorie bearbeiten', 'Modifier une catégorie', 'Modifica categoria', 'Редактировать Категория', 'Kategori Düzenle'),
(137, 'add_new_purchase', 'Add New Purchase', 'নতুন ক্রয় করো', 'Añadir Nueva Compra', 'إضافة شراء الجديد', 'In New Kauf', 'Ajouter une nouvelle Achat', 'Aggiungi nuovo acquisto', 'Добавить новое приобретение', 'Yeni Satınalma ekle'),
(138, 'basic_information', 'Basic Information', 'মৌলিক তথ্য', 'Información básica', 'معلومات اساسية', 'Grundinformation', 'Informations de base', 'Informazioni di base', 'Основная информация', 'Temel Bilgiler'),
(139, 'due_date', 'Due Date', 'নির্দিষ্ট তারিখ', 'Fecha de vencimiento', 'التاريخ المقرر', 'Geburtstermin', 'Date d''échéance', 'Scadenza', 'Срок', 'Bitiş tarihi'),
(140, 'add_product_/_service', 'Add Product / Service', 'প্রোডাক্ট / সার্ভিস যোগ', 'Añadir Producto / Servicio', 'إضافة منتج / خدمة', 'Fügen Sie Produkte / Service', 'Ajouter Produit / Service', 'Aggiungi Prodotto / Servizio', 'Добавить товар / услуга', 'Ürün / Hizmet Ekle'),
(141, 'add_a_product_/_service', 'Add A Product / Service', 'একটি প্রোডাক্ট / সার্ভিস যোগ', 'Añadir un producto / servicio', 'إضافة منتج / خدمة', 'Fügen Sie Produkt / Dienstleistung', 'Ajouter un produit / service', 'Aggiungere un prodotto / servizio', 'Добавить товар / услугу', 'A Ürün / Hizmet Ekle'),
(142, 'product_code', 'Product Code', 'প্রোডাক্ট কোড', 'Código de producto', 'رمز المنتج', 'Produktcode', 'Code produit', 'Codice di produzione', 'Код продукта', 'Ürün Kodu'),
(143, 'quantity', 'Quantity', 'পরিমাণ', 'Cantidad', 'كمية', 'Menge', 'Quantité', 'Quantità', 'Количество', 'Nicelik'),
(144, 'total', 'Total', 'মোট', 'Total', 'الإجمالي الكلي', 'Gesamt', 'Le total', 'Totale', 'Всего', 'Toplam'),
(145, 'payment_information', 'Payment Information', 'পেমেন্ট তথ্য', 'Información del pago', 'معلومات الدفع', 'Zahlungsinformationen', 'Information de Paiement', 'Informazioni sul pagamento', 'Платежная информация', 'ödeme bilgileri'),
(146, 'sub_total', 'Sub Total', 'উপ মোট', 'Total parcial', 'جنوب إجمالي', 'Zwischensumme', 'Total', 'Sub totale', 'Промежуточный итог', 'Alt Toplam'),
(147, 'VAT', 'VAT', 'ভ্যাট', 'TINA', 'ضريبة', 'Mehrwertsteuer', 'T.V.A.', 'I.V.A.', 'НДС', 'DKV'),
(148, 'select_VAT', 'Select VAT', 'নির্বাচন ভ্যাট', 'Seleccione el IVA', 'اختر VAT', 'Wählen MwSt', 'Sélectionnez la TVA', 'Seleziona IVA', 'Выберите НДС', 'Seçin KDV'),
(149, 'no_VAT', 'No VAT', 'কোন ভ্যাট', 'Sin IVA', 'لا ضريبة القيمة المضافة', 'Keine MwSt', 'Pas de TVA', 'No IVA', 'Нет НДС', 'KDV'),
(150, 'discount_amount', 'Discount Amount', 'হ্রাসকৃত মুল্য', 'Importe de descuento', 'مقدار الخصم', 'Rabattbetrag', 'Montant de la remise', 'Totale sconto', 'Сумма скидки', 'İndirim tutarı'),
(151, 'grand_total', 'Grand Total', 'সর্বমোট', 'Gran total', 'المجموع الإجمالي', 'Endsumme', 'somme finale', 'Somma totale', 'Общая сумма', 'Genel Toplam'),
(152, 'financial_account', 'Financial Account', 'আর্থিক বিবরণ', 'Cuenta financiera', 'حساب مالي', 'Bankkonto', 'Compte financier', 'Conto finanziario', 'Финансовый счет', 'Finans Hesabı'),
(153, 'select_an_account', 'Select An Account', 'একটি একাউন্ট নির্বাচন করুন', 'Seleccione una cuenta', 'حدد حسابا', 'Wählen Sie ein Konto', 'Sélectionnez un compte', 'Selezionare un account', 'Выберите учетную запись', 'Hesap seçin'),
(154, 'bank_accounts', 'Bank Accounts', 'ব্যাংক হিসাব', 'Cuentas bancarias', 'حسابات بنكية', 'Bankkonten', 'Comptes bancaires', 'Conto in banca', 'Банковские счета', 'Banka hesabı'),
(155, 'cash_accounts', 'Cash Accounts', 'নগদ হিসাব', 'Cuentas de efectivo', 'الحسابات النقدية', 'Geldkonten', 'Comptes de trésorerie', 'Conti di cassa', 'Денежные счета', 'Nakit Hesapları'),
(156, 'other_accounts', 'Other Accounts', 'অন্যান্য অ্যাকাউন্ট', 'Otras Cuentas', 'حسابات أخرى', 'Weitere Konten', 'Autres comptes', 'Altri account', 'Другие счета', 'Diğer Hesaplar'),
(157, 'create_new_purchase', 'Create New Purchase', 'নতুন ক্রয় তৈরি', 'Crear nueva Compra', 'إنشاء شراء الجديد', 'Neu erstellen Kauf', 'Créer un nouveau Achat', 'Crea nuovo acquisto', 'Создать новое приобретение', 'Yeni Satınalma Oluştur'),
(158, 'manage_purchases', 'Manage Purchases', 'ক্রয় সেকেন্ড', 'Manejo de Compras', 'إدارة المشتريات', 'Käufe verwalten', 'Gérer achats', 'Gestire Acquisti', 'Управление Закупки', 'Satın yönetin'),
(159, 'edit_purchase', 'Edit Purchase', 'সম্পাদনা ক্রয়', 'Editar Compra', 'تحرير شراء', 'Kauf bearbeiten', 'Modifier Achat', 'Modifica Acquisto', 'Редактировать Покупка', 'Düzenleme Satınalma'),
(160, 'update_purchase', 'Update Purchase', 'আপডেটের জন্য ক্রয়', 'Actualización de compra', 'تحديث الشراء', 'Update Kauf', 'Mise à jour Achat', 'Aggiornamento Acquisto', 'Обновление Покупка', 'Güncelleme Satınalma'),
(161, 'purchase_invoice_details', 'Purchase Invoice Details', 'চালান বিবরণ ক্রয়', 'Compra Factura Detalles', 'شراء تفاصيل الفاتورة', 'Erwerben Rechnungsdetails', 'Achetez Détails de la facture', 'Acquista Dettagli fattura', 'Покупка счете-фактуре', 'Fatura Bilgileri Satınalma'),
(162, 'purchase_invoice', 'Purchase Invoice', 'ক্রয় চালান', 'Compra Factura', 'شراء فاتورة', 'Kaufrechnung', 'Facture d''achat', 'Acquisto Fattura', 'Счет заказа', 'Satınalma faturası'),
(163, 'from', 'From', 'থেকে', 'De', 'من عند', 'Von', 'De', 'Da parte di', 'Из', 'Gönderen'),
(164, 'purchase_items', 'Purchase Items', 'ক্রয় আইটেম', 'Comprar artículos', 'شراء الأصناف', 'Kaufgegenstände', 'Achat Articles', 'Acquisto di oggetti', 'Покупка товары', 'Satınalma Kalemleri'),
(165, 'vat', 'Vat', 'ভাঁটি', 'Tina', 'ضريبة القيمة المضافة', 'Bütte', 'T.V.A', 'I.V.A', 'НДС', 'Fıçı'),
(166, 'discount', 'Discount', 'ডিসকাউন্ট', 'Descuento', 'تخفيض السعر', 'Rabatt', 'Remise', 'Sconto', 'Скидка', 'İndirim'),
(167, 'email_invoice', 'Email Invoice', 'ইমেইল চালান', 'Email Factura', 'البريد الإلكتروني الفاتورة', 'E-Mail-Rechnung', 'Email facture', 'Email Fattura', 'E-mail счет-фактура', 'E-posta Fatura'),
(168, 'vat', 'Vat', 'ভাঁটি', 'Tina', 'ضريبة القيمة المضافة', 'Bütte', 'T.V.A', 'I.V.A', 'НДС', 'Fıçı'),
(169, 'invoice', 'Invoice', 'চালান', 'Factura', 'فاتورة', 'Rechnung', 'Facture d''achat', 'Fattura', 'Выставленный счет', 'Fatura'),
(170, 'invoice_emailed_successfuly', 'Invoice Emailed Successfuly', 'চালান ইমেল Successfuly', 'Factura enviadas por correo electrónico exitosamente', 'فاتورة بالبريد الالكتروني بنجاح', 'Rechnungs Emailed Erfolgreicher', 'Facture Envoyé avec succès', 'Fattura Inviato con successo', 'Счета по электронной почте успешно', 'Fatura Emailed başarıyla'),
(171, 'vat', 'Vat', 'ভাঁটি', 'Tina', 'ضريبة القيمة المضافة', 'Bütte', 'T.V.A', 'I.V.A', 'НДС', 'Fıçı'),
(172, 'add_new_sale', 'Add New Sale', 'নতুন বিক্রয় যোগ', 'Añadir nueva venta', 'إضافة بيع الجديد', 'In New Sale', 'Ajouter un nouveau Vente', 'Aggiungi nuovo Vendita', 'Добавить Продажа', 'Yeni Sale ekle'),
(173, 'create_new_sale', 'Create New Sale', 'নতুন বিক্রয় তৈরি', 'Crear nueva venta', 'إنشاء بيع الجديد', 'Neues Sale', 'Créer un nouveau Vente', 'Crea nuovo Vendita', 'Создать Продажа', 'Yeni Sale Oluştur'),
(174, 'manage_sales', 'Manage Sales', 'সেলস পরিচালনা', 'Manejo de Ventas', 'إدارة المبيعات', 'Verkäufe verwalten', 'Gérer ventes', 'Gestire le vendite', 'Управление продаж', 'Satış yönetin'),
(175, 'edit_sale', 'Edit Sale', 'সম্পাদনা বিক্রয়', 'Editar Venta', 'تحرير بيع', 'Verkauf Bearbeiten', 'Modifier Vente', 'Modifica Vendita', 'Редактировать Продажа', 'Düzenleme Satış'),
(176, 'update_sale', 'Update Sale', 'আপডেট বিক্রয়', 'Actualización de Venta', 'تحديث بيع', 'Update Verkauf', 'Mise à jour de Vente', 'Aggiornamento Vendita', 'Обновление Продажа', 'Güncelleme Satış'),
(177, 'sale_invoice_details', 'Sale Invoice Details', 'চালান বিবরণ বিক্রয়', 'Venta Factura Detalles', 'بيع تفاصيل الفاتورة', 'Verkauf Rechnungsdetails', 'Vente Détails de la facture', 'Vendita Dettagli fattura', 'Продажа счете-фактуре', 'Fatura Bilgileri Sale'),
(178, 'sale_invoice', 'Sale Invoice', 'বিক্রয় চালান', 'Venta Factura', 'بيع الفاتورة', 'Verkauf Rechnung', 'Vente facture', 'Vendita Fattura', 'Продажа Счет', 'Satış Faturası'),
(179, 'sale_items', 'Sale Items', 'বিক্রয় আইটেম', 'Artículos a la venta', 'بيع الأدوات', 'Sale Items', 'Vendre des articles', 'Articoli in saldo', 'Продажа предметов', 'Satış Öğeler'),
(180, 'vat', 'Vat', 'ভাঁটি', 'Tina', 'ضريبة القيمة المضافة', 'Bütte', 'T.V.A', 'I.V.A', 'НДС', 'Fıçı'),
(181, 'manage_incomes', 'Manage Incomes', 'আয় গালাগাল প্রতিবেদন', 'Manejo de Ingresos', 'إدارة الدخل', 'Die Einkommen verwalten', 'Gérer revenus', 'Gestire redditi', 'Управление Доходы', 'Gelirleri yönetin'),
(182, 'add_new_income', 'Add New Income', 'গেম আয় যুক্ত করুন', 'Añadir Nuevo Ingreso', 'إضافة الدخل الجديد', 'In New Income', 'Ajouter un nouveau revenu', 'Aggiungi nuovo reddito', 'Добавить Новые поступления', 'Yeni Gelir ekle'),
(183, 'description', 'Description', 'বিবরণ', 'Descripción', 'الوصف', 'Beschreibung', 'Description', 'Descrizione', 'Описание', 'Açıklama'),
(184, 'income_/_expense_category', 'Income / Expense Category', 'আয় / ব্যয় বিভাগ', 'Ingreso / Gasto Categoría', 'الدخل / المصاريف الفئة', 'Erträge / Aufwendungen Kategorie', 'Produits / charges Catégorie', 'Proventi / Oneri Categoria', 'Доходы / расходы Категория', 'Gelir / Gider Kategori'),
(185, 'add_income', 'Add Income', 'আয় করো', 'Añadir Ingresos', 'إضافة الدخل', 'In Income', 'Ajouter revenu', 'Aggiungere reddito', 'Добавить положение', 'Gelir ekle'),
(186, 'edit_income', 'Edit Income', 'সম্পাদনা আয়', 'Editar Ingresos', 'تحرير الدخل', 'Bearbeiten Income', 'Modifier le revenu', 'Modifica reddito', 'Редактировать прибыль', 'Düzenleme Gelir'),
(187, 'manage_expenses', 'Manage Expenses', 'খরচ পরিচালনা', 'Gestionar los gastos', 'إدارة النفقات', 'Ausgaben verwalten', 'Gérer les dépenses', 'Gestire le spese', 'Управление расходы', 'Giderleri yönetin'),
(188, 'add_new_expense', 'Add New Expense', 'নতুন ব্যয় করো', 'Agregar nuevo Gasto', 'إضافة حساب جديد', 'In New Expense', 'Ajouter nouvelle dépense', 'Aggiungi nuovo Expense', 'Добавить Расход', 'Yeni Gider ekle'),
(189, 'add_expense', 'Add Expense', 'ব্যয় করো', 'Añadir Gasto', 'إضافة حساب', 'Expense hinzufügen', 'Ajouter Expense', 'Aggiungere Expense', 'Добавить Расход', 'Gider ekle'),
(190, 'edit_expense', 'Edit Expense', 'সম্পাদনা ব্যয়', 'Editar Gasto', 'تحرير المصاريف', 'Expense bearbeiten', 'Modifier Expense', 'Modifica Expense', 'Редактировать расходов', 'Düzenleme Gider'),
(191, 'manage_income_/_expense_categories', 'Manage Income / Expense Categories', 'আয় / ব্যয় বিভাগ পরিচালনা', 'Administrar ingresos / gastos Categorías', 'إدارة الدخل / المصاريف الفئات', 'Verwalten Erträge / Aufwendungen Kategorien', 'Gérer Produits / charges Catégories', 'Gestire proventi / oneri categorie', 'Управление доходы / расходы Категории', 'Gelir / Gider Kategoriler yönetin'),
(192, 'manage_account_statements', 'Manage Account Statements', 'অ্যাকাউন্ট বিবৃতি পরিচালনা', 'Manejo de Estados de Cuenta', 'إدارة كشوف الحساب', 'Verwalten Kontoauszüge', 'Gérer relevés de compte', 'Gestire Estratti conto', 'Управление выписки со счета', 'Hesap İfadeleri yönetin'),
(193, 'all_accounts', 'All Accounts', 'সমস্ত অ্যাকাউন্ট', 'Todas las Cuentas', 'جميع الحسابات', 'Alle Accounts', 'Tous les comptes', 'Tutti gli account', 'Все счета', 'Tüm Hesaplar'),
(194, 'transaction', 'Transaction', 'লেনদেন', 'Transacción', 'صفقة', 'Transaktion', 'Transaction', 'Transazione', 'Сделка', 'Işlem'),
(195, 'all_transactions', 'All Transactions', 'সকল লেনদেন', 'Todas las transacciones', 'كل الحركات المالية', 'Alle Transaktionen', 'toutes transactions', 'Tutte le transazioni', 'Все транзакции', 'Tüm İşlemler'),
(196, 'from_date', 'From Date', 'তারিখ থেকে', 'Desde la fecha', 'من التاريخ', 'Ab Datum', 'Partir de la date', 'Dalla Data', 'С даты', 'İtibaren'),
(197, 'to_date', 'To Date', 'এখন পর্যন্ত', 'Hasta la fecha', 'حتى تاريخه', 'Miteinander ausgehen', 'À ce jour', 'Ad oggi', 'Встретиться', 'Bugüne kadar'),
(198, 'filter', 'Filter', 'ফিল্টার', 'Filtrar', 'فلتر', 'Filter', 'Filtre', 'Filtro', 'Фильтр', 'Filtre'),
(199, 'total_income', 'Total Income', 'মোট আয়', 'Ingresos totales', 'إجمالي الدخل', 'Gesamteinkommen', 'Revenu total', 'Reddito totale', 'Общая прибыль', 'Toplam gelir'),
(200, 'income_bar', 'Income Bar', 'আয় বার', 'Bar Ingresos', 'بار الدخل', 'Income Bar', 'Bar de revenu', 'Reddito Bar', 'Доходы Бар', 'Gelir Bar'),
(201, 'current_financial_year_starts_from', 'Current Financial Year Starts From', 'চলতি অর্থবছরে থেকে শুরু', 'Ejercicio en curso se inicia desde', 'التيار السنة المالية تبدأ من', 'Laufende Geschäftsjahr geht von', 'EXERCICE EN COURS commence à partir de', 'Attuale Esercizio Prezzi da', 'Текущий финансовый год начинается с', 'Cari Mali Yıl Gönderen Başlıyor'),
(202, 'january', 'January', 'জানুয়ারী', 'enero', 'كانون الثاني', 'Januar', 'janvier', 'gennaio', 'Января', 'Ocak'),
(203, 'edit_financial_year_settings', 'Edit Financial Year Settings', 'সম্পাদনা অর্থবছরে সেটিংস', 'Editar configuración de ejercicio', 'تعديل الإعدادات السنة المالية', 'Bearbeiten Geschäftsjahr Einstellungen', 'Modifier les paramètres Exercice', 'Modifica impostazioni Esercizio', 'Изменить настройки финансового года', 'Düzenleme Mali Yılı Ayarlarını'),
(204, 'income_percentage', 'Income Percentage', 'আয় শতকরা', 'Porcentaje de Ingresos', 'نسبة الدخل', 'Gewinn- und Verlustprozent', 'Pourcentage du revenu', 'Reddito Percentuale', 'Доход в процентах', 'Gelir Yüzde'),
(205, 'total_expense', 'Total Expense', 'মোট ব্যয়', 'Gasto total', 'مجموع النفقات', 'Gesamtausgaben', 'Total Expense', 'Total Expense', 'Всего расходов', 'Toplam Gider'),
(206, 'expense_bar', 'Expense Bar', 'ব্যয় বার', 'Gasto Bar', 'حساب بار', 'Expense Bar', 'Dépenses Bar', 'Expense Bar', 'Расходы Бар', 'Gider Bar'),
(207, 'expense_percentage', 'Expense Percentage', 'ব্যয় শতকরা', 'Gasto Porcentaje', 'حساب النسبة المئوية', 'Expense Prozent', 'Dépenses Pourcentage', 'Percentuale spese', 'Расходы в процентах', 'Gider Yüzde'),
(208, 'income_expense_comparison_report', 'Income Expense Comparison Report', 'আয় ব্যয় তুলনা প্রতিবেদন', 'Gastos Ingresos Comparación Reportar', 'نفقات الدخل مقارنة تقرير', 'Income Expense Vergleichsbericht', 'Dépenses de revenu Rapport de comparaison', 'Proventi Oneri report di confronto', 'Прибыль Сравнение Сообщить', 'Gelir Gider Raporu Karşılaştırma'),
(209, 'income_expense_percentage', 'Income Expense Percentage', 'আয় ব্যয় শতকরা', 'Gastos Ingresos Porcentaje', 'نفقات دخل النسبة المئوية', 'Income Expense Prozent', 'Dépenses de revenu Pourcentage', 'Proventi Oneri Percentuale', 'Расходы в процентах доходов', 'Gelir Gider Yüzde'),
(210, 'manage_notes', 'Manage Notes', 'নোট পরিচালনা', 'Administrar Notas', 'إدارة الملاحظات', 'Notizen verwalten', 'Gérer des notes', 'Gestisci note', 'Управление Примечания', 'Notlar yönetin'),
(211, 'create_note', 'Create Note', 'নোট তৈরি', 'Crear Nota', 'إنشاء ملاحظة', 'Erstellen Hinweis', 'Créer Remarque', 'Crea nota', 'Создать Примечание', 'Not oluştur'),
(212, 'untitled', 'Untitled', 'শিরোনামহীন', 'Intitulado', 'بدون عنوان', 'Ohne Titel', 'Sans titre', 'Senza titolo', 'Без названия', 'Başlıksız'),
(213, 'save_note', 'Save Note', 'নোট সংরক্ষণ করুন', 'Guardar Nota', 'حفظ ملاحظة', 'Notiz speichern', 'Enregistrer la note', 'Salva Nota', 'Сохранить Примечание', 'Kaydet Not'),
(214, 'delete_note', 'Delete Note', 'নোট মুছে', 'Eliminar Nota', 'حذف ملاحظة', 'Hinweis Löschen', 'Supprimer Note', 'Elimina Nota', 'Удалить Примечание', 'Not sil'),
(215, 'admin_list', 'Admin List', 'অ্যাডমিন তালিকা', 'Lista de administración', 'قائمة المشرف', 'Admin Liste', 'Liste Admin', 'Lista Admin', 'Список Админ', 'Yönetici Listesi'),
(216, 'add_new_admin', 'Add New Admin', 'নতুন অ্যাডমিন যোগ', 'Añadir nuevo administrador', 'إضافة جديد الادارية', 'In New Admin', 'Ajouter nouvel admin', 'Aggiungi nuovo amministratore', 'Добавить Администратор', 'Yeni Yönetici Ekle'),
(217, 'add_admin', 'Add Admin', 'অ্যাডমিন যোগ', 'Añadir administración', 'إضافة الادارية', 'In Admin', 'Ajouter admin', 'Aggiungere Admin', 'Добавить Администратор', 'Yönetici ekle'),
(218, 'system_title', 'System Title', 'সিস্টেম শিরোনাম', 'Sistema Título', 'نظام العنوان', 'System Titel', 'Système titre', 'Titolo di sistema', 'Система Название', 'Sistem Başlığı'),
(219, 'system_email', 'System Email', 'সিস্টেম ইমেইল', 'Sistema de Correo', 'نظام البريد الإلكتروني', 'E-Mail-System', 'Système Email', 'Email sistema', 'Система E-mail', 'Sistem E-posta'),
(220, 'text_align', 'Text Align', 'টেক্সট সারিবদ্ধ', 'Texto Alinear', 'محاذاة النص', 'Text Align', 'Text Align', 'Allinea il testo', 'Text Align', 'Metin Hizala'),
(221, 'language', 'Language', 'ভাষা', 'idioma', 'لغة', 'Sprache', 'Langue', 'Lingua', 'Язык', 'Dil'),
(222, 'system_currency', 'System Currency', 'সিস্টেম মুদ্রা', 'Moneda del sistema', 'نظام العملات', 'Systemwährung', 'Système devise', 'Sistema di valuta', 'Система валют', 'Sistem Para'),
(223, 'show_price_as', 'Show Price As', 'দেখান মূল্য হিসাবে', 'Mostrar Precio Como', 'عرض الأسعار و', 'Fragen Sie den Preis als', 'Voir les prix que', 'Chiedi il prezzo Come', 'Показать цену как', 'Icin arayabilirsiniz As'),
(224, 'financial_year_start', 'Financial Year Start', 'আর্থিক বছরের শুরু', 'Ejercicio de inicio', 'بداية السنة المالية', 'Geschäftsjahr starten', 'Année financière Démarrer', 'Esercizio Inizio', 'Финансовый год Начало', 'Mali Yıl Başlangıç'),
(225, 'from_january', 'From January', 'জানুয়ারি থেকে', 'A partir de enero', 'خلال الفترة من يناير', 'Von Januar', 'De Janvier', 'Da gennaio', 'С января', 'Ocak');
INSERT INTO `bk_language` (`phrase_id`, `phrase`, `en`, `bn`, `es`, `ar`, `de`, `fr`, `it`, `ru`, `tr`) VALUES
(226, 'from_july', 'From July', 'জুলাই থেকে', 'A partir de julio', 'من يوليو', 'Von Juli', 'De Juillet', 'Dal luglio', 'С июля', 'Temmuz-'),
(227, 'save', 'Save', 'সংরক্ষণ', 'Ahorrar', 'حفظ', 'sparen', 'Conserver', 'Salvare', 'Сохранить', 'Kaydet'),
(228, 'upload_logo', 'Upload Logo', 'আপলোড লোগো', 'Subir Logo', 'تحميل الشعار', 'Logo hochladen', 'Upload Logo', 'Upload Logo', 'Загрузить логотип', 'Yükleme Logosu'),
(229, 'upload', 'Upload', 'আপলোড', 'Subir', 'الرفع', 'Hochladen', 'Télécharger', 'Caricare', 'Загрузить', 'Yükleme'),
(230, 'email_template_settings', 'Email Template Settings', 'ইমেইল টেমপ্লেট সেটিংস', 'Configuración de plantilla de correo electrónico', 'إعدادات قالب البريد الإلكتروني', 'E-Mail-Template-Einstellungen', 'Paramètres Email Template', 'Impostazioni e-mail dei modelli', 'Настройки электронной почты шаблона', 'E-posta Şablon Ayarları'),
(231, 'new_admin_account_opening', 'New Admin Account Opening', 'নতুন অ্যাডমিন অ্যাকাউন্ট খোলা', 'Nueva Apertura Administración de cuentas', 'جديد الادارة فتح الحساب', 'New Admin Kontoeröffnung', 'Nouvelle ouverture de compte Administrateur', 'Nuova apertura Amministratore account', 'Новый Админ Открытие счета', 'Yeni Yönetici Hesabı Açılışı'),
(232, 'password_reset_confirmation', 'Password Reset Confirmation', 'পাসওয়ার্ড রিসেট নিশ্চিতকরণ', 'Restablecer contraseña Confirmación', 'إعادة تعيين كلمة المرور تأكيد', 'Password Reset Confirmation', 'Password Reset Confirmation', 'Password Reset Conferma', 'Сброс пароля Подтверждение', 'Parola Sıfırlama Onayı'),
(233, 'email_subject', 'Email Subject', 'ইমেইল বিষয়', 'Asunto del correo', 'موضوع البريد الإلكتروني', 'E-Mail Betreff', 'Sujet du courriel', 'Oggetto dell''email', 'Тема письма', 'E-posta konu'),
(234, 'email_body', 'Email Body', 'ইমেলের', 'Cuerpo del correo electronico', 'البريد الإلكتروني الجسم', 'Email Körper', 'Email du corps', 'Email Corpo', 'E-mail тела', 'E-posta Vücut'),
(235, 'save_template', 'Save Template', 'Save Template এ', 'Guardar plantilla', 'حفظ القالب', 'Vorlage speichern', 'Enregistrer le modèle', 'Salva modello', 'Сохранить шаблон', 'Şablonu Kaydet'),
(236, 'manage_language', 'Manage Language', 'ভাষা পরিচালনা', 'Administrar Idioma', 'إدارة اللغة', 'Sprache verwalten', 'Gérer Langue', 'Gestire Lingua', 'Управление Язык', 'Dil yönetin'),
(237, 'language_list', 'Language List', 'নতুন ভাষাটি তালিকায় আগে', 'Lista Idioma', 'قائمة لغة', 'Sprachenliste', 'Liste de Langue', 'Elenco lingue', 'Список языков', 'Dil Listesi'),
(238, 'add_phrase', 'Add Phrase', 'শব্দবন্ধ যোগ', 'Añadir Frase', 'إضافة العبارة', 'Phrase hinzufügen', 'Ajouter Phrase', 'Aggiungere Frase', 'Добавить фразу', 'Cümle ekle'),
(239, 'add_language', 'Add Language', 'নতুন ভাষা যোগ করা', 'Agregar idioma', 'إضافة اللغة', 'Sprache hinzufügen', 'Ajouter une langue', 'Aggiungi lingua', 'Добавить язык', 'Dil ekle'),
(240, 'option', 'Option', 'পছন্দ', 'Opción', 'خيار', 'Option', 'Option', 'Opzione', 'Вариант', 'Seçenek'),
(241, 'edit_phrase', 'Edit Phrase', 'সম্পাদনা শব্দবন্ধ', 'Editar Frase', 'تحرير العبارة', 'Phrase bearbeiten', 'Modifier Phrase', 'Modifica Frase', 'Редактировать Фраза', 'Düzenleme Cümle'),
(242, 'write_language_file', 'Write Language File', 'ভাষা ফাইল লিখুন', 'Escribe Archivo Idioma', 'إرسال ملف اللغة', 'Schreiben Sprache Datei', 'Ecrire fichier Langue', 'Scrivi file Lingua', 'Написать языковой файл', 'Dil Dosyası yaz'),
(243, 'delete_language', 'Delete Language', 'ভাষা মুছে', 'Eliminar Idioma', 'حذف اللغة', 'Sprache löschen', 'Supprimer Langue', 'Eliminare Lingua', 'Удалить Язык', 'Dil Sil'),
(244, 'phrase', 'Phrase', 'শব্দবন্ধ', 'Frase', 'العبارة', 'Phrase', 'Phrase', 'Frase', 'Фраза', 'İfade'),
(245, 'value_required', 'Value Required', 'মূল্য প্রয়োজন', 'Valor Obligatorio', 'القيمة المطلوبة', 'Wert Erforderlich', 'Valeur Obligatoire', 'Valore Obbligatorio', 'Значение Обязательно', 'Değer Gerekli'),
(246, 'update_phrase', 'Update Phrase', 'আপডেট শব্দবন্ধ', 'Actualización Frase', 'تحديث العبارة', 'Update-Satz', 'Mise à jour Phrase', 'Aggiornamento Frase', 'Обновление Фраза', 'Güncelleme Cümle'),
(247, 'manage_vat_settings', 'Manage Vat Settings', 'ভ্যাট সেটিংস পরিচালনা করুন', 'Administrar configuración IVA', 'إدارة إعدادات ضريبة القيمة المضافة', 'MwSt-Einstellungen verwalten', 'Gérer les paramètres Vat', 'Gestisci impostazioni Iva', 'Управление настройками Vat', 'Vat Ayarlarını Yönet'),
(248, 'add_new_vat', 'Add New Vat', 'নতুন ভ্যাট যোগ', 'Agregar nuevo IVA', 'إضافة ضريبة القيمة المضافة الجديدة', 'In New Vat', 'Ajouter un nouveau Vat', 'Aggiungi nuovo Iva', 'Добавить Vat', 'Yeni Vat ekle'),
(249, 'vat_name', 'Vat Name', 'ভ্যাট নাম', 'Vat Nombre', 'اسم ضريبة القيمة المضافة', 'Vat Namen', 'Vat Nom', 'Iva Nome', 'НДС Имя', 'Vat Adı'),
(250, 'percentage', 'Percentage', 'শতকরা হার', 'Porcentaje', 'نسبة مئوية', 'Prozentsatz', 'Pourcentage', 'Percentuale', 'Процент', 'Yüzde'),
(251, 'add_vat', 'Add Vat', 'ভ্যাট যোগ', 'Añadir Vat', 'إضافة ضريبة القيمة المضافة', 'Vat hinzufügen', 'Ajouter Vat', 'Aggiungere Iva', 'Добавить Vat', 'Vat ekle'),
(252, 'edit_vat', 'Edit Vat', 'সম্পাদনা জালা', 'Editar Vat', 'تحرير ضريبة القيمة المضافة', 'Vat bearbeiten', 'Modifier Vat', 'Modifica Iva', 'Редактировать НДС', 'Düzenleme Vat'),
(253, 'manage_profile', 'Manage Profile', 'অমিমাংসীত সংস্করণ লগ', 'Administrar perfil', 'إدارة الملف', 'Profil verwalten', 'Gérer le profil', 'Gestisci profilo', 'Управление профиля', 'Profilinizi Yönetin'),
(254, 'edit_profile', 'Edit Profile', 'জীবন বৃত্তান্ত সম্পাদনা', 'Editar perfil', 'تعديل الملف الشخصي', 'Profil bearbeiten', 'Modifier le profil', 'Modifica Profilo', 'Редактировать профиль', 'Profili Düzenle'),
(255, 'update_profile', 'Update Profile', 'প্রফাইল হালনাগাদ', 'Actualización del perfil', 'تحديث الملف', 'Profil aktualisieren', 'Mettre à jour le profil', 'Aggiorna il profilo', 'Обновить профиль', 'Profili güncelle'),
(256, 'change_password', 'Change Password', 'পাসওয়ার্ড পরিবর্তন করুন', 'Cambiar la contraseña', 'تغيير كلمة السر', 'Kennwort ändern', 'Changer le mot de passe', 'Cambiare la password', 'Изменить пароль', 'Şifre değiştir'),
(257, 'current_password', 'Current Password', 'বর্তমান পাসওয়ার্ড', 'contraseña actual', 'كلمة السر الحالية', 'Aktuelles Passwort', 'Mot de passe actuel', 'Password attuale', 'текущий пароль', 'Şimdiki Şifre'),
(258, 'new_password', 'New Password', 'নতুন পাসওয়ার্ড', 'nueva contraseña', 'كلمة سر جديدة', 'Neues Kennwort', 'nouveau mot de passe', 'nuova password', 'новый пароль', 'Yeni Şifre'),
(259, 'confirm_new_password', 'Confirm New Password', 'নিশ্চিত কর নতুন গোপননম্বর', 'Confirmar nueva contraseña', 'تأكيد كلمة السر الجديدة', 'Bestätige neues Passwort', 'Confirmer le nouveau mot de passe', 'Conferma la nuova password', 'Подтвердите новый пароль', 'Yeni şifreyi onayla'),
(260, 'update_password', 'Update Password', 'আপডেট পাসওয়ার্ড', 'Actualiza contraseña', 'تحديث كلمة المرور', 'Passwort aktualisieren', 'Mise à jour Mot de passe', 'Aggiorna password', 'Обновление Пароль', 'Güncelleme Şifre');

-- --------------------------------------------------------

--
-- Table structure for table `bk_note`
--

DROP TABLE IF EXISTS `bk_note`;
CREATE TABLE IF NOT EXISTS `bk_note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `bk_note`
--

INSERT INTO `bk_note` (`note_id`, `title`, `note`, `user_id`) VALUES
(14, 'Enim non minima ex sint natus', 'Ex eos qui labore perspiciatis, corporis dolore magni in soluta Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.Ex eos qui labore perspiciatis, corporis dolore magni in soluta.', 1),
(13, 'Fugit rerum praesentium mollitia', 'Ad consequatur, nisi irure veniam, cillum illum, fugiat, accusamus est in minima voluptatibus  saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit saepe suscipit eu nesciunt dolore blanditiis qui amet ipsa aut magna eiusmod quia ducimus quam suscipit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bk_product`
--

DROP TABLE IF EXISTS `bk_product`;
CREATE TABLE IF NOT EXISTS `bk_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `product_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `price` longtext COLLATE utf8_unicode_ci NOT NULL,
  `notes` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bk_product`
--

INSERT INTO `bk_product` (`product_id`, `type`, `product_code`, `name`, `product_category_id`, `price`, `notes`) VALUES
(1, 'product', 'a9130c0db5', 'Fasttrack Wrist Watch', 2, '1499', 'Minus explicabo. Esse voluptatibus reprehenderit optio, Nam accusamus reprehenderit quis modi itaque enimmmmm'),
(2, 'service', '09f5249e4d', 'Home Delivery Service', 4, '20', 'Fugit, obcaecati in elit, ad aut soluta animi, velit, nihil do amet, in iste officiaaaaaa'),
(3, 'product', '6d5b00442d', 'iPhone 6s', 1, '700', 'Placeat, consequuntur quos magnam atque est, nostrud consequatur facilis qui cupidatat.'),
(4, 'service', '35a4427624', 'Catering Service', 4, '50', 'Molestiae consequuntur in ipsam quisquam est nobis repellendus. Ea qui fuga. Veritatis voluptas.');

-- --------------------------------------------------------

--
-- Table structure for table `bk_product_category`
--

DROP TABLE IF EXISTS `bk_product_category`;
CREATE TABLE IF NOT EXISTS `bk_product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bk_product_category`
--

INSERT INTO `bk_product_category` (`product_category_id`, `name`) VALUES
(1, 'Electronics'),
(2, 'Fashion'),
(4, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `bk_purchase`
--

DROP TABLE IF EXISTS `bk_purchase`;
CREATE TABLE IF NOT EXISTS `bk_purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `vat_id` int(11) NOT NULL,
  `discount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  `due_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bk_purchase`
--

INSERT INTO `bk_purchase` (`purchase_id`, `purchase_code`, `supplier_id`, `account_id`, `amount`, `vat_id`, `discount`, `creation_date`, `due_date`) VALUES
(1, '0dceace19d', 6, 3, '3518.95', 3, '50', '1452621600', '1454176800'),
(2, '17f2bbf883', 5, 3, '774', 3, '50', '1453312800', '1461780000');

-- --------------------------------------------------------

--
-- Table structure for table `bk_purchase_item`
--

DROP TABLE IF EXISTS `bk_purchase_item`;
CREATE TABLE IF NOT EXISTS `bk_purchase_item` (
  `purchase_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` longtext COLLATE utf8_unicode_ci NOT NULL,
  `purchase_price` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`purchase_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_purchase_item`
--

INSERT INTO `bk_purchase_item` (`purchase_item_id`, `purchase_id`, `product_id`, `quantity`, `purchase_price`) VALUES
(1, 1, 1, '2', '1600'),
(2, 1, 4, '5', '53'),
(3, 2, 3, '1', '800');

-- --------------------------------------------------------

--
-- Table structure for table `bk_sale`
--

DROP TABLE IF EXISTS `bk_sale`;
CREATE TABLE IF NOT EXISTS `bk_sale` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `vat_id` int(11) NOT NULL,
  `discount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  `due_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sale_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bk_sale`
--

INSERT INTO `bk_sale` (`sale_id`, `sale_code`, `customer_id`, `account_id`, `amount`, `vat_id`, `discount`, `creation_date`, `due_date`) VALUES
(1, 'f516551fc3', 4, 3, '4538.55', 2, '100', '1452708000', '1462816800'),
(2, '4503a5cb3f', 2, 4, '1438.35', 3, '50', '1453744800', '1452880800');

-- --------------------------------------------------------

--
-- Table structure for table `bk_sale_item`
--

DROP TABLE IF EXISTS `bk_sale_item`;
CREATE TABLE IF NOT EXISTS `bk_sale_item` (
  `sale_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sale_price` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sale_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `bk_sale_item`
--

INSERT INTO `bk_sale_item` (`sale_item_id`, `sale_id`, `product_id`, `quantity`, `sale_price`) VALUES
(4, 1, 1, '3', '1500'),
(5, 1, 2, '1', '20'),
(6, 1, 4, '1', '50'),
(7, 2, 3, '2', '710'),
(8, 2, 2, '1', '25');

-- --------------------------------------------------------

--
-- Table structure for table `bk_sessions`
--

DROP TABLE IF EXISTS `bk_sessions`;
CREATE TABLE IF NOT EXISTS `bk_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bk_sessions`
--

INSERT INTO `bk_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('005e600a4e522e1207e3d1cbc0375e1648f25f8a', '::1', 1453294628, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239343339353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('010fac54cb875e22c085a76c3c449c9a8543ddb5', '::1', 1453627719, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632373633383b7265715f75726c7c733a34343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('030babac8826b72305141e195a7c99a3e390288a', '::1', 1453372699, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337323336323b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('047da7e894705a0fec076ea11d928690704aff2d', '::1', 1453814003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831333939343b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32373a22496e766f69636520456d61696c6564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('04930003e7a603e925727182d0846a8a8deed777', '::1', 1453118086, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131373835333b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('04dc200dd27ed0f299075bbd2af2e69b79c4a093', '::1', 1452682220, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638313835393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('04e755dfa34c22b98022e88db4b995ce53f18c78', '::1', 1453286647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238363634373b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('054e556f6e570801378739f51459ce322cbcea78', '::1', 1453785273, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738353136363b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('058bbe58a44827297a1fab2e47a26c51cf9cd826', '::1', 1453704952, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730343635353b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('06a266e710a74e47754ef165c6469c66c0856689', '::1', 1453807069, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830363830373b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('06cee36f8c9b5e58ce6dc646f4f0d9fac32b5555', '::1', 1453811538, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831313531343b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0a2142d70054fcace206edfcf0c115d1b0971f1a', '::1', 1453356659, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335363439373b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0a3d412819aaabf5f5c46f8af15551ff4014a89d', '::1', 1452676483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637363236393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('0b1ea7a8accfe1ba1027c0e4c5a15fa430b6b149', '::1', 1453708648, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730383530363b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0c1b9b47922f637ac1ff5ee8c436a5fd5de3a679', '::1', 1453806098, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830353830383b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0c6f247756f16907078358e924edb4df44bc5c9d', '::1', 1452757113, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323735373039313b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('0df6d9879603b436b72cf4b536cde1d724f2ffb5', '::1', 1453800262, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739393939393b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('0f747f40764fe6d7616655942b7798a8ba657c2e', '::1', 1453888393, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838383230383b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('109da799dc53fcc6915b0748dfb570bea1c56332', '::1', 1453716331, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731363135323b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1179f723e6b3b62d0874742894bfb1320e83e101', '::1', 1453881978, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838313937313b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1371652f827980c2133e8e4a289fb9d10427f407', '::1', 1453967739, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936373439333b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('14176b8bccc5c6d7c2830db9278872613f4083b5', '::1', 1453799385, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739393235333b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1516a8ec7dbdde2ab36896443636dbe69f7777b4', '::1', 1453295034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239343735353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1586720cc27bd4c8621350189a8cec3e7910c374', '::1', 1453795204, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739343937333b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1644fad2f4aa488c72a6b3e04d9a2f3d38143a45', '::1', 1453809705, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830393431373b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('17554df9e7e12a1dccc13f3efaf6be97607dda74', '::1', 1452675747, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637353437353b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('178101961719aba73e7afbbf6fe75d9b919ce96c', '::1', 1453704639, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730343334393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1943c7d5cd4c291d372788e0d970c38813bd0fe0', '::1', 1453363787, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336333532393b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('19a32ea999043a8b6a430168c07a228936122f4f', '::1', 1453719660, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731393338333b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f656469742f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1b0e14b5bbec82024fc841ff2e2db0e65cca442b', '::1', 1453962851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936323834373b),
('1ca383b7f8ddf3460f3c837ea75f9f144db45253', '::1', 1453296135, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239363133353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1dc879d74667b790a23198af08191db726951768', '::1', 1453715336, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731353135343b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('1fc98f954d0f4ba0f1f25ef28e416c7868c46aa3', '::1', 1453291430, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239313135343b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2096e034dbbd3e8d2e09ff2ae2c264bc1b8c92de', '::1', 1453364857, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336343835363b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2174b915459834b404c71a8e3a029fc9a32653b8', '::1', 1453718631, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731383338373b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f656469742f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('232ede4a1b3ac9e702314c24a8f4754c1f758806', '::1', 1452681768, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638313336313b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('2451f2699b77f54b905fd590c4265d7784b436a1', '::1', 1453888099, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838373834353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2487b79a6765f46bd581708a34b4e56674a7074d', '::1', 1453800576, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830303331333b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('25617c802ea8a444a062c63319a8414e8ba3e7aa', '::1', 1453810057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830393735383b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('25e6fce1fecaa59d00d5a498ff78c92e9f648654', '::1', 1453295629, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239353338353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('25fbd2ef862fa44e1eaf63efaf49886c369120e5', '::1', 1453276289, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237363039383b),
('26199a5bdad9ff2e6c2cc1ba20df45010eb520ab', '::1', 1453631497, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633313239373b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('26561fc93957919f2ac9ee9070dad87c05e5333c', '::1', 1453965098, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936343835373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('267f59ad61d01184dcaf9d041bdb4ef5292b5337', '::1', 1453880369, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838303336393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('268984e4248a8a258350a9f026c5907c77347e17', '::1', 1453287860, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238373835393b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('28e19813f9d8df6077bfc3e6774160e406b69c43', '::1', 1453295970, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239353733333b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('28fa8cc6b80ae7920688dbbc48e390900a6fc957', '::1', 1453809083, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830383934383b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2afe2b7ce0288c74d579af7fc271664e244d387b', '::1', 1453884009, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838333931363b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2b756692dfe8d24efee46d9dba749d7d8178b80f', '::1', 1453717040, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731363833313b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2b89d1bb7fe829193ae2aa1ba9a34afa41fcb374', '::1', 1453715981, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731353737363b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2d7ddfc2b86853e02e0e6da71f46d2743ed8080e', '::1', 1453886272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838363136383b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2df4976d3dffefa966bca7e15a77126258134452', '::1', 1453717915, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731373835353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f656469742f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2e0c2a5934370a3c15758214272a7d0e2979a9d1', '::1', 1453357986, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335373733373b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2e2f27eacc318a34b637db239cb66baa1c682734', '::1', 1453706728, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730363437313b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2f7d76cdcd478e256737571f634980e86132e280', '::1', 1453289652, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238393438383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('2f8e7aeaa829941996546bab7672e0bf9af8fc79', '::1', 1453624087, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632333832383b666c6173685f6d6573736167657c733a32343a22446174612044656c65746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('305685e7d899e20fa4d434570d108277e6865df0', '::1', 1453623547, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632333235343b666c6173685f6d6573736167657c733a32343a22446174612043726561746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('316a92e703e201f54359867be5882a185c6abb9d', '::1', 1453975390, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937353132353b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('32d235cde282210ac299f95c9ba2b371c55eedb2', '::1', 1453113948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131333635363b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('33ce66ad20afe1c5ece6567eab723bfaf61f3a2d', '::1', 1453806418, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830363238343b7265715f75726c7c733a34343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('33f70782bc22f82b6740e51cc187260bf6000ed1', '::1', 1453285933, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238353637353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('341375a1f4ef372f70af03b331c2e8ebdd77ddcd', '::1', 1453888850, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838383535383b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3570c8fb9c13226b1518d213569ee3095bb2546d', '::1', 1453627297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632373239373b),
('370f4be07e7a2156835c4133c4f21c133df7d58a', '::1', 1453884484, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838343337313b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3773c7ef7ec9d3eadb526ad9cee92455ac9b95dd', '::1', 1453700379, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730303337393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('37ab2787b3b9fa54726444a2ad79d5507a44e287', '::1', 1453709511, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730393531313b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3890a3cccd098bf11fd8c3ce1da5720107e51069', '::1', 1453266570, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333236363536383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b),
('38abbf1f3d038b7a2ddeb34d79f26ff0e9a1c6e0', '::1', 1453296693, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239363538303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3b227e1a5b9fcca86f1c30b90bf33ce3b3a17d81', '::1', 1453274228, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237343230373b),
('3b55155d77037bc9dbe009c8ea6a1f92ee588435', '::1', 1453290422, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239303135363b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3bc31bd8fe6a9c897a6d753549887263ef9754d2', '::1', 1453808484, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830383239383b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3dd51a451e75d651a69e04a7e351d6c05a1c91fc', '::1', 1453299409, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239393132353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3df6ba2308854c59027641ad7616476f8c422981', '::1', 1453363512, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336333232313b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('3f437cbb85b076880d9b2056e054f8f92775b1b1', '::1', 1452681225, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638313031373b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('3f89f7d648f2d7938c382b463513306f25723eeb', '::1', 1453801343, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830313034383b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('43802392092d2acdaf5d8c0728acc233911ca300', '::1', 1453788675, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738383431343b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('45eab1ca772aa567a469432b825a88c20f1daa8d', '::1', 1453367229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336373031343b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4a0d3fecc6d520635c222170312224fb958ff7e0', '::1', 1453118347, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131383232313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4ab39f3eb5b1d8cafdf544210f37f0b86368338a', '::1', 1453810888, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831303838373b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4bf6fff7b180e4f544b6d0d9711f2e734ffa744d', '::1', 1452683022, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638323932313b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('4d1f238e9e5a7ef8fee9d38b717bb24488088522', '::1', 1453807441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830373239313b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4d2cca2b71ce143c3ab95fe0c958d1aed7b0e9bf', '::1', 1453367945, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336373830303b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('4dd2f774c1d3be8d85fa5773d60c276a4bd7b9f0', '::1', 1453810622, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831303436303b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32373a22496e766f69636520456d61696c6564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('4f5b03aab6d6f7e626d35edf258ed1c9e06a3fcf', '::1', 1453623620, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632333632303b),
('4fa7adf8ddb09eaf4b5e417e7feb0a9cf3a8b816', '::1', 1453717544, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731373534343b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('51422908fe46144791f1b9c99cf9d8e3acc225f8', '::1', 1453801967, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830313433323b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5187ab7346575cc4df49268a22c8f18dddfb3ee7', '::1', 1453629236, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632393138303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b);
INSERT INTO `bk_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('53fc3f5a24e03f3506950fef6c62c2eb09092b44', '::1', 1453287023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238373032323b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('573ad302530fc6c20578d762c51b8ac73e1a646f', '::1', 1453707173, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730373031393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('578fffbe0db161d01e347d2a1e364b4fed907c19', '::1', 1453788910, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738383738353b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('58edf2a9697af17f029f33bf8ba692b5bebd54f0', '::1', 1453285034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238343736313b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5923e6f9be6f2516f678c97be766d3e17c13f39c', '::1', 1452664441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636343130303b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('5940e59d131c9536b4d362c8b9b0ceef665febc2', '::1', 1453374617, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337343434363b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b),
('5988355462c2d2c9ff9071f07271aa14de429c8b', '::1', 1453890879, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333839303831303b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5a786e8f85886050441815e8f41216514809246a', '::1', 1452665027, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636343831393b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('5c7edd63d1d1e4fbbd7c0efeb8b6b0863d560475', '::1', 1453962618, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936323334303b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f36223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5d414e35c1b3f3df8a9f7d39175e718ac3f84507', '::1', 1453882420, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838323430373b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5d81036852dcfd5f28f3497818d2ca3c1046edf8', '::1', 1453698928, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333639383930393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5e34ebed82cb759a405a0063fd6a54694cddf11e', '::1', 1453881527, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838313532343b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5e7d5564024617739442621210653b6a387b6eaf', '::1', 1453802893, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830323834313b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('5fa61160bd18ffbf80d32af68f6049000f706367', '::1', 1453878647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333837383439303b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('615b1dfa73c3a063abd0b8d2ece0f04454de5835', '::1', 1453884678, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838343637383b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('62540f3976c46635c3f07e661fc5d7eb19861f5d', '::1', 1453873809, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333837333632303b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f696e766f6963652f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('64dcc148a254a2a05533ee7e813a15acf1822cdf', '::1', 1453637246, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633373234363b),
('6593f9949038d433046844c29130a23e6961a6ff', '192.168.0.104', 1453707762, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730373430353b7265715f75726c7c733a34323a22687474703a2f2f3139322e3136382e302e3130382f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6607036f4a09feb6fc377b71d7c0aba3f615292f', '::1', 1452760572, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323736303238303b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('6701ee34674ce4fecd52285d9ffc143502400bdc', '::1', 1453286483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238363334303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('67b988ce7de2c4e5d39995996178f2b0e506acb1', '::1', 1453717466, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731373232333b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('68145f30aa85f4b5e3d91f2db9047f108748258c', '::1', 1453970326, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937303035343b),
('684b82354da10e74212da67273547831eec56a7c', '::1', 1453636607, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633363537363b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32343a22446174612044656c65746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('68b7758ea51a4096e50eb40b8668b8d05ae1996f', '::1', 1453275478, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237353230363b),
('6961a89146a0e3dc819434eed456efeac6105dde', '::1', 1453116870, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131363538313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6a9bfc1ef5d1c2ade06836bc19cf4a86e602102e', '::1', 1453116132, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131353834373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6bb17ee17747215646fca7b300f49f938dba965c', '::1', 1453631646, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633313634363b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6bdbc109da4f675cc9ee2e334d0f35b3c308cde4', '::1', 1453623245, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632323935313b666c6173685f6d6573736167657c733a32343a22446174612043726561746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('6c0dc52804f5d04c07c9c8a1a7a7667439234c49', '::1', 1453965594, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936353534373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6d4c1824e9f6ff19693449a91a34e9daad00a0af', '::1', 1453372908, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337323930383b),
('6ebaf07cb4cbfa62a310e7193b3cbde67a796f84', '::1', 1453719851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731393731353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f656469742f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('6edf5188bcf884bfaa263392351e9e3e61475aec', '::1', 1453705033, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730343935383b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('70f01f52b8246c51784bf031d9bd198b80f79c1c', '::1', 1453885625, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838353430383b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('714e03ea58bfaadbc9adb6f7e1b49da66eb969b5', '::1', 1453117840, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131373534303b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('71eeb826963a672e0e7e8ac7ef61e1fa261292ac', '::1', 1453294254, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239343034383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('73bd9f16df0862be664438eabc628a9a86822b63', '::1', 1453807602, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830373630313b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('73c40e0423fbc6975c5ddbf245b856f128699c5d', '::1', 1452664098, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636323836383b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('74c29279bebf752bd1c001b80f5c7c8630ee62b9', '::1', 1453786532, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738363239323b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('77eada1e69d919dc1482b44ec817626ef96f5242', '::1', 1453806275, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830363234303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('7836aaa1be33a50d40d8665235cfe26b4228f02d', '::1', 1453291049, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239303739303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('78eb44bfa18302fd5a4c21adf2b8339b618c5028', '::1', 1453299100, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239383830353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('7e6c3d5ab4a4a98ed216ffaba17d119008792954', '::1', 1453703981, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730333638393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('7e93c4d590f3defa37023b522dc57e392505e527', '::1', 1453631293, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333633303830393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('811e64b69a8c3569a2abf6caa0d2c1147d20955a', '::1', 1453115504, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131353238303b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('827cff16190f09551a48a37adb8a2d42ee6a7df6', '::1', 1453965963, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936353936333b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('82af6745e4c873b114eccba1061edba438e851f3', '::1', 1453367793, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336373439343b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('835e35705ca310ffa04f66c44950ded9a39e4a88', '::1', 1453968665, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936383433333b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('8366fe38b1e02c7ccf18a646e83f1f9b5d6d3b89', '::1', 1453364204, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336333936323b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('870cc02ae2fba223374ba911d0dc75ae02209063', '::1', 1453800894, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830303632313b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('889c9037abc29df8fd86e0b69d06b69dc48f8b42', '::1', 1453971112, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937313039323b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b),
('8aeb6127538420afb73d809e787ba36e40e99bd3', '::1', 1453802518, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830323332393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('8db28812d5c90a82478532eae17c31463aeaea2c', '::1', 1453885898, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838353734373b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('907580ea36159831cd313b10d5b6f769578a9df8', '::1', 1453813668, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831333635393b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32373a22496e766f69636520456d61696c6564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('9103911d3403479e8cf33c3475fcc3f68bc891a0', '::1', 1453801982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830313936393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('92e17fb8207da50920b63a986e41679e2be2ed9a', '::1', 1453976252, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937363037393b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('93a8ab8d3ceebb4afffceffce1d016bebe5e28d1', '::1', 1453963482, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936333335353b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('946340504594e855b2aa1dd7f2de356d09d84af8', '::1', 1453360909, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336303636323b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('95619abe5359833ccebdf427c6abdc0dd8111d06', '::1', 1453975122, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937343831373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('95e3e10dc93ed7a5f33522ee4f3f7cd0d407255e', '::1', 1453968178, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936373937353b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('960dcb7df382ec3241d73d6c717f47a46bb46092', '::1', 1453977545, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937373339323b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a31363a2253657474696e67732055706461746564223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('96159cf3a25473fae47ec1641d03af6c7fc960a1', '::1', 1453628254, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632373938303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('96b2fbc0df330454ad2cded813d1505720aed122', '::1', 1453714207, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731343137323b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('973f392f704848cdeb5cbfbc65c7fb8de7806c91', '::1', 1453977392, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937373039313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32313a2246696c65204372656174696f6e2053756363657373223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226e6577223b7d),
('978cf7642f46bf42bb71495bbc7b522c803b8d9d', '::1', 1453351631, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335313630383b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('98818b4b8f3e049d72292ae91cca6f99fe14891e', '::1', 1453285329, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238353036363b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('999ca45a5087e095e08ce2781b032060bfa1ad77', '::1', 1453274575, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237343537343b),
('9a118c77235cbca7131d7255c64ed4dc490fa4a1', '::1', 1453808909, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830383631333b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9b0250c7fa973f96bcd8b7f0ec5c8ef8fc4a705b', '192.168.0.104', 1453708275, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730383132333b7265715f75726c7c733a34323a22687474703a2f2f3139322e3136382e302e3130382f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9b332d537b71be6bbf48654a48136bde4b0a3810', '::1', 1453372269, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337313837383b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9caba1e7527d331858816041ff39da773b86a2c2', '::1', 1453117514, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131373232323b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9cd8817d1b516a2394e38be6968bbd3c8df17f2d', '::1', 1453292194, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239323133323b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9d053e239e1cb25d97cb10ec02dd814ce0900afb', '::1', 1453714902, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731343834303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('9fa9797898a73f24fac419aab32f33bd9ba49350', '::1', 1453890784, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333839303439353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a3fa5002d9499edcb88bdac40e6c403ba9d4a3e4', '::1', 1452664722, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636343439333b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('a502707b36cadd128e06968d689bb5b4c1668d06', '::1', 1453707575, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730373537353b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a5b9306af92de65bbc9e8965b0163e5860e6a273', '192.168.0.104', 1453707948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730373737343b7265715f75726c7c733a34323a22687474703a2f2f3139322e3136382e302e3130382f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a645df4f059b7e2a45a38f5ad9c98f84f8095118', '::1', 1453719059, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731393033363b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f656469742f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a70cedb2bce31c50c5d2c0849dd29346f36e5536', '::1', 1453799936, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739393637313b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a7722d00f34d69dbb19522d3c234150dac61ae94', '::1', 1452675387, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637353131323b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('a78ef89613b22cddad95c0393748239c4025fa53', '::1', 1453361219, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336313035393b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('a8549c52c0c1eac39eff0882b3e9f36f70ecdc0f', '::1', 1453287674, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238373534323b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('a878677a80f5131b2420faeb5f16769f6872b796', '::1', 1452680115, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638303036303b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('a8c1bba7026ecc756ad4b22ab0d327d0c8ca2585', '::1', 1453289999, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238393739383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ab1b3ecad1afcf0729393cc323800a9ebb30cabe', '::1', 1453796535, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739363238373b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ac159e050a47ad93e32c680cf5bbc1d999d3f764', '::1', 1452682552, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638323233323b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('ae046a479a866752c8b203230dd6bef7f911b682', '::1', 1453886951, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838363833353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b1da75f06436aecc8bab05dd3f79267954ef74a1', '::1', 1453812483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831323234363b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b29e354a537b3694709bf6331990e704cd382108', '::1', 1453621972, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333631393437303b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b404137bc2ac4a443152e579c76ee2749507510f', '::1', 1452670265, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637303235393b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b);
INSERT INTO `bk_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('b48a56c892d02de75cf7f2d04d1f2086d3244b03', '::1', 1453714832, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731343533383b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b836e38f4680c4cdfabfde1e041bb6c614563d96', '::1', 1453355330, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335353333303b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('b8e3aedc05bceff9db5f61d0ed5c63c8968fd242', '::1', 1452668077, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636373836363b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b666c6173685f6d6573736167657c733a32343a22446174612043726561746564205375636365737366756c79223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('b956f8e6acb6029380d6614b05bcdb7678f85eab', '::1', 1453972158, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937323132343b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('bbe9d6e494e669b672c8ad66d5b4ea89d0d538a9', '::1', 1453804112, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830333933333b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('bc0dd799e9a7cd141c65172de67922829236ca5c', '::1', 1453961089, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333935393536353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f36223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('bcaa08a638c48e3e292e4e32206d2a37ee6adf7c', '::1', 1453795955, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739353732353b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('bdf9d5664dc89586d9270507efaad290355e7415', '::1', 1453109517, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333130393531373b),
('be1bdf94bdad215281df6b98fb4f85efe090e256', '::1', 1453622104, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333632323130343b),
('be2ea8dd7b6ab521ba20c923658c290b8870e86d', '::1', 1452676205, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323637353932363b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('bf34b45d9974583a1aae91ce315382b988c9a4cd', '::1', 1453371713, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333337313533343b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c02a3bd1839fdfe9ee6573d5df81d03ba6f6a16b', '::1', 1453370078, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336393836313b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b666c6173685f6d6573736167657c733a32323a22456d61696c2054656d706c6174652055706461746564223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('c3ebdbc6cae595d8dfb52e1062c77d3659aee89a', '::1', 1453794808, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739343635363b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c550acb6e960212529f50b17ec5cdeae7890f206', '::1', 1453715710, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731353435393b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c5560779aec14d356efddebccf4b17c3d6b3f8f2', '::1', 1453114340, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131343237373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c5f5a9c83c76c779248fa5c680a46334fec06dd6', '::1', 1453787036, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738373031323b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('c851c132d228cb1301ed32ba9a6d0178fc746fca', '::1', 1453291939, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239313832363b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ca0b983365823f8f892f52669e7b25bcf5c5b703', '::1', 1453968897, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936383738313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('cb0d194acea3eb732e4ff6bf9b568983937371fe', '::1', 1453627149, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632363837343b),
('cc14e2d5611247cf565cb653934ba90d26b5cc11', '::1', 1453275746, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333237353533313b),
('ccefc146689b535eb4f93f2117453cb9f53aeffa', '::1', 1453284606, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238343332373b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ce57d2c6d1dc5b2a8941207446c5b269ec37803f', '::1', 1452669228, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636393038393b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('cf2e2bf4ecbebf5e58395a3d3332037bf8f5528c', '::1', 1453961726, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936313533353b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f36223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('cfffe926642097b32c902b9918e84d9c5a7149a8', '::1', 1452680862, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638303537323b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('d0899f7406e90cb51ed9dc88bc9a8882914669fc', '::1', 1453806771, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830363438323b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d09d78b47a28a8b727f34b6227444944fff6f802', '::1', 1453794650, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333739343335323b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d0f5e0dd8237f2283152f90f5178777cb5d94f57', '::1', 1453625725, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632353732353b),
('d2cef1ecd505203897766d3d4aee78a9a9355da3', '::1', 1453293963, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239333731303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d3e971b32281b085a20dca12a2b46868b6a1741f', '::1', 1453284073, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238333835353b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d4886854f892b676572930e35f96a39d0d3c7002', '::1', 1453961292, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936313039313b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f36223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d57e53025c35758bd8b40cf5cf3ed1c6a6c3672a', '::1', 1453622272, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632323230323b),
('d72b3a3391a1a8815dc2e2d3e1bfb654bcfeefb3', '::1', 1453704243, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333730343032353b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('d81ca00300e2b8370aec3ba635133f76154d282a', '::1', 1453890340, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333839303036333b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f636f6e746163742f636f6e746163745f64657461696c732f34223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('df2915346e8ef60b4a59cfe95f1b989ab6335ce5', '::1', 1453785861, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738353836313b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e03baa114d076f2d50f0bccfc6fc412040ba3f62', '::1', 1453786985, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738363639343b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e0b49ed1398a0d97cc5b4e2e8b7166b590a56199', '::1', 1453290688, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239303437303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e290dce45813b16d0660aa835ff2d392b93a0027', '::1', 1453976723, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937363435343b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e453ec675632518f82404478cb0de5d8279f2736', '::1', 1453788057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738383035373b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e4b8b53fcf1b10f1535e3c1d2e9ee0d3b061ee23', '::1', 1453107711, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333130363835333b7265715f75726c7c733a36373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f696e766f6963652f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e52aa76e897cb07c18190dd9d2288eb0cc5bda45', '::1', 1453883546, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333838333333303b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e5ff3e44fa800b6561288f1920b60f7bb371a668', '::1', 1453361800, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336313530383b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e62c1d778cc6cf872d3826412b8d54179deca781', '::1', 1452682864, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323638323537303b7265715f75726c7c733a35373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f7075726368617365223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('e8456907994b8c7aaefe4f585029020c5140fec1', '::1', 1453622915, 0x757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b5f5f63695f6c6173745f726567656e65726174657c693a313435333632323633313b),
('e8caaf9f4c1e13e98fb4985b51c4292cb33b7a6c', '::1', 1453362093, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336313831373b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e9e3a0db30d72bf03bacc26068a54a309edc0e35', '::1', 1453965512, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936353232393b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('e9e576cc1c4c23140ac1c9e6e9aaf5e679734a45', '::1', 1453116397, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131363137323b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('eae9896cdd7161f996e20ba4a35dd0476a9d7255', '::1', 1453810453, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333831303135383b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('eafb0cff437902900e373969d7af6247fa36e94c', '::1', 1453365673, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333336353637333b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('ebe56557c2d97018eb6300861ab0beed66af3587', '::1', 1453295334, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333239353037303b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f1435fd698716f7a5cfc9c5fdaa97c99bd2e1f0a', '::1', 1453106851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333039393938363b7265715f75726c7c733a36373a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f696e766f6963652f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('f2db01485ce554acecf191b86cb6ed29a588ca31', '::1', 1453113283, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131333137313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f2f6d3b27338f66b705f71e9cfbbda13b8e96d7c', '::1', 1453967346, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333936373138373b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f4dc368a747c673186c61baa6d72bee4b7908ff8', '::1', 1453977084, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937363738353b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f4e6eb055e4c2c028a95ed8ebd79fe8449e018e5', '::1', 1453114229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131333935383b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f5dac37f1d7f12fd61deead8b4a1470b00d1fc12', '::1', 1453718016, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731383030333b7265715f75726c7c733a36343a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f656469742f32223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f88f698023500c4958a497011a813f09844739c3', '::1', 1453281420, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333238313430383b7265715f75726c7c733a36363a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f6163636f756e745f73746174656d656e74223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f914d2173f7f3f9416ef92d132058adca31075fe', '::1', 1453716735, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333731363437373b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('f94ec773a80cf09054be79ba5de98afe9de23ce2', '::1', 1452760730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323736303630353b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('f9eb028362f5b7eabc2c91b6e390955c61fa11dc', '::1', 1453808142, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333830373939353b7265715f75726c7c733a35383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f61646d696e2f746573745f7064665f76696577223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('fa459bb308bcb8c4ec1cced65ff51850678c1daa', '::1', 1453117163, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333131363931313b7265715f75726c7c733a33383a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e706870223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('fc41c78eb4d47293c65986e373fea09ffa818e73', '::1', 1453357257, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333335373134363b7265715f75726c7c733a36333a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f7265706f7274696e672f657870656e73655f7265706f7274223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b),
('fcd370a48ec65dca1ffa0336dad9a18c34408223', '::1', 1452665485, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636353135313b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('fd713206a9bf0c377e15f9de689232fe86fc0f9c', '::1', 1453970481, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937303432313b),
('fdcefb33c5af69106f30dfb32ef2176e91f5d7ff', '::1', 1452666939, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323636353534353b7265715f75726c7c733a36313a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f70757263686173655f616464223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b),
('ff1f14dc157cb7594b54fb0c2e1a656aa78d0dcc', '::1', 1453974789, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333937343531313b),
('ff8ded0e47b5c7ec36efc617876fd037eb785c85', '::1', 1453789608, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435333738393535333b7265715f75726c7c733a36303a22687474703a2f2f6c6f63616c686f73742f626f6f6b6b656570696e672f696e6465782e7068702f696e76656e746f72792f73616c655f656469742f31223b757365725f69647c733a313a2231223b757365725f656d61696c7c733a31373a2261646d696e406578616d706c652e636f6d223b757365725f66697273745f6e616d657c733a343a224a6f686e223b757365725f6c6173745f6e616d657c733a333a22446f65223b73797374656d5f63757272656e63797c733a333a22424454223b63757272656e63795f706c6163696e677c733a31343a2261667465725f776974685f676170223b);

-- --------------------------------------------------------

--
-- Table structure for table `bk_settings`
--

DROP TABLE IF EXISTS `bk_settings`;
CREATE TABLE IF NOT EXISTS `bk_settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `bk_settings`
--

INSERT INTO `bk_settings` (`settings_id`, `type`, `description`) VALUES
(11, 'skin_colour', 'blue'),
(2, 'system_title', 'Bookkeeping'),
(4, 'system_currency_id', 'BDT'),
(12, 'text_align', 'left-to-right'),
(17, 'language', 'en'),
(3, 'system_email', 'bookkeeping@example.com'),
(19, 'currency_placing', 'after_with_gap'),
(20, 'financial_year_start', 'january');

-- --------------------------------------------------------

--
-- Table structure for table `bk_user`
--

DROP TABLE IF EXISTS `bk_user`;
CREATE TABLE IF NOT EXISTS `bk_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` char(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bk_user`
--

INSERT INTO `bk_user` (`user_id`, `first_name`, `last_name`, `email`, `password`, `status`) VALUES
(1, 'John', 'Doe', 'admin@example.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1),
(2, 'Rhoda', 'Bell', 'aqua.primera@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bk_vat`
--

DROP TABLE IF EXISTS `bk_vat`;
CREATE TABLE IF NOT EXISTS `bk_vat` (
  `vat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `percentage` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bk_vat`
--

INSERT INTO `bk_vat` (`vat_id`, `name`, `percentage`) VALUES
(2, 'Tax', '1.5'),
(3, 'Vat A', '3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
